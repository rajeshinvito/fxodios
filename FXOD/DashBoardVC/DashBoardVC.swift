
//
//  DashBoardVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//self.tabBarController?.navigationController?.pushViewController(ViewControleer, animated: true)
//self.tabBarController?.show(ViewControleer, sender: self)

import UIKit

class DashBoardVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    //MARK:- ------- Class IBOUTLETS -----------
    @IBOutlet weak var Accountbackviewref: UIView!
    @IBOutlet weak var Collectionviewref: UICollectionView!
    @IBOutlet weak var Ammountlblref: UILabel!
    @IBOutlet weak var Back: UIButton!
    //MARK:- ------- Class propeties ------
    var colcontentArr  =  ["My Account","Deposit","Withdrawal","Transfer","My Transactions","Investment","Support","IB Account"]
    var colImagesArr : [UIImage] =  [#imageLiteral(resourceName: "MyAccount"),#imageLiteral(resourceName: "Deposit"),#imageLiteral(resourceName: "Withdrawal"),#imageLiteral(resourceName: "Transfer"),#imageLiteral(resourceName: "MyTransactions"),#imageLiteral(resourceName: "Investment"),#imageLiteral(resourceName: "Support"),#imageLiteral(resourceName: "IBAccount")]
    //MARK:- ------- Class View lyfe cycle  ------
    
//    override func loadView() {
//        self.tabBarController?.tabBar.isHidden = false
//
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Accountbackviewref.layer.cornerRadius = 22
        Accountbackviewref.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.Back.isHidden = false
        self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
        self.Back.backgroundColor = .clear
        self.navigationController?.navigationBar.isHidden = true
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.tabBarController?.tabBar.isHidden = false

        //Api calling....
        self.DashboardApiMethod()
        self.tabBarController?.tabBar.isHidden = false

    }
    override func viewDidAppear(_ animated: Bool) {
    self.tabBarController?.tabBar.isHidden = false

    }
    
   
    //MARK:- -------- Class Button Actions --------
    @IBAction func Profilebtnref(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "menuopened")
        self.movetonextvc(id: "profileVC", storyBordid: "DashBoard")
    }
    @IBAction func createLiveAcc(_ sender:Any){
        self.movetonextvc(id: "CreateLiveAccountVC", storyBordid: "DashBoard")
    }
    
    @IBAction func moveToInfo(_ sender:Any){
          self.movetonextvc(id: "KnowledgebaseVC", storyBordid: "DashBoard")
      }
      
    @IBAction func createDemoAcc(_ sender:Any){
        self.movetonextvc(id: "CreateDemoAccountVC", storyBordid: "DashBoard")
        
    }
    @IBAction func cmdMenu(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "menuopened")
        Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
    }
    
    @IBAction func logOutbtnref(_ sender: Any) {
        let alert = UIAlertController(title: "Logout?", message: "Are your Sure want to logout?",  preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
            //Cancel Action
        }))
        alert.addAction(UIAlertAction(title: "YES",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        self.logoutMethod()
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        
                                        var navigation = UINavigationController()
                                        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                                        navigation = UINavigationController(rootViewController: initialViewControlleripad)
                                        appDelegate.window?.rootViewController = navigation
                                        appDelegate.window?.makeKeyAndVisible()
                                        
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func UserInfobtnref(_ sender: Any) {
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colcontentArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :homeCollecCell  = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCollecCell", for: indexPath) as! homeCollecCell
        cell.Imageref.image =  colImagesArr[indexPath.row]
        cell.Titlelblref.text = colcontentArr[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            //  self.movetonextvc(id: "MyAccountVC", storyBordid: "DashBoard")
            //            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            //            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
            //            nxtVC.vcFROM = "DashBoardVc"
            //            self.navigationController?.pushViewController(nxtVC, animated: true)
            
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
            initialViewController.selectedIndex = 1
            let navController = UINavigationController(rootViewController: initialViewController)
            navController.setViewControllers([initialViewController], animated:true)
            navController.isNavigationBarHidden = true
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else  if indexPath.row == 1 {
            // self.movetonextvc(id: "DepositVC", storyBordid: "DashBoard")
            //            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            //            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "DepositVC") as! DepositVC
            //            nxtVC.vcFROM = "DashBoardVc"
            //
            //            self.navigationController?.pushViewController(nxtVC, animated: true)
            
            
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
            initialViewController.selectedIndex = 2
            let navController = UINavigationController(rootViewController: initialViewController)
            navController.setViewControllers([initialViewController], animated:true)
            navController.isNavigationBarHidden = true
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
            
        }else  if indexPath.row == 2 {
            //   self.movetonextvc(id: "WithdrawalVC", storyBordid: "DashBoard")
            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "WithdrawalVC") as! WithdrawalVC
            nxtVC.vcFROM = "DashBoardVc"
            
            self.navigationController?.pushViewController(nxtVC, animated: true)
        }else  if indexPath.row == 3 {
            //self.movetonextvc(id: "TransferVC", storyBordid: "DashBoard")
            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "TransferVC") as! TransferVC
            nxtVC.vcFROM = "DashBoardVc"
            
            self.navigationController?.pushViewController(nxtVC, animated: true)
        }else  if indexPath.row == 4 {
            // self.movetonextvc(id: "InvestmentVC", storyBordid: "DashBoard")
            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "MyTransactionVC") as! MyTransactionVC
            nxtVC.vcFROM = "DashBoardVc"
            
            self.navigationController?.pushViewController(nxtVC, animated: true)
        }else  if indexPath.row == 5 {
            // self.movetonextvc(id: "InvestmentVC", storyBordid: "DashBoard")
            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "InvestmentVC") as! InvestmentVC
            nxtVC.vcFROM = "DashBoardVc"
            
            self.navigationController?.pushViewController(nxtVC, animated: true)
        }else  if indexPath.row == 6 {
            //  self.movetonextvc(id: "IBAccountVC", storyBordid: "DashBoard")
                        let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
                        let nxtVC = Storyboard.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
                        nxtVC.vcFROM = "DashBoardVc"
                        self.navigationController?.pushViewController(nxtVC, animated: true)
            
            
//            let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
//            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
//            initialViewController.selectedIndex = 3
//            let navController = UINavigationController(rootViewController: initialViewController)
//            navController.setViewControllers([initialViewController], animated:true)
//            navController.isNavigationBarHidden = true
//            self.revealViewController().setFront(navController, animated: true)
//            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else  if indexPath.row == 7 {
            //  self.movetonextvc(id: "IBAccountVC", storyBordid: "DashBoard")
            let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            let nxtVC = Storyboard.instantiateViewController(withIdentifier: "IBAccountVC") as! IBAccountVC
            nxtVC.vcFROM = "DashBoardVc"
            
            self.navigationController?.pushViewController(nxtVC, animated: true)
        }
    }
    
    
    
    
    
    
    
    
    
    func logoutMethod(){
        
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
            
        ]
        NetworkManager.Apicalling(url: logOUTUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(false, forKey: "LoggedIn")
                UserDefaults.standard.removeObject(forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "user_id")
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set("", forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "name")
                
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    func DashboardApiMethod(){
        self.tabBarController?.tabBar.isHidden = false

        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: DashBoardUrl, paramaters: parameters, httpMethodType: .post, success: { (response:DashboardModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                if let Amount = response.payload.balance.total_amount as? Int {
                    self.Ammountlblref.text = "$  \(Amount)"
                }
                if response.payload.ib_menu == 1 {
                    self.colcontentArr  =  ["My Account","Deposit","Withdrawal","Transfer","My Transactions","Investment","Support","IB Account"]
                    self.colImagesArr =  [#imageLiteral(resourceName: "MyAccount"),#imageLiteral(resourceName: "Deposit"),#imageLiteral(resourceName: "Withdrawal"),#imageLiteral(resourceName: "Transfer"),#imageLiteral(resourceName: "MyTransactions"),#imageLiteral(resourceName: "Investment"),#imageLiteral(resourceName: "Support"),#imageLiteral(resourceName: "IBAccount")]
                }else {
                    self.colcontentArr  =  ["My Account","Deposit","Withdrawal","Transfer","My Transactions","Investment","Support"]
                    self.colImagesArr =  [#imageLiteral(resourceName: "MyAccount"),#imageLiteral(resourceName: "Deposit"),#imageLiteral(resourceName: "Withdrawal"),#imageLiteral(resourceName: "Transfer"),#imageLiteral(resourceName: "MyTransactions"),#imageLiteral(resourceName: "Investment"),#imageLiteral(resourceName: "Support")]
                }
                self.tabBarController?.tabBar.isHidden = false

                if response.payload.document_verify == 1 {
                    UserDefaults.standard.set(true, forKey: "IsDocumentVerify")
                }else {
                    UserDefaults.standard.set(false, forKey: "IsDocumentVerify")
                    
                }
                
                if response.payload.email_verify == 1 {
                    UserDefaults.standard.set(true, forKey: "IsEmailVerify")
                    
                }else {
                    UserDefaults.standard.set(false, forKey: "IsEmailVerify")
                    
                }
                
                self.Collectionviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
extension DashBoardVC:MenuDelegate {
    
    func MenuMethods() {
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
    
    
    
}



class homeCollecCell : UICollectionViewCell {
    @IBOutlet weak var Imageref: UIImageView!
    @IBOutlet weak var Titlelblref: UILabel!
    
}
extension DashBoardVC {
    //    func revealController(revealController: SWRevealViewController!, panGestureBeganFromLocation location: CGFloat, progress: CGFloat) {
    //
    //        let image = self.originalImage
    //        self.imageBlur?.image = image?.applyFixedBlurWithRadius(CGFloat(progress*self.maxRadius))
    //    }
    //
    //    func revealController(revealController: SWRevealViewController!, panGestureMovedToLocation location: CGFloat, progress: CGFloat, overProgress: CGFloat) {
    //        let image = self.originalImage
    //        self.imageBlur?.image = image?.applyFixedBlurWithRadius(CGFloat(progress*self.maxRadius))
    //
    //    }
    //
    //    func revealController(revealController: SWRevealViewController!, panGestureEndedToLocation location: CGFloat, progress: CGFloat, overProgress: CGFloat) {
    //        let image = self.originalImage
    //        self.imageBlur?.image = image?.applyFixedBlurWithRadius(CGFloat(progress*self.maxRadius))
    //
    //    }
    //
    //    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
    //        if(position == .Left)
    //        {
    //            self.imageBlur?.image = self.originalImage
    //        }
    //        if(position == .Right)
    //        {
    //            let image = self.originalImage
    //            self.imageBlur?.image = image?.appl
    //            applyFixedBlurWithRadius(CGFloat(1*self.maxRadius))
    //        }
    //    }
    //
    //    func takeSnapShotOfView(view:UIView) ->UIImage
    //    {
    //        UIGraphicsBeginImageContext(CGSize(width: view.frame.size.width, height: view.frame.size.height))
    //        view.drawHierarchy(in: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), afterScreenUpdates: true)
    //        let image = UIGraphicsGetImageFromCurrentImageContext()!
    //        UIGraphicsEndImageContext()
    //
    //        return image
    //    }
}
