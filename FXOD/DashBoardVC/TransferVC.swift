//
//  TransferVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DropDown
class TransferVC: UIViewController {
    
    //MARK:- ------- Class IBOutlets ----------
    
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var chooseAccountbtnref: UIButton!
    @IBOutlet weak var ChooseAccountTfref: UITextField!
    @IBOutlet weak var AccountNumberTfref: UITextField!
    @IBOutlet weak var AccountTypeTfref: UITextField!
    @IBOutlet weak var CurrencyTfref: UITextField!
    @IBOutlet weak var AmountTfref: UITextField!

    //MARK:--------- class propeties ----------
    
    var vcFROM = ""
    var dropDown = DropDown()
    var AccountsArr = [String]()
    var AccountsArr2 = [accountsdata]()
    var selectfromAcc = String()
    //MARK:- ------- View Lyfe Cycle ----------
    
    override func viewDidLoad() {
            super.viewDidLoad()
          
      }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true

       self.tabBarController?.tabBar.isHidden = true
//                  UserDefaults.standard.set(true, forKey: "menuopened")
//                  tabBarController?.reloadInputViews()
//        if vcFROM == "DashBoardVc" {
//            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
//            self.Back.backgroundColor = .white
//        }else {
            //UserDefaults.standard.set(false, forKey: "frommenu")
            self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
            self.Back.backgroundColor = .clear
       // }
        
        //  self.tabBarController?.tabBar.isHidden = true
        self.TransferPrefetchData()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
                         self.tabBarController?.tabBar.isHidden = false

       }
    
    @IBAction func cmdMenu(_ sender: Any) {
//        if vcFROM == "DashBoardVc" {
//            self.popToBackVC()
//        }else {
//            //  self.tabBarController?.tabBar.isHidden = true
//                   UserDefaults.standard.set(true, forKey: "menuopened")
//                   self.tabBarController?.reloadInputViews()
////            sharedMenuClass.menuObj.addMenuClass(view: self.view)
////                      sharedMenuClass.menuObj.menuView.delegate = self
            
            Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

        //}
    }
    
    
    @IBAction func ChhoseAccountbtnref(_ sender: Any) {
        let viewSource = chooseAccountbtnref
        
        dropDown.anchorView = viewSource
        
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = AccountsArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.ChooseAccountTfref.text = item
            self?.AccountTypeTfref.text = self?.AccountsArr2[index].account_type
            self?.CurrencyTfref.text = self?.AccountsArr2[index].account_currency
            self?.selectfromAcc = self?.AccountsArr2[index].account_number ?? ""
        }
        dropDown.show()
    }
    @IBAction func Resetbtnref(_ sender: Any) {

        self.ChooseAccountTfref.text = ""
        self.AccountNumberTfref.text = ""
        self.AccountTypeTfref.text = ""
        self.CurrencyTfref.text = ""
        self.AmountTfref.text = ""
    }
    
    @IBAction func Submitbtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateTransferAccountDetails {
        case .valid:
            // self.moveToNext()
            self.TransfertoAccountMethod()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
}
extension TransferVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

//MARK:-   CLASS API HITTINGS   
extension TransferVC{
    func TransferPrefetchData(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: TransferPrefetchURL, paramaters: parameters, httpMethodType: .post, success: { (response:TransferModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                
                self.AccountsArr2 = response.payload.accounts
                //  self.PaymentMethodsArr2 = response.payload.payment_methods
                for i in 0..<response.payload.accounts.count {
                    var accountstr = String()
                    accountstr += response.payload.accounts[i].account_number
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_group
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_type_title
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_currency
                    self.AccountsArr.append(accountstr)
                }
                //                for i in 0..<response.payload.payment_methods.count {
                //                    self.PaymentMethodsArr.append(response.payload.payment_methods[i].method_name)
                //                    var imageurl = imageBaseurl + response.payload.payment_methods[i].icon
                //
                //
                //                    self.paymentIMG.append(imageurl)
                //                }
                
                // self.Tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    
    //MARK:- Create live Account func
    func TransfertoAccountMethod(){
        indicator.showActivityIndicator()
        var fromaccount_number = self.selectfromAcc
        guard let ToAccountNumber = AccountNumberTfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let Amount = AmountTfref.text else{
                   indicator.hideActivityIndicator()
                   return}
        
        let parameters = [
            "from_account_id":fromaccount_number,
            "to_account":ToAccountNumber,
            "amount":Amount
        ]
        NetworkManager.Apicalling(url: TransferAddURL, paramaters: parameters, httpMethodType: .post, success: { (response:CreateLiveAccountModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ShowAlertWithPop(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
//MARK: - Validation
extension TransferVC {
    var validateTransferAccountDetails:UserValidationState {
        if ChooseAccountTfref.text == "" || AccountNumberTfref.text ?? "" == "" || AccountTypeTfref.text ?? "" == "" || CurrencyTfref.text ?? "" == "" || AmountTfref.text ?? "" == ""{
            return .invalid(ATErrorMessage.login.required)
        }else if AccountTypeTfref.text?.isEmpty == true  {
            return .invalid("Please enter Account type")
        }else if CurrencyTfref.text?.isEmpty == true {
            return .invalid("Please enter currency")
        }else if AmountTfref.text?.isEmpty == true {
            return .invalid("Please enter your Amount")
        }else if ChooseAccountTfref.text?.isEmpty == true {
            return .invalid("Please choose your account")
        }
        
        return .valid
    }
    
    
}
