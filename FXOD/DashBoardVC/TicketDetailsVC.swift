//
//  TicketDetailsVC.swift
//  FXOD
//
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class TicketDetailsVC: UIViewController {
    //MARK:- --------- Class IBOutlets ----------
    @IBOutlet weak var Codelblref: UILabel!
    @IBOutlet weak var Reporterlblref: UILabel!
    @IBOutlet weak var Departmentlblref: UILabel!
    @IBOutlet weak var Prioritylblref: UILabel!
    @IBOutlet weak var ticketstatuslblref: UILabel!
    //MARK:- --------- Class Propeties ----------
    
    var Ticketlidtdata = Ticketdata()
    //MARK:- --------- Class View lyfe cycle ----------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true

        //code
        if let code = Ticketlidtdata.code as? String {
            self.Codelblref.text = code
        }
        //reporter
        //username
        if let userName = UserDefaults.standard.string(forKey: "name") as? String {
            self.Reporterlblref.text = userName
        }
        //Department
        if let Department = Ticketlidtdata.deptname as? String {
            self.Departmentlblref.text = Department
        }
        //priority
        if let priority = Ticketlidtdata.priority as? String {
            self.Prioritylblref.text = priority
        }
        //Ticket status
        if let ticketstatus = Ticketlidtdata.status as? String {
            self.ticketstatuslblref.text = ticketstatus
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
                 self.tabBarController?.tabBar.isHidden = true

     }
//    override func viewDidDisappear(_ animated: Bool) {
//                               self.tabBarController?.tabBar.isHidden = false
//
//             }
    //MARK:- --------- Class Button actions ----------
    
    @IBAction func backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
}
