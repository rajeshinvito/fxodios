//
//  MyTransactionVCViewController.swift
//  FXOD
//
//  Created by rajesh gandru on 6/25/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class MyTransactionVC: UIViewController,filterdataDelegate {
    
    //MARK:- --------- Class IBAction ---------
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var tableviewref: UITableView!
    
    //MARK:- -------- CLASS PROPETIES -------
    var tableViewData = [MytransactionsListdata]()
    var vcFROM = ""
    var FinalDictArr  = Dictionary<String,Array<MytransactionsListdata>>()
    var Arrkeys  = Array<String>()

    //MARK:- --------- VIEW LYFE CYCLE ---------
    override func viewDidLoad() {
            super.viewDidLoad()
          self.GetAllTransactionsList(category : "",status:"",date :"")

      }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true

        self.tabBarController?.tabBar.isHidden = true
                 // UserDefaults.standard.set(true, forKey: "menuopened")
                  //tabBarController?.reloadInputViews()
//        if vcFROM == "DashBoardVc" {
//            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
//            self.Back.backgroundColor = .white
//        }else {
//            UserDefaults.standard.set(false, forKey: "frommenu")
            self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
            self.Back.backgroundColor = .clear
       // }
        
        // Do any additional setup after loading the view.
   
        //Api calling....
        
        //self.GetAllTransactionsList(category : "",status:"",date :"")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
                         self.tabBarController?.tabBar.isHidden = false

       }
    override func viewWillDisappear(_ animated: Bool) {
           self.tabBarController?.tabBar.isHidden = false
       }
    //MARK:- ---------- CLASS BUTTON ACTION ------
    @IBAction func cmdMenu(_ sender: Any) {
//        if vcFROM == "DashBoardVc" {
//            self.popToBackVC()
//        }else {
             
//            sharedMenuClass.menuObj.addMenuClass(view: self.view)
//                      sharedMenuClass.menuObj.menuView.delegate = self
            
            Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

     //   }
    }
    
    @IBAction func filterbtnref(_ sender: Any) {
        let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        nxtVC.delegate = self
        self.navigationController?.pushViewController(nxtVC, animated: true)
        // self.movetonextvc(id: "FilterVC", storyBordid: "DashBoard")
    }
    
    func userDidEnterInformation(date_str : String,Status : String, category : String) {
           //(category : String,status:String,date :String)
        self.GetAllTransactionsList(category : category,status:Status,date :date_str)
       }
}
//MARK:- ---------- CLASS TABLEVIEW DETASOURCE AND DELEGATE  ------

extension MyTransactionVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
      //  return FinalDictArr.count
        
        
        var numOfSection: NSInteger = 0
              if FinalDictArr.count > 0
              {
                  self.tableviewref.backgroundView = nil
                  numOfSection = FinalDictArr.count
              }
              else
              {
                  var noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableviewref.bounds.size.width, height: self.tableviewref.bounds.size.height))
                  noDataLabel.text = "No Data Available"
                  noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                  noDataLabel.textAlignment = NSTextAlignment.center
                  self.tableviewref.backgroundView = noDataLabel
                  
              }
        
        return numOfSection
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionname = Arrkeys[section] as? String else { return 0 }
        return FinalDictArr[sectionname]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MyTransactionCell = tableView.dequeueReusableCell(withIdentifier: "MyTransactionCell", for: indexPath) as! MyTransactionCell
        
        let sectionname = Arrkeys[indexPath.section] as? String ?? ""
        let sectionArr = FinalDictArr[sectionname] as! Array<MytransactionsListdata>
        
        // type of transaction
        let transactionType = sectionArr[indexPath.row].type as? String

        if transactionType == "deposit" {
            cell.typeTransactionIMGref.image = #imageLiteral(resourceName: "Deposite")
            
            // transaction title
            if let transactiontitle = sectionArr[indexPath.row].to_account as? String {
                cell.TransactionTitlelblref.text = "Deposit to \(transactiontitle)"
            }
        }else if transactionType == "withdrawal" {
            cell.typeTransactionIMGref.image = #imageLiteral(resourceName: "WithDrew")
            // transaction title
            if let transactiontitle = sectionArr[indexPath.row].to_account as? String {
                cell.TransactionTitlelblref.text = "Withdrawal to \(transactiontitle)"
            }
        }else if transactionType == "transfer" {
            cell.typeTransactionIMGref.image = #imageLiteral(resourceName: "Transfer")
            if let transactiontitle = sectionArr[indexPath.row].to_account as? String {
                cell.TransactionTitlelblref.text = "Transfer to \(transactiontitle)"
            }
        }
        //transaction Date
        if let transactionDate = sectionArr[indexPath.row].payment_date as? String {
              var transdate = transactionDate.toDateString(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", ouputDateFormat: "MMM d, yyyy")
            cell.TransactionDatelblref.text  = transdate
        }
        //TransactionID
        if let TransactionID = sectionArr[indexPath.row].id as? Int {
            cell.TransactionIDlblref.text = "Reference ID : \(TransactionID)"
        }
        //TransactionStatus
        if let TransactionStatus = sectionArr[indexPath.row].status as? String {
            cell.TransactionStatuslblref.text = TransactionStatus
        }
        
        //TransactionAmount
        if let TransactionAmount = sectionArr[indexPath.row].amount as? String {
            cell.TransactionAmountlblref.text = TransactionAmount
        }
        return cell
    }
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {

        let sectionname = Arrkeys[section] as? String ?? ""
        return sectionname
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = UIColor(named: "lightweight")
            headerView.backgroundView?.backgroundColor = UIColor(named: "lightweight")
            headerView.textLabel?.textColor = UIColor(named: "#7D90AA")
             headerView.textLabel?.font = UIFont(name: "Montserrat-Regular", size: 15.0)
        }
    }
}

//MARK:- ---------- CLASS MENU ACTIONS  ------

extension MyTransactionVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}
//MARK:- ---------- CLASS API HITTINGS   ------

extension MyTransactionVC{
    func GetAllTransactionsList(category : String,status:String,date :String){
        indicator.showActivityIndicator()
        //?category=&status =&date =
        let nexturl = "?category=\(category)&status=\(status)&date=\(date)"
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: AllTransactionURL + nexturl, paramaters: parameters, httpMethodType: .get, success: { (response:MytransactionsListModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.tableViewData = response.payload
                var Arrpayment_date  = Array<String>()
                var ContentArray = [MytransactionsListdata]()
                
                for i in 0..<self.tableViewData.count{
                    
                    if let transactionDate = self.tableViewData[i].payment_date as? String {
                        var transdate = transactionDate.toDateString(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", ouputDateFormat: "MMMM yyyy")
                        print(transdate)
                        let contains = self.Arrkeys.contains(where: { $0 == transdate })
                        if !(contains) {
                            self.Arrkeys.append(transdate)
                            Arrpayment_date.append(transactionDate)
                        }else {
                            Arrpayment_date.append(transactionDate)
                        }
                    }
                    
                }
                var arrInt = [Int]()
                var arrInt2 = Int()

                for k in 0..<self.tableViewData.count{
                    if let transactionDate = self.tableViewData[k].payment_date as? String {
                        var transdate = transactionDate.toDateString(inputDateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", ouputDateFormat: "MMMM yyyy")
                        print(transdate)
                        
                        for j in 0..<self.Arrkeys.count {
                            if transdate == self.Arrkeys[j] {
                                if !(arrInt.contains(j)){
                                    arrInt.append(j)
                                    arrInt2 = j
                                }
                                var Dict  = MytransactionsListdata()
                                Dict = self.tableViewData[k]
//                                Dict["id"] = self.tableViewData[k].id
//                                Dict["client"] = self.tableViewData[k].client
//                                Dict["account_number"] = self.tableViewData[k].account_number
//                                Dict["to_account"] = self.tableViewData[k].to_account
//                                Dict["payment_date"] = transdate
//                                Dict["amount"] = self.tableViewData[k].amount
//                                Dict["payment_method"] = self.tableViewData[k].payment_method
//                                Dict["status"] = self.tableViewData[k].status
//                                Dict["type"] = self.tableViewData[k].type
                                ContentArray.append(Dict)
                                self.FinalDictArr.updateValue(ContentArray, forKey: self.Arrkeys[j])
                                
                            }else {
                                
                                if arrInt.contains(j) {
                                    if arrInt2 <= j {
                                        ContentArray.removeAll()
                                    }
                                }
                            }
                            
                        }
                        
                        
                    }
                    print(self.Arrkeys)
                }

                print(self.FinalDictArr)
                self.tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
}
extension String {
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = outputFormat
        if date != nil{
            return dateFormatter.string(from: date!)
        }
        return "\(Date())"
    }
}
