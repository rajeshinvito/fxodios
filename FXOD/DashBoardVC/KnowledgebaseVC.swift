//
//  KnowledgebaseVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class KnowledgebaseVC: UIViewController {

    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var tableviewref: UITableView!

    var knowledgebasearr = [KnowbasePrefetchdata]()
    var knowledgebase2arr = [KnowbasePrefetchseconddata]()

    var vcFROM = ""
    
    var urltype = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true

        self.urltype = ""
      //  if vcFROM == "DashBoardVc" {
            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
       // }else {
//            self.Back.setImage(#imageLiteral(resourceName: "Menu"), for: .normal)
//
//        }
        //Api calling...
        self.GetKnowledgecategoriesBaseList()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
                self.tabBarController?.tabBar.isHidden = true

    }
    override func viewDidDisappear(_ animated: Bool) {
                      self.tabBarController?.tabBar.isHidden = false

    }
    @IBAction func cmdMenu(_ sender: Any) {
        if urltype == "second" {
            //Api calling...
            self.GetKnowledgecategoriesBaseList()
        }else{
        //    if vcFROM == "DashBoardVc" {
                self.popToBackVC()
//            }else {
//                //            sharedMenuClass.menuObj.addMenuClass(view: self.view)
//                //            sharedMenuClass.menuObj.menuView.delegate = self
//                Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
//
//            }
        }
    }
  
}
extension KnowledgebaseVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

extension KnowledgebaseVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if self.urltype == "first" {
        return knowledgebasearr.count
         }else {
            return knowledgebase2arr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:KnowledgebaseCell = tableView.dequeueReusableCell(withIdentifier: "KnowledgebaseCell", for: indexPath) as! KnowledgebaseCell
         if self.urltype == "first" {
        cell.tittlelblref.text = knowledgebasearr[indexPath.row].name
         }else {
            cell.tittlelblref.text = knowledgebase2arr[indexPath.row].subject

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.urltype == "first" {
            self.GetKnowledgeBaseList(id: "\(knowledgebasearr[indexPath.row].id ?? 0)")
        }
    }
}
extension KnowledgebaseVC  {
    func GetKnowledgecategoriesBaseList(){
           indicator.showActivityIndicator()
           let parameters = [
               "":""
           ]
           NetworkManager.Apicalling(url: KnowldgecategoriesbaseURL, paramaters: parameters, httpMethodType: .get, success: { (response:KnowbasePrefetchModel) in
               print(response.payload)
               if response.status == 1 {
                   indicator.hideActivityIndicator()
                self.urltype = "first"
                   self.knowledgebasearr = response.payload
                  
                
                if self.urltype == "second" {
                    let transition = CATransition()
                                        transition.type = CATransitionType.push
                                        transition.subtype = CATransitionSubtype.fromRight
                                        self.tableviewref.reloadData()
                                        self.tableviewref.layer.add(transition, forKey: nil)
                }else {
                   self.tableviewref.reloadData()
                }
               }else {
                   indicator.hideActivityIndicator()
                   self.ShowAlert(message: response.message)
               }
           }) { (errorMsg) in
               
               indicator.hideActivityIndicator()
               if let err = errorMsg as? String{
                   self.ShowAlert(message: err)
               }
           }
       }
    func GetKnowledgeBaseList(id : String){
              indicator.showActivityIndicator()
              let parameters = [
                  "category_id":id
              ]
              NetworkManager.Apicalling(url: KnowldgebaseURL, paramaters: parameters, httpMethodType: .post, success: { (response:KnowbasePrefetchsecondModel) in
                  print(response.payload)
                  if response.status == 1 {
                      indicator.hideActivityIndicator()
                   self.urltype = "second"
                      self.knowledgebase2arr = response.payload
                    
                    let transition = CATransition()
                          transition.type = CATransitionType.push
                          transition.subtype = CATransitionSubtype.fromRight
                          self.tableviewref.reloadData()
                          self.tableviewref.layer.add(transition, forKey: nil)
                  }else {
                      indicator.hideActivityIndicator()
                      self.ShowAlert(message: response.message)
                  }
              }) { (errorMsg) in
                  
                  indicator.hideActivityIndicator()
                  if let err = errorMsg as? String{
                      self.ShowAlert(message: err)
                  }
              }
          }
}
