
//
//  TimerTabVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class CommonChatVC: UIViewController {
    @IBOutlet weak var Back: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

          self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
  
      
      
    
    
    //MARK:------- class button actions -----------
       @IBAction func cmdMenu(_ sender: Any) {
//               self.tabBarController?.tabBar.isHidden = true
//               UserDefaults.standard.set(true, forKey: "menuopened")
//               sharedMenuClass.menuObj.addMenuClass(view: self.view)
//               sharedMenuClass.menuObj.menuView.delegate = self
        Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

          
       }
}
//MARK:---------- Menu Action ------------
extension CommonChatVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}
