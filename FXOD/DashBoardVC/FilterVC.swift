//
//  FilterVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/25/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import FSCalendar


protocol filterdataDelegate: class {
    func userDidEnterInformation(date_str : String,Status : String, category : String)
}
class FilterVC: UIViewController{
    
    //MARK:- --- Class IBOUTLEt ---------------
    @IBOutlet weak var backbtnref: UIButton!
    @IBOutlet weak var RejectBtnref: UIButton!
    @IBOutlet weak var Waitingbtnref: UIButton!
    @IBOutlet weak var approvedbtnref: UIButton!
    @IBOutlet weak var Transferviewref: UIView!
    @IBOutlet weak var DepositViewref: UIView!
    @IBOutlet weak var Withdrawlviewref: UIView!
    @IBOutlet weak var calendar: FSCalendar!
    //MARK:- ------ Class Properties -----------------
    var reject = true
    var waiting = true
    var approve = true
    
    var viewStatus = 0
    weak var delegate: filterdataDelegate? = nil
    
    var selecteddate = String()
    var status = String()
    var category = String()

    //MARk:----  view lyfe cycle  ---------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

      //  self.tabBarController?.tabBar.isHidden = true
          calendar.appearance.headerMinimumDissolvedAlpha = 0

        // Do any additional setup after loading the view.
    }
    
    //MARk:---- button actions   ---------------
    
    @IBAction func cmdMenu(_ sender: Any) {
        self.popToBackVC()
    }
    
    
    @IBAction func ApprovedBntref(_ sender: Any) {
        self.approvedbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.RejectBtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.Waitingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.status = "completed"
    }
    
    @IBAction func Waitingbtnref(_ sender: Any) {
        self.Waitingbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.RejectBtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.approvedbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.status = "pending"
    }
    
    @IBAction func Rejectedbtnref(_ sender: Any) {
        self.RejectBtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.Waitingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.approvedbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.status = "rejected"
    }
    
    
    @IBAction func transferbtnref(_ sender: Any) {
        self.Transferviewref.borderColor = UIColor(named: "#186BFE")
        self.DepositViewref.borderColor = UIColor(named: "#7D90AA")
        self.Withdrawlviewref.borderColor = UIColor(named: "#7D90AA")
        self.category = "transfer"
        
    }
    
    
    @IBAction func Depositbtnref(_ sender: Any) {
        self.Transferviewref.borderColor = UIColor(named: "#7D90AA")
        self.DepositViewref.borderColor = UIColor(named: "#186BFE")
        self.Withdrawlviewref.borderColor = UIColor(named: "#7D90AA")
        self.category = "deposit"

    }
    
    
    @IBAction func withdrawlbtnref(_ sender: Any) {
        self.Transferviewref.borderColor = UIColor(named: "#7D90AA")
        self.DepositViewref.borderColor = UIColor(named: "#7D90AA")
        self.Withdrawlviewref.borderColor = UIColor(named: "#186BFE")
        self.category = "withdrawal"

    }
    
    
    @IBAction func Conformbtnref(_ sender: Any) {
        
        
        /*38. -category  - //deposit/withdrawal/transfer
        39. -status - //pending/completed/rejected
        40. -date - (format: 2020-05-15 - Y-m-d)*/
        delegate?.userDidEnterInformation(date_str : self.selecteddate,Status : self.status, category : self.category)

        self.popToBackVC()
    }
    
    @IBAction func nextTapped(_ sender:UIButton) {
        calendar.setCurrentPage(getPreviousMonth(date: calendar.currentPage), animated: true)

    }

    @IBAction  func previousTapped(_ sender:UIButton) {
        calendar.setCurrentPage(getNextMonth(date: calendar.currentPage), animated: true)
    }

    func getNextMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: 1, to:date)!
    }

    func getPreviousMonth(date:Date)->Date {
        return  Calendar.current.date(byAdding: .month, value: -1, to:date)!
    }
    
    
}
extension FilterVC :FSCalendarDelegate,FSCalendarDataSource{
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
       // Do the same inside this function and you should be fine
        print(date)
       // let dateString =  Stringfro stringFromDate(date as Date)
//        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
//        print("selected dates is \(selectedDates)"
        
            self.selecteddate = convertDateFormatter(datestr: date)
    }
    
    
    func convertDateFormatter(datestr: Date) -> String
    {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = dateFormatter.string(from: datestr)


        dateFormatter.dateFormat = "yyyy-MM-dd"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let timeStamp = dateFormatter.string(from: datestr)


        return timeStamp
    }
}
