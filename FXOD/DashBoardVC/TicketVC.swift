//
//  TicketVC.swift
//  FXOD
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class TicketVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK:- ----- Class IBOutlets --------
    @IBOutlet weak var tableviewref: UITableView!
    @IBOutlet weak var backbtnref: UIButton!
    
    //MARK:- ----- Class Propeties --------
    
    var Ticketlidtdata = [Ticketdata]()
    //MARK:- ----- Class View lyfe cycles --------
    override func viewDidLoad() {
        super.viewDidLoad()

        //Api calling....
        self.GetTicketDetailsList()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
                self.tabBarController?.tabBar.isHidden = true

    }
//    override func viewDidDisappear(_ animated: Bool) {
//                         self.tabBarController?.tabBar.isHidden = false
//
//       }
    //MARK:- ----- Class Button Actions --------
    @IBAction func backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    
    @IBAction func addticketbtnref(_ sender: Any) {
        self.movetonextvc(id: "AddTicketVC", storyBordid: "DashBoard")
    }
    
    
}
//MARK:- ----- Class tableview Delegate and datasource --------
extension TicketVC {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Ticketlidtdata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TicketViewCell = tableView.dequeueReusableCell(withIdentifier: "TicketViewCell", for: indexPath) as! TicketViewCell
        //ticket title
        if let tickettitle = Ticketlidtdata[indexPath.row].code as? String {
            cell.TicketTitleref.text = tickettitle
        }
        //Subject
        if let subject = Ticketlidtdata[indexPath.row].subject as? String {
            cell.subjectlblref.text = subject
        }
        //Priority
        if let priority = Ticketlidtdata[indexPath.row].priority as? String {
            cell.prioritylblref.text = priority
        }
        //Department
        if let department = Ticketlidtdata[indexPath.row].deptname as? String {
            cell.Departmentlblref.text = department
        }
        //Created date
        if let CreatedDate = Ticketlidtdata[indexPath.row].created_at as? String {
            cell.CreatedDatelblref.text = CreatedDate
        }
        //Due Date
        if let DueDate = Ticketlidtdata[indexPath.row].due_date as? String {
            cell.Duedatelblref.text = DueDate
        }
        
        //response_status
        if let response_status = Ticketlidtdata[indexPath.row].response_status as? String {
            cell.Ticketstatuslblref.text = response_status
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  self.movetonextvc(id: "TicketDetailsVC", storyBordid: "DashBoard")
        self.movetofromTicketlist(Ticket_data : Ticketlidtdata[indexPath.row])
    }
}
//TicketListModel
extension TicketVC {
    //MARK:-   CLASS GET LIVE ACCOUNT LIST API 
    
    func GetTicketDetailsList(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: ticketListURL, paramaters: parameters, httpMethodType: .get, success: { (response:TicketListModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.Ticketlidtdata = response.payload
                self.tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
