//
//  tabbarVCViewController.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class tabbarVCViewController: UITabBarController,UITabBarControllerDelegate{
   let coustmeTabBarView:UIView = {
        
        //  daclare coustmeTabBarView as view
        let view = UIView(frame: .zero)
        
        // to make the cornerRadius of coustmeTabBarView
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.clipsToBounds = true
        
        // to make the shadow of coustmeTabBarView
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: -8.0)
        view.layer.shadowOpacity = 0.12
        view.layer.shadowRadius = 10.0
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
        if UserDefaults.standard.bool(forKey: "menuopened") {}else {
//        addcoustmeTabBarView()
//        hideTabBarBorder()
        }
        self.delegate = self


    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let tabBarIndex = self.selectedIndex
        print(tabBarIndex)
        self.tabBar.isHidden = false

//        if tabBarIndex == 0 {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                  let tabBarController = appDelegate.tabBarController
//                  tabBarController?.selectedIndex = tabBarIndex
//                  revealViewController().pushFrontViewController(tabBarController, animated: true)
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
   /*
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let nav1 = UINavigationController()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        
        //First VC
        let first: DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        nav1.viewControllers = [first]
        nav1.setNavigationBarHidden(true, animated: false)
        nav1.title = "first"
        let item = UITabBarItem()
        // item.title = "Home"
        item.image = UIImage(named: "Home")
        nav1.tabBarItem = item
        
        //Second VC
        let nav2 = UINavigationController()
        let second: MyAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
        nav2.viewControllers = [second]
        nav2.setNavigationBarHidden(true, animated: false)
        let item2 = UITabBarItem()
        // item2.title = "Home"
        item2.image = UIImage(named: "MyAccount")
        nav2.tabBarItem = item2
        
        //third VC
        let nav3 = UINavigationController()
        let third: DepositVC = mainStoryboard.instantiateViewController(withIdentifier: "DepositVC") as! DepositVC
        nav3.viewControllers = [third]
        nav3.setNavigationBarHidden(true, animated: false)
        let item3 = UITabBarItem()
        // item3.title = "Home"
        item3.image = UIImage(named: "Deposit")
        nav3.tabBarItem = item3
        
        //third VC
        let nav4 = UINavigationController()
        let fourth: CommonChatVC = mainStoryboard.instantiateViewController(withIdentifier: "CommonChatVC") as! CommonChatVC
        nav4.viewControllers = [fourth]
        nav4.setNavigationBarHidden(true, animated: false)
        let item4 = UITabBarItem()
        // item4.title = "Home"
        item4.image = UIImage(named: "MyTransactions")
        nav4.tabBarItem = item4
        
        
        
        print(viewController)
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            //do your stuff
            let tabController = UITabBarController()
            tabController.viewControllers = [nav1,nav2,nav3,nav4]
            tabController.selectedIndex = tabBarIndex
            
            appDelegate.window!.rootViewController = tabController
            appDelegate.window?.makeKeyAndVisible()            //NotificationCenter.default.post(name: Notification.Name("NotificationtoDashBoardVC"), object: nil)
            
        }else if tabBarIndex == 1 {
            let tabController = UITabBarController()
            tabController.viewControllers = [nav1,nav2,nav3,nav4]
            tabController.selectedIndex = tabBarIndex
            
            appDelegate.window!.rootViewController = tabController
            appDelegate.window?.makeKeyAndVisible()
            // NotificationCenter.default.post(name: Notification.Name("NotificationtoAccountVC"), object: nil)
            
        }else if tabBarIndex == 2 {
            let tabController = UITabBarController()
            tabController.viewControllers = [nav1,nav2,nav3,nav4]
            tabController.selectedIndex = tabBarIndex
            
            appDelegate.window!.rootViewController = tabController
            appDelegate.window?.makeKeyAndVisible()
            NotificationCenter.default.post(name: Notification.Name("NotificationtoDiposite"), object: nil)
            
        }else {
            let tabController = UITabBarController()
            tabController.viewControllers = [nav1,nav2,nav3,nav4]
            tabController.selectedIndex = tabBarIndex
            
            appDelegate.window!.rootViewController = tabController
            appDelegate.window?.makeKeyAndVisible()
            NotificationCenter.default.post(name: Notification.Name("NotificationtoCommonChatVC"), object: nil)
            
        }
        */
        //print(tabBarIndex)
    }

    
   
    override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
        // coustmeTabBarView.frame = tabBar.frame
      }
    
    override func viewDidAppear(_ animated: Bool) {
//        var newSafeArea = UIEdgeInsets()
//
//        // Adjust the safe area to the height of the bottom views.
//        newSafeArea.bottom += coustmeTabBarView.bounds.size.height
//
//        // Adjust the safe area insets of the
//        //  embedded child view controller.
//        self.children.forEach({$0.additionalSafeAreaInsets = newSafeArea})
    }

    private func addcoustmeTabBarView() {
        //
       coustmeTabBarView.frame = tabBar.frame
        view.addSubview(coustmeTabBarView)
        view.bringSubviewToFront(self.tabBar)
    }
    
    
    func hideTabBarBorder()  {
        let tabBar = self.tabBar
        tabBar.backgroundImage = UIImage.from(color: .clear)
        tabBar.shadowImage = UIImage()
        tabBar.clipsToBounds = true

    }
    

}






extension UIImage {
    static func from(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}
