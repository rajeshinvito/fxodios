//
//  CreateLiveAccountVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/18/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DropDown

class CreateLiveAccountVC: UIViewController {
    @IBOutlet weak var AccounttypeTFref: UITextField!
    @IBOutlet weak var Currencytfref: UITextField!
    @IBOutlet weak var Leveragetfref: UITextField!
    @IBOutlet weak var MasterPasswordtfref: UITextField!
    @IBOutlet weak var InvesterPasswordtfref: UITextField!
    @IBOutlet weak var Submitbtnref: UIButton!
    @IBOutlet weak var TermsCheckbtnref: UIButton!
    @IBOutlet weak var Depositeaccbtnref: UIButton!
    @IBOutlet weak var btnChooseAccount: UIButton!
    @IBOutlet weak var btnChooseLeverage: UIButton!
    
    var istermsCheckbtn = true
    var isDepositeCheckbtn = true
    var dropDown = DropDown()
    var AccountsArr = ["Account Type"]
    var AccountsIDSArr = [0]
    var selectedID = Int()
    var LeverageArr = ["Leverage"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        // Auto gen Passwords
        let len = 8
        let pswdChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        let rndPswd = String((0..<len).compactMap{ _ in pswdChars.randomElement() })
        self.MasterPasswordtfref.text = rndPswd
        
        let rndPswd2 = String((0..<len).compactMap{ _ in pswdChars.randomElement() })
        self.InvesterPasswordtfref.text = rndPswd2
        // Do any additional setup after loading the view.
        
        
        //Api Calling....
        self.LivePrefetchData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    @IBAction func Backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    
    @IBAction func Submitbtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateCreateLiveAccountDetails {
        case .valid:
            // self.moveToNext()
            self.CreateLiveAccount()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    @IBAction func Resetbtnref(_ sender: Any) {
        self.Depositeaccbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.TermsCheckbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.AccounttypeTFref.text = ""
        self.Leveragetfref.text = ""
        // Auto gen Passwords
        let len = 8
        let pswdChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        let rndPswd = String((0..<len).compactMap{ _ in pswdChars.randomElement() })
        self.MasterPasswordtfref.text = rndPswd
        
        let rndPswd2 = String((0..<len).compactMap{ _ in pswdChars.randomElement() })
        self.InvesterPasswordtfref.text = rndPswd2
    }
    
    @IBAction func Termscheckbtnref(_ sender: Any) {
        if istermsCheckbtn {
            istermsCheckbtn = false
            self.TermsCheckbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        }else {
            istermsCheckbtn = true
            self.TermsCheckbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
            
        }
    }
    
    @IBAction func DepositeAccountbtnref(_ sender: Any) {
        if isDepositeCheckbtn {
            isDepositeCheckbtn = false
            self.Depositeaccbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        }else {
            isDepositeCheckbtn = true
            self.Depositeaccbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        }
    }
    
    
    @IBAction func cmdAccount(_ sender: Any) {
        let viewSource = btnChooseAccount
        
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = AccountsArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.AccounttypeTFref.text = item
            self?.selectedID = self?.AccountsIDSArr[index] ?? 0
            
        }
        dropDown.show()
        
    }
    
    
    @IBAction func cmdLeverage(_ sender: Any) {
        let viewSource = btnChooseLeverage
        
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = LeverageArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            
            
            
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.Leveragetfref.text = item
            
        }
        dropDown.show()
        
    }
    
    
    
    
}
//MARK:-   CLASS API HITTINGS   
extension CreateLiveAccountVC{
    func LivePrefetchData(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: LivePrefetchDataURl, paramaters: parameters, httpMethodType: .get, success: { (response:LiveAccountPrefetchModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                for i in 0..<response.payload.count {
                    for j in 0..<response.payload[i].leverage.count {
                        self.LeverageArr.append(response.payload[i].leverage[j])
                    }
                    self.AccountsArr.append(response.payload[i].title)
                    self.AccountsIDSArr.append(response.payload[i].id)
                    self.Currencytfref.text = response.payload[i].currency
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    //MARK:- Create live Account func
    func CreateLiveAccount(){
        indicator.showActivityIndicator()
        guard let leverage = Leveragetfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let investerpass = InvesterPasswordtfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let masterpass = MasterPasswordtfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let currency = Currencytfref.text else{
            indicator.hideActivityIndicator()
            return}
        
        let parameters = [
            "account_type_id":"\(selectedID)",
            "account_currency":currency,
            "master_password":masterpass,
            "investor_password":investerpass,
            "leverage":leverage
            //account_balance:
        ]
        NetworkManager.Apicalling(url: CreateLiveAccountURl, paramaters: parameters, httpMethodType: .post, success: { (response:CreateLiveAccountModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                
                self.ShowAlertWithPop(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    
}
//MARK: - Validation
extension CreateLiveAccountVC {
    var validateCreateLiveAccountDetails:UserValidationState {
        if Leveragetfref.text == "" || InvesterPasswordtfref.text ?? "" == "" || MasterPasswordtfref.text ?? "" == "" || Currencytfref.text ?? "" == "" || AccounttypeTFref.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if Leveragetfref.text?.isEmpty == true  {
            return .invalid("Please enter Leverage")
        }else if InvesterPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter InvesterPassword")
        }else if MasterPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter MasterPassword")
        }else if Currencytfref.text?.isEmpty == true {
            return .invalid("Please enter Currency")
        }else if AccounttypeTFref.text?.isEmpty == true {
            return .invalid("Please enter AccountType")
        }
        
        return .valid
    }
    
    
}
