//
//  IBAccountVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class IBAccountVC: UIViewController {

       @IBOutlet weak var Back: UIButton!
    
    var vcFROM = ""
    override func viewDidLoad() {
            super.viewDidLoad()
          
      }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true

       if vcFROM == "DashBoardVc" {
            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
            self.Back.backgroundColor = .white
        }else {
        UserDefaults.standard.set(false, forKey: "frommenu")
            self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
            self.Back.backgroundColor = .clear
        }

      //  self.tabBarController?.tabBar.isHidden = true
                  UserDefaults.standard.set(true, forKey: "menuopened")
                  tabBarController?.reloadInputViews()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cmdMenu(_ sender: Any) {
        if vcFROM == "DashBoardVc" {
            self.popToBackVC()
        }else {
//               self.tabBarController?.tabBar.isHidden = true
//                   UserDefaults.standard.set(true, forKey: "menuopened")
//                   self.tabBarController?.reloadInputViews()
//            sharedMenuClass.menuObj.addMenuClass(view: self.view)
//                      sharedMenuClass.menuObj.menuView.delegate = self
            Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

        }
    }

}
extension IBAccountVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

