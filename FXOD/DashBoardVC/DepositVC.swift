//
//  DepositVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import IQKeyboardManager
import MobileCoreServices
import CropViewController
class DepositVC: UIViewController {
    
    //MARK:- ------- Class IBOutlets ----------
    @IBOutlet weak var Back: UIButton!
    @IBOutlet var btnChoosePaymentMethod: UIButton!
    @IBOutlet var btnChooseAccount: UIButton!
    @IBOutlet var ChooseAccountTF: UITextField!
    @IBOutlet var ChoosePaymentTF: UITextField!
    @IBOutlet weak var Tableviewref: UITableView!
    
    @IBOutlet weak var popUpViewref: UIView!
    
    @IBOutlet weak var Commenttextviewref: IQTextView!
    @IBOutlet weak var Amounttfref: UITextField!
    @IBOutlet weak var UploadFilesImgref: UIImageView!
    //MARK:--------- class propeties ----------
    
    var AccountsArr = [String]()
    var AccountsArr2 = [accountsdata]()
    var PaymentMethodsArr2 = [payment_methodsdata]()
    var selectedPaymentID = Int()
    var selectedAccountNumber = String()
    
    var PaymentMethodsArr = [String]()
    
    // var PaymentMethodsArr = ["Neteller","Bitcoin","Bank Transfer","Credit Card","Skrill"]
    var paymentIMG = [String]()
    
    var dropDown = DropDown()
    var vcFROM = ""
    var we  = "n"
    
    var imagePicked :Bool = false
    var imagePicker = UIImagePickerController()
    var filetype = ""
    var pickedImage = UIImage()
    
    //MARK:------- class view lyfe cycle -----------
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        self.popUpViewref.isHidden = true
        imagePicker.delegate = self
        
        if vcFROM == "DashBoardVc" {
            self.Back.isHidden = false
            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
            self.Back.backgroundColor = .white
        }else {
            self.Back.isHidden = false
            self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
            self.Back.backgroundColor = .clear
        }
        self.Tableviewref.rowHeight = 50
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationtoDiposite"), object: nil)
        
        
        //API Calling.....
        self.DepositPrefetchData()
        
    }
   
    @objc func methodOfReceivedNotification(notification: Notification) {
       self.navigationController?.navigationBar.isHidden = true
       self.tabBarController?.tabBar.isHidden = false
        
    }
    
    
    //MARK:------- class button actions -----------
     @IBAction func cmdMenu(_ sender: Any) {
           if vcFROM == "DashBoardVc" {
               self.popToBackVC()
           }else {
             //  self.tabBarController?.tabBar.isHidden = true
               UserDefaults.standard.set(true, forKey: "menuopened")
//               sharedMenuClass.menuObj.addMenuClass(view: self.view)
//               sharedMenuClass.menuObj.menuView.delegate = self
            Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

           }
       }
    
    @IBAction func Popupbtnref(_ sender: Any) {
        self.popUpViewref.isHidden = true
        
    }
    
    @IBAction func cmdAccount(_ sender: Any) {
        let viewSource = btnChooseAccount
        
        dropDown.anchorView = viewSource
        we  = "y"
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = AccountsArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            
            
            
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.ChooseAccountTF.text = item
            self?.selectedAccountNumber = self?.AccountsArr2[index].account_number ?? ""
        }
        dropDown.show()
        
    }
    
    
    @IBAction func cmdChoosePaymentMethod(_ sender: Any) {
        self.popUpViewref.isHidden = false
    }
    
    @IBAction func UploadFilesbtnref(_ sender: Any) {
        self.showActionSheet()
    }
    
    @IBAction func Dipositebtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateDepositeAccountDetails {
        case .valid:
            // self.moveToNext()
            self.DiposittoAccountMethod()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
        
    }
    
    @IBAction func ResetBtnref(_ sender: Any) {
        self.ChooseAccountTF.text = ""
        self.ChoosePaymentTF.text = ""
        self.Amounttfref.text = ""
        self.Commenttextviewref.text = ""
        self.selectedPaymentID = 0
        self.selectedAccountNumber = ""
        self.UploadFilesImgref.image = UIImage(named: "Uploadfiles")
    }
    
}

//MARK:---------- Menu Action ------------
extension DepositVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

//MARK:-   CLASS API HITTINGS   
extension DepositVC{
    func DepositPrefetchData(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: DepositPrefetchURL, paramaters: parameters, httpMethodType: .post, success: { (response:DepositModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.AccountsArr2 = response.payload.accounts
                self.PaymentMethodsArr2 = response.payload.payment_methods
                for i in 0..<response.payload.accounts.count {
                    var accountstr = String()
                    accountstr += response.payload.accounts[i].account_number
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_group
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_type_title
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_currency
                    self.AccountsArr.append(accountstr)
                }
                for i in 0..<response.payload.payment_methods.count {
                    self.PaymentMethodsArr.append(response.payload.payment_methods[i].method_name)
                    var imageurl = imageBaseurl + response.payload.payment_methods[i].icon
                    
                    
                    self.paymentIMG.append(imageurl)
                }
                
                self.Tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    //MARK:- Create live Account func
    func DiposittoAccountMethod(){
        indicator.showActivityIndicator()
        var account_number = self.selectedAccountNumber
        guard let amount = Amounttfref.text else{
            indicator.hideActivityIndicator()
            return}
        var payment_method_id = "\(self.selectedPaymentID)"
        
        guard let note = Commenttextviewref.text else{
            indicator.hideActivityIndicator()
            return}
        
        let parameters = [
            "account_number":account_number,
            "amount":amount,
            "payment_method_id":payment_method_id,
            "note":note,
            
        ]
        NetworkManager.Apicalling(url: DepositAddURL, paramaters: parameters, httpMethodType: .post, success: { (response:CreateLiveAccountModel) in
            //        NetworkManager.MultiformApicalling(url: DepositPrefetchURL, paramaters: parameters, Images: [self.UploadFilesImgref.image!], Imagesname: "receipt", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ShowAlertWithPop(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
extension DepositVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PaymentMethodsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DepositPaymenttypeCell = tableView.dequeueReusableCell(withIdentifier: "DepositPaymenttypeCell", for: indexPath) as! DepositPaymenttypeCell
        if let ProfilePic = self.paymentIMG[indexPath.row] as? String {
            cell.Imgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
        }
        cell.titleLblref.text = PaymentMethodsArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.popUpViewref.isHidden = true
        self.ChoosePaymentTF.text = PaymentMethodsArr[indexPath.row]
        self.selectedPaymentID = self.PaymentMethodsArr2[indexPath.row].id
    }
}

extension DepositVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentPickerDelegate,UIDocumentMenuDelegate{
    fileprivate func showActionSheet(){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose a file ", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.clickFunction()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func takePhotoFromCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func takePhotoFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            
        }
    }
    
    //MARK: ImagePicker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // let imageData = pickedImage.pngData()
            // let imageData = pickedImage.jpegData(compressionQuality: 1.0)
            let cropViewController = CropViewController(image: pickedImage)
            
            cropViewController.delegate = self
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.aspectRatioPreset = .preset5x4
            
            //  cropViewController.aspectra
            
            self.navigationController?.pushViewController(cropViewController, animated: true)
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func clickFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}

//MARK:- CropViewControllerDelegate Methods
extension DepositVC : CropViewControllerDelegate{
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        self.imagePicked = true
        
        self.UploadFilesImgref.image = image
        //  self.sendTicketrequest(image : image)
        
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: - Validation
extension DepositVC {
    var validateDepositeAccountDetails:UserValidationState {
        if ChooseAccountTF.text == "" || ChoosePaymentTF.text ?? "" == "" || Amounttfref.text ?? "" == "" || Commenttextviewref.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if ChooseAccountTF.text?.isEmpty == true  {
            return .invalid("Please Choose Account Number")
        }else if ChoosePaymentTF.text?.isEmpty == true {
            return .invalid("Please Choose Paymenttype")
        }else if Amounttfref.text?.isEmpty == true {
            return .invalid("Please enter your Amount")
        }else if Commenttextviewref.text?.isEmpty == true {
            return .invalid("Please enter your commit")
        }
        
        return .valid
    }
    
    
}
