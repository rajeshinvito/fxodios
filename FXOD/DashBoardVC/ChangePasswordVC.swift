//
//  ChangePasswordVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import SDWebImage
import CropViewController
class ChangePasswordVC: UIViewController {
    
    
    
    @IBOutlet weak var Profilepicimgref: UIImageView!
    @IBOutlet weak var CurrentPasswodTfref: UITextField!
    @IBOutlet weak var NewPasswordtfref: UITextField!
    @IBOutlet weak var ConformNewPasswordtfref: UITextField!
    
    
    var imagePicked :Bool = false
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(true, forKey: "menuopened")
               self.tabBarController?.reloadInputViews()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func Profilebtnref(_ sender: Any) {
        self.movetonextvcwithAnimationfalse(id: "profileVC", storyBordid: "DashBoard")
    }
    @IBAction func ChangeDocumentbtnref(_ sender: Any) {
        self.movetonextvcwithAnimationfalse(id: "uploadDocumentVC", storyBordid: "DashBoard")
    }
    @IBAction func cmdMenu(_ sender: Any) {
        
        //  self.popToBackVC()
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func imagepickerbntref(_ sender: Any) {
        self.showActionSheet()
    }
    
    @IBAction func ChnagePasswordtfref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch ValidatingPasswordupdate {
        case .valid:
            // self.moveToNext()
            self.ChangePassword()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    @IBAction func ResetBtnref(_ sender: Any) {
        
    }
    
}

extension ChangePasswordVC {
    
    func ChangePassword(){
        
        indicator.showActivityIndicator()
        //Name
        guard let CurrentPasswod = CurrentPasswodTfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Phone
        guard let NewPassword = NewPasswordtfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Address
        guard let ConformNewPassword = ConformNewPasswordtfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        
        
        let parameters = [
            "current_password":CurrentPasswod,
            "new_password":NewPassword,
            "new_password_confirmation":ConformNewPassword
        ]
        NetworkManager.Apicalling(url: ChangePasswordUrl, paramaters: parameters, httpMethodType: .post, success: { (response:ProfileUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: profileVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                }
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
}


extension ChangePasswordVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //    @IBAction func imagepickerbntref(_ sender: Any) {
    //        self.showActionSheet()
    //    }
    //MARK:- UIimagePickercontroller Delegate Methods
    
    fileprivate func showActionSheet (){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func takePhotoFromCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func takePhotoFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            
        }
    }
    
    //MARK: ImagePicker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // let imageData = pickedImage.pngData()
            // let imageData = pickedImage.jpegData(compressionQuality: 1.0)
            let cropViewController = CropViewController(image: pickedImage)
            cropViewController.delegate = self
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.aspectRatioPreset = .preset5x4
            //  cropViewController.aspectra
            
            self.navigationController?.pushViewController(cropViewController, animated: true)
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- CropViewControllerDelegate Methods
extension ChangePasswordVC : CropViewControllerDelegate{
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        self.Profilepicimgref.image = image
        self.imagePicked = true
        //  self.sendTicketrequest(image : image)
        self.sendProfilePic()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func sendProfilePic(){
        indicator.showActivityIndicator()
        
        let parameters = [
            "":""
        ]
        NetworkManager.MultiformApicalling(url: ProfilepicUpdateUrl, paramaters: parameters, Images: [self.Profilepicimgref.image!], Imagesname: "avatar", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //Profile Pic
                if let ProfilePic = response.payload.avatar as? String {
                    self.Profilepicimgref.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.Profilepicimgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
        
    }
    
}
//MARK: - Validation
extension ChangePasswordVC {
    
    var ValidatingPasswordupdate:UserValidationState {
        if CurrentPasswodTfref.text == "" || NewPasswordtfref.text ?? "" == "" || ConformNewPasswordtfref.text ?? "" == ""  {
            return .invalid(ATErrorMessage.login.required)
        }else if CurrentPasswodTfref.text?.isEmpty == true  {
            return .invalid("Please enter your current Password")
        }else if NewPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter your New password")
        }else if ConformNewPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter your Conform New Password")
        }
        
        return .valid
    }
    
    
}
