//
//  SliderMenuVCViewController.swift
//  FXOD
//
//  Created by rajesh gandru on 7/10/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import SDWebImage
//import AARatingBar
class SliderMenuVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var tableview_MenusliderRef: UITableView!
  
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!

    @IBOutlet weak var usernamelblref: UILabel!
    @IBOutlet weak var Documentlblref: UILabel!
    @IBOutlet weak var Documentimgref: UIImageView!
    
    @IBOutlet weak var Emailimgref: UIImageView!
    @IBOutlet weak var Emaillblref: UILabel!
    var menuNamesArray = NSArray()
    var driverprofilePicURL = String()
    var menuImagesArray = NSArray()
    var logintype = String()
    let darkView = UIView()

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        menuNamesArray = ["Home","My Account","Deposit","Withdrawal","Transfer","MY Transactions","Investment","IB Account","Knowledgebase","Tickets","Market Analysis","Logout"]
//        menuImagesArray = ["Home","MyAccount","Deposit","Withdrawal","Transfer","MyTransactions","Investment","IBAccount","Quastion","Ticket","DataAnalysis","LogOut-blue"]
        
    menuNamesArray = ["Home","My Account","Deposit","Withdrawal","Transfer","MY Transactions","Investment","Support","Market Analysis","Logout"]
        menuImagesArray = ["Home","MyAccount","Deposit","Withdrawal","Transfer","MyTransactions","Investment","Support","DataAnalysis","LogOut-blue"]
        
        //driver Profile Picture
        //        imageview_ProlfiePiccRef.layer.borderWidth = 1
        //        imageview_ProlfiePiccRef.layer.masksToBounds = false
        //        imageview_ProlfiePiccRef.layer.borderColor = UIColor.white.cgColor
        //        imageview_ProlfiePiccRef.layer.cornerRadius = imageview_ProlfiePiccRef.frame.height/2
        //        imageview_ProlfiePiccRef.clipsToBounds = true
        //
        //        if driverprofilePicURL == nil || driverprofilePicURL.isEmpty == true {
        //            self.imageview_ProlfiePiccRef.image = UIImage(named: "drivericon")
        //        }else{
        //            let url = URL.init(string:driverprofilePicURL)
        //            imageview_ProlfiePiccRef.sd_setImage(with: url , placeholderImage: UIImage (named: "drivericon"))
        //        }
        
        
        //username
        if let userName = UserDefaults.standard.string(forKey: "name") as? String {
            self.usernamelblref.text = userName
        }
        //Email
//        if let IsEmailVerify = UserDefaults.standard.bool(forKey: "IsEmailVerify") as? Bool {
//            if IsEmailVerify {
//                self.Emailimgref.image = #imageLiteral(resourceName: "Emailverifyd")
//                self.Emaillblref.text = "Email verified"
//            }else {
//                self.Emailimgref.image = #imageLiteral(resourceName: "DocumentNOTverified")
//                self.Emaillblref.text = "Email not verified"
//            }
//        }
//        //Document
//        if let IsDocumentVerify = UserDefaults.standard.bool(forKey: "IsDocumentVerify") as? Bool {
//            if IsDocumentVerify {
//                self.Documentimgref.image = #imageLiteral(resourceName: "Emailverifyd")
//                self.Documentlblref.text = "Document verified"
//            }else {
//                self.Documentimgref.image = #imageLiteral(resourceName: "DocumentNOTverified")
//                self.Documentlblref.text = "Document not verified"
//            }
//        }
        tableview_MenusliderRef.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        darkView.addGestureRecognizer(revealViewController().tapGestureRecognizer())
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        darkView.frame = self.revealViewController().frontViewController.view.bounds
        self.revealViewController().frontViewController.view.addSubview(darkView)
    }

    override func viewWillDisappear(_ animated: Bool) {
        darkView.removeFromSuperview()
    }
    
    //MARK : UITABLEVIEW DATA SOURCE & DELEGATES
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return menuNamesArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cellIdentifier = "SlideMenuTableViewCell"
        let cell: SlideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SlideMenuTableViewCell
        cell.lbl_TitleRef?.text = self.menuNamesArray.object(at: indexPath.row) as? String
       // let image : UIImage = UIImage(named: (self.menuImagesArray.object(at: indexPath.row) as? String)!)!
        let imageName = menuImagesArray[indexPath.row] as? String ?? ""
        cell.imageview_Ref.image = UIImage(named: imageName)
        //image
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nameOfMenu: String? = self.menuNamesArray.object(at: indexPath.row) as? String
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)

        if nameOfMenu == "Home" {
            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
                  initialViewController.selectedIndex = 0
            let navController = UINavigationController(rootViewController: initialViewController)
            navController.setViewControllers([initialViewController], animated:true)
            navController.isNavigationBarHidden = true
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "My Account" {
            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
                  initialViewController.selectedIndex = 1
            let navController = UINavigationController(rootViewController: initialViewController)
            navController.setViewControllers([initialViewController], animated:true)
            navController.isNavigationBarHidden = true
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Deposit"{
            let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
                  initialViewController.selectedIndex = 2
            let navController = UINavigationController(rootViewController: initialViewController)
            navController.setViewControllers([initialViewController], animated:true)
            navController.isNavigationBarHidden = true
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Withdrawal" {
            let paymentHistoryVC = mainStoryboard.instantiateViewController(withIdentifier: "WithdrawalVC") as! WithdrawalVC
            let navController = UINavigationController(rootViewController: paymentHistoryVC)
            navController.setViewControllers([paymentHistoryVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Transfer" {
            let requestSalary = mainStoryboard.instantiateViewController(withIdentifier: "TransferVC") as! TransferVC
            let navController = UINavigationController(rootViewController: requestSalary)
            navController.setViewControllers([requestSalary], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        } else if nameOfMenu == "MY Transactions" {
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "MyTransactionVC") as! MyTransactionVC
            let navController = UINavigationController(rootViewController: settingsVC)
            navController.setViewControllers([settingsVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }
        
        else if nameOfMenu == "Investment" {
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "InvestmentVC") as! InvestmentVC
            let navController = UINavigationController(rootViewController: settingsVC)
            navController.setViewControllers([settingsVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "IB Account" {
//            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "IBAccountVC") as! IBAccountVC
             let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "KnowledgebaseVC") as! KnowledgebaseVC
            let navController = UINavigationController(rootViewController: settingsVC)
            navController.setViewControllers([settingsVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Knowledgebase" {
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "KnowledgebaseVC") as! KnowledgebaseVC
            let navController = UINavigationController(rootViewController: settingsVC)
            navController.setViewControllers([settingsVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Support" {
            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
            let navController = UINavigationController(rootViewController: settingsVC)
            navController.setViewControllers([settingsVC], animated:true)
            self.revealViewController().setFront(navController, animated: true)
            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Market Analysis" {
//            let settingsVC = mainStoryboard.instantiateViewController(withIdentifier: "KnowledgebaseVC") as! KnowledgebaseVC
//            let navController = UINavigationController(rootViewController: settingsVC)
//            navController.setViewControllers([settingsVC], animated:true)
//            self.revealViewController().setFront(navController, animated: true)
//            self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        }else if nameOfMenu == "Logout"{
            let alert = UIAlertController(title: "Logout?", message: "Are your Sure want to logout?",  preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "YES",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.logoutmethodApiCalling()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            
            var navigation = UINavigationController()
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                navigation = UINavigationController(rootViewController: initialViewControlleripad)
                appDelegate.window?.rootViewController = navigation
                appDelegate.window?.makeKeyAndVisible()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }else{
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    // Swift 4.2 onwards
        return 60
       }
    
    func logoutmethodApiCalling(){
        
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
            
        ]
        NetworkManager.Apicalling(url: logOUTUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(false, forKey: "LoggedIn")
                UserDefaults.standard.removeObject(forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "user_id")
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set("", forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "name")
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    
    
    
}
