
//
//  uploadDocumentVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import SDWebImage
import CropViewController
import MobileCoreServices
class uploadDocumentVC: UIViewController,UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    @IBOutlet weak var Profilepicimgref: UIImageView!
    @IBOutlet weak var ClickToUploadbtnref: UIButton!
    var imagePicked :Bool = false
    var imagePicker = UIImagePickerController()
    var filetype = ""
    var pickedImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(true, forKey: "menuopened")
               self.tabBarController?.reloadInputViews()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func Profilebtnref(_ sender: Any) {
        self.movetonextvcwithAnimationfalse(id: "profileVC", storyBordid: "DashBoard")
    }
    
    @IBAction func cmdMenu(_ sender: Any) {
        
        // self.popToBackVC()
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func ChangePasswordbtnref(_ sender: Any) {
        self.movetonextvcwithAnimationfalse(id: "ChangePasswordVC", storyBordid: "DashBoard")
    }
    
    @IBAction func imagepickerbntref(_ sender: Any) {
        self.filetype = "profile"
        self.showActionSheet()
    }
    
    @IBAction func Documentpickerbntref(_ sender: Any) {
        self.filetype = "Document"

           self.showActionSheet2()
       }
       
    
    @IBAction func DocumentUploadToserer(_ sender: Any){
         self.sendDocumentMethod()
    }
}
extension uploadDocumentVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    //    @IBAction func imagepickerbntref(_ sender: Any) {
    //        self.showActionSheet()
    //    }
    //MARK:- UIimagePickercontroller Delegate Methods
    
    fileprivate func showActionSheet (){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    fileprivate func showActionSheet2 (){
          let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
          
          
          actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
self.takePhotoFromCamera()
            
          }))
          actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()

                   }))
          actionSheet.addAction(UIAlertAction(title: "Choose a file ", style: .default, handler: { (alert:UIAlertAction!) -> Void in
              self.clickFunction()
          }))
          actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
          actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
          self.present(actionSheet, animated: true, completion: nil)
      }
    func takePhotoFromCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func takePhotoFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            
        }
    }
    
    //MARK: ImagePicker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // let imageData = pickedImage.pngData()
            // let imageData = pickedImage.jpegData(compressionQuality: 1.0)
            let cropViewController = CropViewController(image: pickedImage)
           
            cropViewController.delegate = self
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.aspectRatioPreset = .preset5x4
            
            //  cropViewController.aspectra
            
            self.navigationController?.pushViewController(cropViewController, animated: true)
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }


    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }


    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func clickFunction(){

    let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}

//MARK:- CropViewControllerDelegate Methods
extension uploadDocumentVC : CropViewControllerDelegate{
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        self.imagePicked = true
         if self.filetype == "profile" {
        self.Profilepicimgref.image = image
        //  self.sendTicketrequest(image : image)
        self.sendProfilePic()
         }else {
            self.pickedImage = image
            self.ClickToUploadbtnref.setImage(image, for: .normal)
           
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func sendProfilePic(){
        indicator.showActivityIndicator()
        
        let parameters = [
            "":""
        ]
        NetworkManager.MultiformApicalling(url: ProfilepicUpdateUrl, paramaters: parameters, Images: [self.Profilepicimgref.image!], Imagesname: "avatar", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //Profile Pic
                if let ProfilePic = response.payload.avatar as? String {
                    self.Profilepicimgref.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.Profilepicimgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
        
    }
    
    
    func sendDocumentMethod(){
        indicator.showActivityIndicator()
        let parameters = [
            "file_type":"id_proof_front"
        ]
        NetworkManager.MultiformApicalling(url: ProfileDocumentuploadUrl, paramaters: parameters, Images: [self.pickedImage], Imagesname: "document", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //Profile Pic
                if let ProfilePic = response.payload.avatar as? String {
                    self.Profilepicimgref.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.Profilepicimgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
                    self.ClickToUploadbtnref.setImage(self.pickedImage, for: .normal)
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
        
    }
    
}
