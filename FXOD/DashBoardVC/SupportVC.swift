//
//  SupportVC.swift
//  FXOD
//
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class SupportVC: UIViewController {

    @IBOutlet weak var BACKBTNREF: UIButton!
    
    var vcFROM = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BACKBTNREF.isHidden = false
               self.BACKBTNREF.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
               self.BACKBTNREF.backgroundColor = .clear
               self.tabBarController?.tabBar.isHidden = true
               self.navigationController?.navigationBar.isHidden = true
               self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
                      self.tabBarController?.tabBar.isHidden = false

    }
    @IBAction func BACKBTNREF(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "menuopened")
        self.BACKBTNREF.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)

    }
    @IBAction func Movetoticketvc(_ sender: Any) {
        self.movetonextvc(id: "TicketVC", storyBordid: "DashBoard")
    }
    
    @IBAction func Movetoknowldgebase(_ sender: Any) {
           self.movetonextvc(id: "KnowledgebaseVC", storyBordid: "DashBoard")
       }
    
    @IBAction func MovetoChatVC(_ sender: Any) {
       // self.movetonextvc(id: "CommonChatVC", storyBordid: "DashBoard")
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
        initialViewController.selectedIndex = 3
        let navController = UINavigationController(rootViewController: initialViewController)
        navController.setViewControllers([initialViewController], animated:true)
        navController.isNavigationBarHidden = true
        self.revealViewController().setFront(navController, animated: true)
        self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
    }

}
