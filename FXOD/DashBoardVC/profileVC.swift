//
//  profileVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import SDWebImage
import CropViewController
import MobileCoreServices

class profileVC: UIViewController,UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    //MARK:- -------Class IBOUtlets ----------
    @IBOutlet weak var Nametfref: UITextField!
    @IBOutlet weak var Emailtfref: UITextField!
    @IBOutlet weak var Phonenumbertftref: UITextField!
    @IBOutlet weak var Addresstfref: UITextField!
    @IBOutlet weak var Countrytfref: UITextField!
    @IBOutlet weak var Statetfref: UITextField!
    @IBOutlet weak var Citytfref: UITextField!
    @IBOutlet weak var PostalCodetfref: UITextField!
    @IBOutlet weak var Profilepicimgref: UIImageView!
    
    @IBOutlet weak var ChangePasswordViewref: UIView!
    @IBOutlet weak var DocumentViewref: UIView!
    
    @IBOutlet weak var Savebtnref: UIButton!
    @IBOutlet weak var Cancelbtnref: UIButton!
    
    @IBOutlet weak var CurrentPasswodTfref: UITextField!
    @IBOutlet weak var NewPasswordtfref: UITextField!
    @IBOutlet weak var ConformNewPasswordtfref: UITextField!
    
    @IBOutlet weak var rigesterbtnref: UIButton!
    @IBOutlet weak var usernamebtnref: UIButton!
    
    //MARK:- ------- Class Properties ----------

    var imagePicked :Bool = false
    var imagePicker = UIImagePickerController()
    
    var selectedDocumentedImage = UIImage()
    var selectedtypebtn = ""
    var proofType = ""
    
    //MARK:- ------- Class View Lyfe cycles  ----------
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedtypebtn = "Edit Profile"
        imagePicker.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        UserDefaults.standard.set(true, forKey: "menuopened")
        
        //phase 2
        self.ChangePasswordViewref.isHidden = true
        self.DocumentViewref.isHidden = true
        self.Savebtnref.isHidden = true
        self.Cancelbtnref.isHidden = true
        
        //Api calling....
        self.ProfileDetailsVC()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
              self.tabBarController?.tabBar.isHidden = false
          }
    //MARK:- ------- Class Button actions ----------

    @IBAction func cmdMenu(_ sender: Any) {
        
        //self.popToBackVC()
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
    }
    //MARK:- Phase 1 button
    //MARK:-  upload documet buttons 
    @IBAction func ChangeDocumentbtnref(_ sender: Any) {
        self.DocumentViewref.isHidden = false
        selectedtypebtn = "Upload Document"
        self.Savebtnref.isHidden = true
        self.Cancelbtnref.isHidden = true
    }
    //MARK:-  Chnage Password buton 
    @IBAction func ChangePasswordbtnref(_ sender: Any) {
        selectedtypebtn = "Change password"
        self.Savebtnref.isHidden = true
        self.Cancelbtnref.isHidden = true
        self.ChangePasswordViewref.isHidden = false
    }
    //MARK:-  Edit profile buton 
    @IBAction func EditProfilebtnref(_ sender: Any) {
        selectedtypebtn = "Edit Profile"
        self.Savebtnref.isHidden = false
        self.Cancelbtnref.isHidden = false
    }
    
    
    //MARK:- phase 2 Buttons
    
    //MARK:- Document front and back and address upload buttons
    @IBAction func DucumentFront(_sender: Any) {
        proofType = "id_proof_front"
        self.showActionSheet2()
    }
    @IBAction func DucumentBack(_sender: Any) {
        proofType = "id_proof_back"
        self.showActionSheet2()
    }
    @IBAction func DucumentAddress(_sender: Any) {
        proofType = "id_proof_address"
        self.showActionSheet2()
    }
    
    //MARK:- Edit profile buttons
    @IBAction func Cancelbtnref(_ sender: Any) {
        self.Savebtnref.isHidden = true
        self.Cancelbtnref.isHidden = true
    }
    @IBAction func SaveBtn(_ sender :UIButton) {
        // TODO: - 👊 Validations -
        switch ValidatingProfileupdate {
        case .valid:
            // self.moveToNext()
            self.ProfileUpdate()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    //MARK:- Chnage password Buttons
    @IBAction func ChnagePasswordtfref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch ValidatingPasswordupdate {
        case .valid:
            // self.moveToNext()
            self.ChangePassword()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    //MARK:- remove all the popups
    @IBAction func removepops(_sender : UIButton) {
        self.ChangePasswordViewref.isHidden = true
        self.DocumentViewref.isHidden = true
    }
    
}
extension profileVC {
    //MARK:- -------  Get UserDetails Api calling ----------

    func ProfileDetailsVC(){
        
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
            
        ]
        NetworkManager.Apicalling(url: ProfileDetailsUrl, paramaters: parameters, httpMethodType: .get, success: { (response:ProfileDetailsModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //Name
                if let name = response.payload.name as? String {
                    self.Nametfref.text = name
                    self.usernamebtnref.setTitle(name, for: .normal)
                }
                
                //register at
                if let registerat = response.payload.created_at as? String {
                    var transdate = registerat.toDateString(inputDateFormat: "yyyy-MM-dd HH:mm:ss", ouputDateFormat: "MMM dd,yyyy")
                    self.rigesterbtnref.setTitle("Registered: " + transdate, for: .normal)
                }
                //Email
                if let Email = response.payload.email as? String {
                    self.Emailtfref.text = Email
                }
                //Phone
                if let Phone = response.payload.phone as? String {
                    self.Phonenumbertftref.text = Phone
                }
                //Address
                if let Address = response.payload.address1 as? String {
                    self.Addresstfref.text = Address
                }
                //Country
                if let Country = response.payload.country as? String {
                    self.Countrytfref.text = Country
                }
                //State
                if let State = response.payload.state as? String {
                    self.Statetfref.text = State
                }
                //City
                if let City = response.payload.city as? String {
                    self.Citytfref.text = City
                }
                //Postal Code
                if let PostalCode = response.payload.postcode as? String {
                    self.PostalCodetfref.text = PostalCode
                }
                //Profile Pic
                if let ProfilePic = response.payload.avatar as? String {
                    self.Profilepicimgref.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.Profilepicimgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    //MARK:- -------  edit  UserDetails Api calling ----------

    func ProfileUpdate(){
        
        indicator.showActivityIndicator()
        //Name
        guard let name = Nametfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Phone
        guard let Phone = Phonenumbertftref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Address
        guard let Address = Addresstfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Country
        guard let Country = Countrytfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //State
        guard let State = Statetfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //City
        guard let City = Citytfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //postcode
        guard let postcode = PostalCodetfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        
        let parameters = [
            "name":name,
            "phone":Phone,
            "address1":Address,
            "address2":Address,
            "country":Country,
            "state":State,
            "city":City,
            "postcode":postcode
        ]
        NetworkManager.Apicalling(url: ProfileUpdateUrl, paramaters: parameters, httpMethodType: .post, success: { (response:ProfileUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.Savebtnref.isHidden = true
                self.Cancelbtnref.isHidden = true
                self.ShowAlert(message: response.message)
                self.ProfileDetailsVC()
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
//MARK: - Validation
extension profileVC {
    //MARK:- -------  edit  UserDetails profile validation Api calling ----------

    var ValidatingProfileupdate:UserValidationState {
        if Nametfref.text == "" || Emailtfref.text ?? "" == "" || Phonenumbertftref.text ?? "" == "" || Addresstfref.text ?? "" == "" || Countrytfref.text ?? "" == "" || Statetfref.text ?? "" == "" || Citytfref.text ?? "" == "" || PostalCodetfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if Emailtfref.text?.isEmpty == true  {
            return .invalid(ATErrorMessage.login.email)
        }else if Nametfref.text?.isEmpty == true {
            return .invalid("Please enter your name")
        }else if Phonenumbertftref.text?.isEmpty == true {
            return .invalid("Please enter your Phone Number")
        }else if Phonenumbertftref.text?.count ?? 0 < 11 {
            return .invalid("Please enter your Valid Phone number")
        }else if Addresstfref.text?.isEmpty == true {
            return .invalid("Please enter your Address")
        }else if Countrytfref.text?.isEmpty == true {
            return .invalid("Please enter your Country")
        }else if Statetfref.text?.isEmpty == true {
            return .invalid("Please enter your State")
        }else if Citytfref.text?.isEmpty == true {
            return .invalid("Please enter your City")
        }else if PostalCodetfref.text?.isEmpty == true {
            return .invalid("Please enter your PostalCode")
        }
        
        return .valid
    }
    
    
}
extension profileVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //MARK:- -------  Picker Images and document sections ----------

    @IBAction func imagepickerbntref(_ sender: Any) {
        self.showActionSheet()
    }
    //MARK:- UIimagePickercontroller Delegate Methods
    
    fileprivate func showActionSheet (){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    fileprivate func showActionSheet2 (){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose a file ", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.clickFunction()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    func takePhotoFromCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func takePhotoFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            
        }
    }
    
    //MARK: ImagePicker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // let imageData = pickedImage.pngData()
            // let imageData = pickedImage.jpegData(compressionQuality: 1.0)
            let cropViewController = CropViewController(image: pickedImage)
            cropViewController.delegate = self
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.aspectRatioPreset = .preset5x4
            //  cropViewController.aspectra
            
            self.navigationController?.pushViewController(cropViewController, animated: true)
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func clickFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}

//MARK:- CropViewControllerDelegate Methods
extension profileVC : CropViewControllerDelegate{
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        if selectedtypebtn == "Upload Document" {
            self.imagePicked = true
            self.selectedDocumentedImage = image
            self.sendDocumentMethod()
        }else {
            self.Profilepicimgref.image = image
            self.imagePicked = true
            //  self.sendTicketrequest(image : image)
            self.sendProfilePic()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- ------- Send Profile pic  ----------

    func sendProfilePic(){
        indicator.showActivityIndicator()
        
        let parameters = [
            "":""
        ]
        NetworkManager.MultiformApicalling(url: ProfilepicUpdateUrl, paramaters: parameters, Images: [self.Profilepicimgref.image!], Imagesname: "avatar", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //Profile Pic
                if let ProfilePic = response.payload.avatar as? String {
                    self.Profilepicimgref.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.Profilepicimgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
        
    }
    
}
extension profileVC {
    //MARK:- ------- Chnage password  ----------

    func ChangePassword(){
        
        indicator.showActivityIndicator()
        //Name
        guard let CurrentPasswod = CurrentPasswodTfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Phone
        guard let NewPassword = NewPasswordtfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        //Address
        guard let ConformNewPassword = ConformNewPasswordtfref.text  else {
            indicator.hideActivityIndicator()
            return
        }
        
        
        let parameters = [
            "current_password":CurrentPasswod,
            "new_password":NewPassword,
            "new_password_confirmation":ConformNewPassword
        ]
        NetworkManager.Apicalling(url: ChangePasswordUrl, paramaters: parameters, httpMethodType: .post, success: { (response:ProfileUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ChangePasswordViewref.isHidden = true
                self.DocumentViewref.isHidden = true
                self.ShowAlert(message: response.message)
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
}
//MARK: - Validation
extension profileVC {
    //MARK:- ------- Chnage password validation ----------

    var ValidatingPasswordupdate:UserValidationState {
        if CurrentPasswodTfref.text == "" || NewPasswordtfref.text ?? "" == "" || ConformNewPasswordtfref.text ?? "" == ""  {
            return .invalid(ATErrorMessage.login.required)
        }else if CurrentPasswodTfref.text?.isEmpty == true  {
            return .invalid("Please enter your current Password")
        }else if NewPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter your New password")
        }else if ConformNewPasswordtfref.text?.isEmpty == true {
            return .invalid("Please enter your Conform New Password")
        }
        
        return .valid
    }
    
    
}
extension profileVC {
    //MARK:- ------- Send Document method ----------

    func sendDocumentMethod(){
        indicator.showActivityIndicator()
        let parameters = [
            "file_type":self.proofType
        ]
        NetworkManager.MultiformApicalling(url: ProfileDocumentuploadUrl, paramaters: parameters, Images: [self.selectedDocumentedImage], Imagesname: "document", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ChangePasswordViewref.isHidden = true
                self.DocumentViewref.isHidden = true
                self.ShowAlert(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
        
    }
}
