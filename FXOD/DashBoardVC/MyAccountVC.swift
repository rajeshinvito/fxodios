//
//  MyAccountVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_7         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_8         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}
class MyAccountVC: UIViewController {
    
    //MARK:-   CLASS OUTLETS 
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var Labelleadingref: NSLayoutConstraint!
    @IBOutlet weak var tableviewref: UITableView!
    @IBOutlet weak var Totalamount : UILabel!
    @IBOutlet weak var TotalDeposits : UILabel!
    @IBOutlet weak var TotalWithdrawl : UILabel!
    @IBOutlet weak var LiveAccounts : UILabel!
    
    @IBOutlet weak var tableviewPopUPref: UITableView!
    
    @IBOutlet weak var PopupViewref: UIView!
    //MARK:-   CLASS PROPERTIES 
    var hiddenSections = Set<Int>()
    var tableViewData = [LiveAccountListdata]()
    var type = String()
    var titleArr  = ["76576576","68768","87687","4654","111111"]
    var titleArr2  = ["9999","888","3333","22222","55555"]
    var vcFROM = ""
    var selectedAccountnumber = ""
    var liveAccountPopUPlist = ["Deposit","Withdrawl","Transfer","Change master password","Change investor password","Open position","Close position","Transaction history"]
    
    var DemoAccountPopUPlist = ["Change master password","Change investor password","Open position","Transaction history"]
    
    //MARK:-   CLASS VIEW LYFE CYCLE STARTS 
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
        
        self.PopupViewref.isHidden  = true
        
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 30)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 50)
        }
        //        if vcFROM == "DashBoardVc" {
        //            self.Back.isHidden = false
        //            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
        //            self.Back.backgroundColor = .white
        //        }else {
        self.Back.isHidden = false
        self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
        self.Back.backgroundColor = .clear
        
        // }
        self.type = "Live"
        //self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationtoAccountVC"), object: nil)
        //Api calling....
        self.DashboardApiMethod()
        
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        // self.navigationController?.navigationBar.isHidden = true
        // self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:-   CLASS BUTTON ACTIONS START 
    
    @IBAction func cmdMenu(_ sender: Any) {
        //        if vcFROM == "DashBoardVc" {
        //            self.popToBackVC()
        //        }else {
        // self.tabBarController?.tabBar.isHidden = true
        // UserDefaults.standard.set(true, forKey: "menuopened")
        //            sharedMenuClass.menuObj.addMenuClass(view: self.view)
        //            sharedMenuClass.menuObj.menuView.delegate = self
        Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
        //  }
    }
    
    
    @IBAction func MyLiveAccountbtnref(_ sender: Any) {
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 30)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 50)
        }
        self.type = "Live"
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.tableViewData.removeAll()
        self.tableviewref.reloadData()
        self.GetLiveAccountsList()
        self.tableviewref.layer.add(transition, forKey: nil)
        
    }
    
    @IBAction func MyDemoAccountbtnref(_ sender: Any) {
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 180)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 250)
        }
        self.type = "Demo"
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.tableViewData.removeAll()
        self.tableviewref.reloadData()
        self.GetDemoAccountsList()
        self.tableviewref.layer.add(transition, forKey: nil)
        //self.tableView.reloadData()
    }
    
    
    @IBAction func HidepopUpBtnref(_ sender: Any) {
        self.PopupViewref.isHidden  = true
    }
    
}
//MARK:-   CLASS SIDE MENU DELEGATES  

extension MyAccountVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

//MARK:-   CLASS TABLE VIEW DELEGATES  

extension MyAccountVC : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.tableviewPopUPref {
            return 1
        }else {
            var numOfSection: NSInteger = 0
            if self.tableViewData.count > 0
            {
                self.tableviewref.backgroundView = nil
                numOfSection = self.tableViewData.count
            }
            else
            {
                var noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableviewref.bounds.size.width, height: self.tableviewref.bounds.size.height))
                noDataLabel.text = "No Data Available"
                noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                noDataLabel.textAlignment = NSTextAlignment.center
                self.tableviewref.backgroundView = noDataLabel
                
            }
            
            return numOfSection
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableviewPopUPref {
            if type == "Live" {
                return liveAccountPopUPlist.count
            }else {
                return  DemoAccountPopUPlist.count
            }
        }else {
            if self.hiddenSections.contains(section) {
                return 0
            }
            
            return 1
        }
        return  0
        //self.tableViewData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tableviewPopUPref {
            let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            if type == "Live" {
                cell.textLabel?.text = liveAccountPopUPlist[indexPath.row]
            }else {
                cell.textLabel?.text = DemoAccountPopUPlist[indexPath.row]
            }
            return cell
        }else {
            let cell: LiveAccountCell = tableView.dequeueReusableCell(withIdentifier: "LiveAccountCell", for: indexPath) as! LiveAccountCell
            //levarage number
            if let levarageNumber = self.tableViewData[1].leverage as? String {
                cell.levaragenumberlblref.text = "1:" + levarageNumber
            }
            
            //        //Platform
            //        if let Platform = self.tableViewData[1].pla as? String {
            //            cell.levaragenumberlblref.text = "1:" + levarageNumber
            //        }
            
            //Account type
            if let accountType = self.tableViewData[1].account_type_title as? String {
                cell.Accounttypelblref.text = accountType
            }
            
            // type
            
            if let type = self.tableViewData[1].account_type as? String {
                cell.typelblref.text = type
            }
            
            //currency
            if let currency = self.tableViewData[1].account_currency as? String {
                cell.Currencylblref.text = currency
            }
            
            
            //Balance
            
            if let Balance = self.tableViewData[1].account_balance as? String {
                cell.Balancelblref.text = Balance
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableviewPopUPref {
            
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.tableviewPopUPref {
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            
            headerView.backgroundColor = UIColor(named: "lightweight")
            
            //Label - 1
            
            let titleLbl = UILabel.init(frame: CGRect.init(x: 20, y: 20, width: tableView.frame.width - 60, height: 22))
            titleLbl.text = self.selectedAccountnumber
            titleLbl.textAlignment = .center
            
            let sectionButton2: UIButton = UIButton(frame: CGRect(x: tableView.frame.width - 60, y: 0, width:  60, height: 80)) //
            sectionButton2.setImage(#imageLiteral(resourceName: "Cross"), for: .normal)
            sectionButton2.backgroundColor = .red
            sectionButton2.tag = section
            sectionButton2.addTarget(self,
                                     action: #selector(self.hideSection3(sender:)),
                                     for: .touchUpInside)
            
            headerView.addSubview(titleLbl)
            headerView.addSubview(sectionButton2)
            
            return headerView
        }else {
            // label
            let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            
            headerView.backgroundColor = UIColor(named: "lightweight")
            
            //Label - 1
            
            let titleLbl = UILabel.init(frame: CGRect.init(x: 20, y: 22, width: 150, height: 22))
            
            
            
            //lanel - 2
            
            let AccountTypeLbl = UILabel.init(frame: CGRect.init(x: 20, y: 54, width: 150, height: 18))
            //Label - 3
            let AccountTypeTitleLbl = UILabel.init(frame: CGRect.init(x: 120, y: 22, width: 40, height: 18))
            AccountTypeTitleLbl.backgroundColor = UIColor(named: "#2ABA00")
            AccountTypeTitleLbl.layer.cornerRadius = 10
            AccountTypeTitleLbl.textAlignment = .center
            AccountTypeTitleLbl.textColor = .white
            AccountTypeTitleLbl.font = UIFont(name: "cerebrisans-regular", size: 12.0)
            //UIView
            
            let tradeView = UIView.init(frame:CGRect.init(x: 170, y: 25, width: 110, height: 40))
             let AccountTypeLbl3 = UILabel.init(frame: CGRect.init(x: 5, y: 8, width: 60, height: 22))
            AccountTypeLbl3.text = "Trade"
            AccountTypeLbl3.textColor = UIColor(named: "#FEA400")
            tradeView.borderWidth = 1
            tradeView.borderColor = .gray
            tradeView.backgroundColor = .white
            tradeView.layer.cornerRadius = 10

            
            let AccountTypeImage = UIImageView.init(frame: CGRect.init(x: 60, y: 8, width: 40, height: 22))
            AccountTypeTitleLbl.layer.cornerRadius = 5
            tradeView.addSubview(AccountTypeLbl3)
            tradeView.addSubview(AccountTypeImage)

            if self.type == "Live" {
                AccountTypeLbl.text = self.tableViewData[section].account_type
                titleLbl.text = self.tableViewData[section].account_number
                AccountTypeTitleLbl.text = "Live"
                AccountTypeImage.image = #imageLiteral(resourceName: "Trade")
            }else {
                AccountTypeLbl.text = self.tableViewData[section].account_type
                titleLbl.text = self.tableViewData[section].account_number
                AccountTypeTitleLbl.text = "Demo"
                AccountTypeImage.image = #imageLiteral(resourceName: "Trade")
            }
            
            //Line Label
            let Linelbl = UILabel.init(frame: CGRect.init(x: 0, y: 79, width: tableView.frame.width, height: 1))
            Linelbl.backgroundColor = UIColor(named: "#CADBFB")
            
            //forward image
            let forwardimage = UIImageView.init(frame: CGRect.init(x: 290, y: 43, width: 6, height: 6))
            forwardimage.image = #imageLiteral(resourceName: "Forward")
            
            // MoreImage
            let MoreImage = UIImageView.init(frame: CGRect.init(x: tableView.frame.width - 10, y: 42, width: 2, height: 12))
            MoreImage.image = #imageLiteral(resourceName: "options")
            
            
            
            
            //Button
            let sectionButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width - 60 , height: 80)) //
            
            sectionButton.tag = section
            sectionButton.addTarget(self,
                                    action: #selector(self.hideSection(sender:)),
                                    for: .touchUpInside)
            
            
            let sectionButton2: UIButton = UIButton(frame: CGRect(x: tableView.frame.width - 60, y: 0, width:  60, height: 80)) //
            
            sectionButton2.tag = section
            sectionButton2.addTarget(self,
                                     action: #selector(self.hideSection2(sender:)),
                                     for: .touchUpInside)
            //        headerView.addSubview(label)
            headerView.addSubview(titleLbl)
            headerView.addSubview(AccountTypeLbl)
            headerView.addSubview(forwardimage)
            headerView.addSubview(MoreImage)
            headerView.addSubview(Linelbl)
            headerView.addSubview(sectionButton)
            headerView.addSubview(sectionButton2)
            headerView.addSubview(tradeView)
            headerView.addSubview(AccountTypeTitleLbl)
            return headerView
            //return sectionButton
            
        }
    }
    
    
    
    
    @objc
    private func hideSection(sender: UIButton) {
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
            //  for row in 0..<tableViewData[section].count {
            indexPaths.append(IndexPath(row: 0,
                                        section: section))
            // }
            
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.tableviewref.insertRows(at: [IndexPath(row: 0,
                                                        section: section)],
                                         with: .bottom)
        } else {
            self.hiddenSections.insert(section)
            self.tableviewref.deleteRows(at: [IndexPath(row: 0,
                                                        section: section)],
                                         with: .top)
        }
    }
    
    
    @objc
    private func hideSection2(sender: UIButton) {
        self.selectedAccountnumber = self.tableViewData[sender.tag].account_number
        self.PopupViewref.isHidden  = false
        self.tableviewPopUPref.reloadData()
    }
    
    @objc
    private func hideSection3(sender: UIButton) {
        self.PopupViewref.isHidden  = true
        self.tableviewPopUPref.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
}


//MARK:-   CLASS API HITTINGS   
extension MyAccountVC{
    func DashboardApiMethod(){
        
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: DashBoardUrl, paramaters: parameters, httpMethodType: .post, success: { (response:DashboardModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                if let TotalAmount = response.payload.balance.total_amount as? Int {
                    self.Totalamount.text = "\(TotalAmount)"
                }
                if let TotalDeposit = response.payload.balance.deposit as? Int {
                    self.TotalDeposits.text = "\(TotalDeposit)"
                    
                }
                if let Totalwithdrawl = response.payload.balance.withdrawal as? Int {
                    self.TotalWithdrawl.text = "\(Totalwithdrawl)"
                }
                if let LiveAccounts = response.payload.balance.liveaccounts as? Int {
                    self.LiveAccounts.text = "\(LiveAccounts)"
                }
                
                self.GetLiveAccountsList()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                self.GetLiveAccountsList()
                
            }
        }) { (errorMsg) in
            self.GetLiveAccountsList()
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    
    //MARK:-   CLASS GET LIVE ACCOUNT LIST API 
    
    func GetLiveAccountsList(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: LiveAccountListURL, paramaters: parameters, httpMethodType: .get, success: { (response:LiveAccountListModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.tableViewData = response.payload
                self.tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    func GetDemoAccountsList(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: DemoAccountListURL, paramaters: parameters, httpMethodType: .get, success: { (response:LiveAccountListModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.tableViewData = response.payload
                self.tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
