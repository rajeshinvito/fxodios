//
//  InvestmentVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class InvestmentVC: UIViewController {
    
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var Labelleadingref: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var hiddenSections = Set<Int>()
    let tableViewData = [
        ["1","2","3","4","5"],
        ["1","2","3","4","5"],
        ["1","2","3","4","5"],
        ["1","2","3","4","5"],
        ["1","2","3","4","5"],
    ]
    var titleArr  = ["76576576","68768","87687","4654","111111"]
    var titleArr2  = ["9999","888","3333","22222","55555"]
    
    var MyInvesttableViewData = [Investdata]()
    var FoundManagerstableViewData = [foundmanagersdata]()
    
    
    var vcFROM = ""
    var type = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
        
        // self.uiAnimate(view : Labelleadingref, Constratint : 50)
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 20)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 50)
        }
        
        //        if vcFROM == "DashBoardVc" {
        //            self.Back.setImage(#imageLiteral(resourceName: "Backbtn"), for: .normal)
        //            self.Back.backgroundColor = .white
        //        }else {
        //            UserDefaults.standard.set(false, forKey: "frommenu")
        self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
        self.Back.backgroundColor = .clear
        //}
        self.type = "Demo"
        self.tabBarController?.tabBar.isHidden = true
        //                  UserDefaults.standard.set(true, forKey: "menuopened")
        //                  tabBarController?.reloadInputViews()
        self.GetFoundManagersAccountsList()
        // Do any additional setup after loading the view.
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
    }
    @IBAction func cmdMenu(_ sender: Any) {
//        if vcFROM == "DashBoardVc" {
//            self.popToBackVC()
//        }else {
            //               self.tabBarController?.tabBar.isHidden = true
            //                   UserDefaults.standard.set(true, forKey: "menuopened")
            //                   self.tabBarController?.reloadInputViews()
            //            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            //                      sharedMenuClass.menuObj.menuView.delegate = self
            Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            
        //}
    }
    
    
    @IBAction func MYInvestMentAccountbtnref(_ sender: Any) {
        // self.uiAnimate(view : Labelleadingref, Constratint : 260)
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 180)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 250)
        }
        self.type = "Live"
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.MyInvesttableViewData.removeAll()
        self.tableView.reloadData()
        self.GetmyInvestAccountsList()
        self.tableView.layer.add(transition, forKey: nil)
        
    }
    
    @IBAction func foundmanagersAccountbtnref(_ sender: Any) {
        if(Device.IS_IPHONE_5 || Device.IS_IPHONE_4_OR_LESS){
            //--- set your constrain for iphone 5 and 4
            self.uiAnimate(view : Labelleadingref, Constratint : 20)
        }else if(Device.IS_IPAD){
            //--- set your constrain for ipad
        }else{
            //--- set default constrain
            self.uiAnimate(view : Labelleadingref, Constratint : 50)
        }
        self.type = "Demo"
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.FoundManagerstableViewData.removeAll()
        self.tableView.reloadData()
        self.GetFoundManagersAccountsList()
        self.tableView.layer.add(transition, forKey: nil)
        //self.tableView.reloadData()
    }
}
extension InvestmentVC:MenuDelegate {
    
    func MenuMethods() {
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        self.view.addGestureRecognizer(edgePan)
    }
    
    @objc func screenEdgeSwiped(sender:UIScreenEdgePanGestureRecognizer) {
        
        if sender.edges == .left {
            sharedMenuClass.menuObj.addMenuClass(view: self.view)
            sharedMenuClass.menuObj.menuView.delegate = self
        }
        else if sender.edges == .right {
            
        }
    }
    
    func callMethod(vcIndex:Int) {
        navigationClass(index:vcIndex)
    }
    
    func touchEvents(_ direction:String) {
        if direction == "left" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
        if direction == "right" {
            sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        }
    }
}

extension  InvestmentVC: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        var count = 0
        if self.type == "Live" {
            count =  self.MyInvesttableViewData.count
        }else {
            count =  self.FoundManagerstableViewData.count
        }
        
        
        var numOfSection: NSInteger = 0
        if count > 0
        {
            self.tableView.backgroundView = nil
            numOfSection = count
        }
        else
        {
            var noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            noDataLabel.text = "No Data Available"
            noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            
        }
        
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.hiddenSections.contains(section) {
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  self.type == "Demo" {
            let cell: fundManagerCell = tableView.dequeueReusableCell(withIdentifier: "fundManagerCell", for: indexPath) as! fundManagerCell
            //minimumDeposite
            if let minimumDeposite = self.FoundManagerstableViewData[indexPath.row].min_deposit as? Int {
                cell.minimumDepositelblref.text = "\(minimumDeposite ?? 0)"
            }
            //Gain
            if let Gain = self.FoundManagerstableViewData[indexPath.row].growth as? String {
                cell.Gainlblref.text = Gain
            }
            //Daily Gain
            if let DailyGain = self.FoundManagerstableViewData[indexPath.row].daily_growth as? String {
                cell.DailyGainLblref.text = DailyGain
            }
            //Montly Gain
            if let MonthlyGain = self.FoundManagerstableViewData[indexPath.row].monthly_growth as? String {
                cell.MontlyGainlblref.text = MonthlyGain
            }
            //DrawDown
            if let DrawDown = self.FoundManagerstableViewData[indexPath.row].drawdown as? String {
                cell.Drawdownlblref.text = DrawDown
            }
            
            cell.InvestBtnref.tag = indexPath.row
            cell.InvestBtnref.addTarget(self, action: #selector(createMyInestmentaccount(sender:)), for: .touchUpInside)
            
            return cell
        }else {
            let cell: MyInvestAccountCell = tableView.dequeueReusableCell(withIdentifier: "MyInvestAccountCell", for: indexPath) as! MyInvestAccountCell
            
            //leverage
            if let leverage = self.MyInvesttableViewData[indexPath.row].leverage as? String {
                cell.Leveragelblref.text = leverage
            }
            //account Type
            if let AccountType = self.MyInvesttableViewData[indexPath.row].account_type_title as? String {
                cell.Accounttypelblref.text = AccountType
            }
            //type
            if let type = self.MyInvesttableViewData[indexPath.row].account_type as? String {
                cell.typelblref.text = type
            }
            //currency
            if let currency = self.MyInvesttableViewData[indexPath.row].account_currency as? String {
                cell.Currencytypelblref.text = currency
            }
            
            //Balance
            if let Balance = self.MyInvesttableViewData[indexPath.row].account_balance as? String {
                cell.Balancelblref.text = Balance
            }
            
            return cell
        }
        
    }
    @objc func createMyInestmentaccount(sender: UIButton){
        let buttonTag = sender.tag
        let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: "MyInvestVC") as! MyInvestVC
        nxtVC.MamID = "\(self.FoundManagerstableViewData[buttonTag].id)"
        self.navigationController?.pushViewController(nxtVC, animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // label
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        headerView.backgroundColor = UIColor(named: "lightweight")
        
        //Label - 1
        
        let titleLbl = UILabel.init(frame: CGRect.init(x: 20, y: 22, width: 140, height: 22))
        
        
        
        //lanel - 2
        
        let AccountTypeLbl = UILabel.init(frame: CGRect.init(x: 20, y: 54, width: 140, height: 18))
        
        AccountTypeLbl.textColor = UIColor(named: "#4F6C8D")
        
        if self.type == "Live" {
            titleLbl.text = MyInvesttableViewData[section].account_type_title
            AccountTypeLbl.text = MyInvesttableViewData[section].account_number
        }else {
            titleLbl.text = FoundManagerstableViewData[section].title
            
            AccountTypeLbl.text = FoundManagerstableViewData[section].account_number
            
        }
        
        //Line Label
        let Linelbl = UILabel.init(frame: CGRect.init(x: 0, y: 79, width: tableView.frame.width, height: 1))
        Linelbl.backgroundColor = UIColor(named: "#CADBFB")
        
        //forward image
        //        let forwardimage = UIImageView.init(frame: CGRect.init(x: 125, y: 43, width: 6, height: 6))
        //        forwardimage.image = #imageLiteral(resourceName: "Forward")
        
        // MoreImage
        let MoreImage = UIImageView.init(frame: CGRect.init(x: tableView.frame.width - 30, y: 42, width: 2, height: 6))
        MoreImage.image = #imageLiteral(resourceName: "Forward")
        
        
        
        
        //Button
        let sectionButton: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 80)) //
        
        // let sectionButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        //        sectionButton.setTitle(String(section),
        //                               for: .normal)
        // sectionButton.backgroundColor = .red
        
        //        sectionButton.tag = section
        //        sectionButton.setTitleColor(.black, for: .normal)
        sectionButton.tag = section
        sectionButton.addTarget(self,
                                action: #selector(self.hideSection(sender:)),
                                for: .touchUpInside)
        //        headerView.addSubview(label)
        headerView.addSubview(titleLbl)
        headerView.addSubview(AccountTypeLbl)
        // headerView.addSubview(forwardimage)
        headerView.addSubview(MoreImage)
        headerView.addSubview(Linelbl)
        headerView.addSubview(sectionButton)
        
        return headerView
        //return sectionButton
    }
    
    
    
    
    @objc
    private func hideSection(sender: UIButton) {
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            //            if self.type == "Live" {
            //                for row in 0..<self.MyInvesttableViewData[section].count {
            //                    indexPaths.append(IndexPath(row: row,
            //                                                section: section))
            //                }
            //            }else {
            //                for row in 0..<self.FoundManagerstableViewData[section].count {
            indexPaths.append(IndexPath(row: 0,
                                        section: section))
            //}
            //}
            
            
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.tableView.insertRows(at: indexPathsForSection(),
                                      with: .fade)
        } else {
            self.hiddenSections.insert(section)
            self.tableView.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
}
extension InvestmentVC {
    func GetmyInvestAccountsList(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: myinvestURl, paramaters: parameters, httpMethodType: .get, success: { (response:MyinvestModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.MyInvesttableViewData = response.payload
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    func GetFoundManagersAccountsList(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: FoundManagersURL, paramaters: parameters, httpMethodType: .get, success: { (response:foundmanagersModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.FoundManagerstableViewData = response.payload
                self.tableView.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
