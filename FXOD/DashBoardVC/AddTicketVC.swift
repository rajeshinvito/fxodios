//
//  AddTicketVC.swift
//  FXOD
//
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DropDown
import CropViewController
import MobileCoreServices
import IQKeyboardManager
class AddTicketVC: UIViewController,UIDocumentMenuDelegate,UIDocumentPickerDelegate {
    
    
    var DepartmentArr = Array<String>()
    var Priorities = Array<String>()
    var Priorities_ID = Array<Int>()
    var DepartmentArr_Id = Array<Int>()
    var dropDown = DropDown()
    var imagePicked :Bool = false
    var imagePicker = UIImagePickerController()
    var filetype = ""
    var pickedImage = UIImage()
    var selectedDepartmentID = String()
    var selectedPriorityID = String()
    @IBOutlet var btnDepartmentList: UIButton!
    @IBOutlet var btnPriorityList: UIButton!
    @IBOutlet var textfeild_DepartmentList: UITextField!
    @IBOutlet var textfeild_PriorityList: UITextField!
    @IBOutlet var textfeild_Subject: UITextField!
    @IBOutlet var textView_Commit: IQTextView!

    @IBOutlet weak var ClickToUploadbtnref: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.tabBarController?.tabBar.isHidden = true

        //Api calling...
        self.AddTicketPrefetchData()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
                 self.tabBarController?.tabBar.isHidden = true

     }
//    override func viewDidDisappear(_ animated: Bool) {
//                            self.tabBarController?.tabBar.isHidden = false
//
//          }
    @IBAction func Backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    
    
    @IBAction func Departmentlistbtnref(_ sender: Any) {
        let viewSource = btnDepartmentList
        
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = DepartmentArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            //  self?.ChooseAccountTF.text = item
            self?.textfeild_DepartmentList.text = item
            self?.selectedDepartmentID = "\(self?.DepartmentArr_Id[index] ?? 0)"
            // = self?.AccountsArr2[index].account_number ?? ""
        }
        dropDown.show()
    }
    
    @IBAction func PriorityListbtnref(_ sender: Any) {
        let viewSource = btnPriorityList
        
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = Priorities
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.textfeild_PriorityList.text = item
            self?.selectedPriorityID = "\(self?.Priorities_ID[index] ?? 0)"

            // self?.existingaccountnumberbtnref.setTitle(item, for: .normal)
            // = self?.AccountsArr2[index].account_number ?? ""
        }
        dropDown.show()
    }
    
    @IBAction func Documentpickerbntref(_ sender: Any) {
        
        self.showActionSheet()
    }
    @IBAction func submitTicketbntref(_ sender: Any) {
        TicketStoreOnservermethod()
    }
    
    @IBAction func resetbtnref(_ sender: Any) {
        self.textfeild_DepartmentList.text = ""
        self.textfeild_PriorityList.text = ""
        self.textfeild_Subject.text = ""
        self.textView_Commit.text = ""
        self.ClickToUploadbtnref.setImage(#imageLiteral(resourceName: "Uploadfiles"), for: .normal)
        self.selectedDepartmentID = ""
        self.selectedPriorityID = ""
    }
    
}
extension AddTicketVC {
    func AddTicketPrefetchData(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: ticketPrefetchURL, paramaters: parameters, httpMethodType: .get, success: { (response:ticketPrefetchModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                for i in 0..<response.payload.departments.count {
                    self.DepartmentArr.append(response.payload.departments[i].deptname)
                    self.DepartmentArr_Id.append(response.payload.departments[i].deptid)
                }
                for i in 0..<response.payload.priorities.count {
                    self.Priorities.append(response.payload.priorities[i].priority)
                    self.Priorities_ID.append(response.payload.priorities[i].id)
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    func TicketStoreOnservermethod(){
        indicator.showActivityIndicator()
        guard let subject = textfeild_Subject.text else{
                     indicator.hideActivityIndicator()
                     return}
        guard let comment = textView_Commit.text else{
            indicator.hideActivityIndicator()
            return}
        
        let parameters = [
            "department":self.selectedDepartmentID,
            "subject":subject,
            "priority":self.selectedPriorityID,
            "comment":comment
        ]
     NetworkManager.MultiformApicalling(url: ticketStoreURL, paramaters: parameters, Images: [self.pickedImage], Imagesname: "upload", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ShowAlertWithPop(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
extension AddTicketVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    fileprivate func showActionSheet (){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        actionSheet.addAction(UIAlertAction(title: "Take a Photo", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromCamera()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.takePhotoFromGallery()
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Choose a file ", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.clickFunction()
        }))
        actionSheet.view.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    func takePhotoFromCamera(){
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.allowsEditing = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.cameraCaptureMode = .photo
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func takePhotoFromGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            
        }
    }
    
    //MARK: ImagePicker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            // let imageData = pickedImage.pngData()
            // let imageData = pickedImage.jpegData(compressionQuality: 1.0)
            let cropViewController = CropViewController(image: pickedImage)
            
            cropViewController.delegate = self
            cropViewController.aspectRatioLockEnabled = true
            cropViewController.aspectRatioPreset = .preset5x4
            
            //  cropViewController.aspectra
            
            self.navigationController?.pushViewController(cropViewController, animated: true)
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func clickFunction(){
        
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}

//MARK:- CropViewControllerDelegate Methods
extension AddTicketVC : CropViewControllerDelegate{
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.imagePicked = true
        self.pickedImage = image
        self.ClickToUploadbtnref.setImage(image, for: .normal)
        self.navigationController?.popViewController(animated: true)
    }
}
