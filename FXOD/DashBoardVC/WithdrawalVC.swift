
//
//  WithdrawalVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManager

class WithdrawalVC: UIViewController {
    //MARK:- -----  Class IBOutlets ------
    @IBOutlet weak var Back: UIButton!
    @IBOutlet var btnChooseAccount: UIButton!
    @IBOutlet var ChooseAccountTF: UITextField!
    
    @IBOutlet var Amounttfref: UITextField!
    @IBOutlet weak var Commenttextviewref: IQTextView!
    
    @IBOutlet weak var Newbtnref: UIButton!
    @IBOutlet weak var Existingbtnref: UIButton!
    @IBOutlet weak var existingAccountNumberTFref: UITextField!
    @IBOutlet weak var existingaccountnumberbtnref: UIButton!
    @IBOutlet weak var DropDownimgref: UIImageView!
    
    @IBOutlet weak var PaymentMethodbtnref: UIButton!
    @IBOutlet weak var Paymentmethodtfref: UITextField!
    @IBOutlet weak var PopUpviewref: UIView!
    @IBOutlet weak var tableviewref: UITableView!
    
    //MARK:- -------- Class Propeties -----
    var vcFROM = ""
    var AccountsArr2 = [accountsdata]()
    var existingAccountArr = [String]()
    var AccountsArr = [String]()
    var PaymentMethodsArr = [String]()
    var dropDown = DropDown()
    var PaymentMethodsArr2 = [payment_methodsdata]()
    var paymentIMG = [String]()
    var selectedIndexPath = IndexPath()
    var ToAccountType = String()
    var selectionpaymentID = Int()
    var ExistingaccountId = Int()
    var ExistingaccountIdArr = [Int]()
    var selectedAccountNumber = ""
    //MARK:- ------------ VIEW LYFE CYCLE ---------
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ToAccountType = "new"
        
        self.Newbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.Existingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.existingAccountNumberTFref.isHidden = false
        self.existingaccountnumberbtnref.isHidden = true
        self.DropDownimgref.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.PopUpviewref.isHidden = true
        self.Back.setImage(#imageLiteral(resourceName: "MenuWhite"), for: .normal)
        self.Back.backgroundColor = .clear
        self.Newbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.Existingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        // self.tabBarController?.tabBar.isHidden = true
        self.WithDrawalPrefetchData()
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
                         self.tabBarController?.tabBar.isHidden = false

       }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    //MARK:- --------- class button action ---------
    @IBAction func cmdMenu(_ sender: Any) {
        //        if vcFROM == "DashBoardVc" {
        //            //self.popToBackVC()
        //
        //
        //            let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        //                   let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
        //                   initialViewController.selectedIndex = 0
        //                   let navController = UINavigationController(rootViewController: initialViewController)
        //                   navController.setViewControllers([initialViewController], animated:true)
        //                   navController.isNavigationBarHidden = true
        //                   self.revealViewController().setFront(navController, animated: true)
        //                   self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
        //        }else {
        //   self.tabBarController?.tabBar.isHidden = true
        
        //            sharedMenuClass.menuObj.addMenuClass(view: self.view)
        //                      sharedMenuClass.menuObj.menuView.delegate = self
        Back.addTarget(revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
        // }
    }
    
    @IBAction func popuphidebtnref(_ sender: Any) {
        self.PopUpviewref.isHidden = true
    }
    
    @IBAction func resetBtnref(_ sender: Any) {
        self.ChooseAccountTF.text = ""
        self.selectedAccountNumber = ""
        self.Paymentmethodtfref.text = ""
        self.selectionpaymentID = 0
        self.Newbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.Existingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.existingAccountNumberTFref.isHidden = false
        self.existingaccountnumberbtnref.isHidden = true
        self.DropDownimgref.isHidden = true
        self.existingAccountNumberTFref.text = ""
        self.Amounttfref.text = ""
        self.Commenttextviewref.text = ""
        
    }
    @IBAction func cmdAccount(_ sender: Any) {
        let viewSource = btnChooseAccount
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = AccountsArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.ChooseAccountTF.text = item
            self?.selectedAccountNumber = self?.AccountsArr2[index].account_number ?? ""
        }
        dropDown.show()
        
    }
    
    @IBAction func Paymentmethodbtnref(_ sender: Any) {
        self.PopUpviewref.isHidden = false
        
    }
    
    @IBAction func newbtnref(_ sender: Any) {
        self.ToAccountType = "new"
        
        self.Newbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        self.Existingbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
        self.existingAccountNumberTFref.isHidden = false
        self.existingaccountnumberbtnref.isHidden = true
        self.DropDownimgref.isHidden = true
    }
    
    @IBAction func existingbtnref(_ sender: Any) {
        self.ToAccountType = "Exist"
        
        // TODO: - 👊 Validations -
        switch validatepaymentmethodDetails {
        case .valid:
            
            self.Newbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)
            self.Existingbtnref.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
            self.existingAccountNumberTFref.isHidden = true
            self.existingaccountnumberbtnref.isHidden = false
            self.DropDownimgref.isHidden = false
            self.existingaccountnumberbtnref.setTitle("select", for: .normal)
            self.ExistingaccountDeatails{
            }
            
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
        
    }
    
    @IBAction func Submitbtnref(_ sender: Any) {
        switch validatewithdrawalDetails {
        case .valid:
            self.WithDrawlAccountMethod()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    @IBAction func Existingaccountnumberbtnref(_ sender: Any) {
        let viewSource = existingaccountnumberbtnref
        
        dropDown.anchorView = viewSource
        dropDown.direction = .bottom
        dropDown.topOffset = CGPoint(x: 0, y: -30)
        dropDown.dataSource = existingAccountArr
        dropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            // Setup your custom UI components
            cell.optionLabel.textAlignment = .left
            cell.optionLabel.removeImage()
        }
        
        dropDown.selectionAction = { [weak self] (index, item) in
            //  self?.ChooseAccountTF.text = item
            self?.existingaccountnumberbtnref.setTitle(item, for: .normal)
            // = self?.AccountsArr2[index].account_number ?? ""
            self?.ExistingaccountId = self?.ExistingaccountIdArr[index] ?? 0
        }
        dropDown.show()
    }
    
}



extension WithdrawalVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PaymentMethodsArr2.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DepositPaymenttypeCell = tableView.dequeueReusableCell(withIdentifier: "DepositPaymenttypeCell", for: indexPath) as! DepositPaymenttypeCell
        if let ProfilePic = self.paymentIMG[indexPath.row] as? String {
            cell.Imgref.sd_setImage(with: URL(string: ProfilePic), placeholderImage: UIImage(named: "User"))
        }
        cell.titleLblref.text = PaymentMethodsArr2[indexPath.row].method_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.PopUpviewref.isHidden = true
        self.Paymentmethodtfref.text = PaymentMethodsArr[indexPath.row]
        self.selectionpaymentID = PaymentMethodsArr2[indexPath.row].id
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
//MARK:-   CLASS API HITTINGS   
extension WithdrawalVC{
    func WithDrawalPrefetchData(){
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
        ]
        NetworkManager.Apicalling(url: WithdrawalPrefetchURL, paramaters: parameters, httpMethodType: .post, success: { (response:DepositModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.AccountsArr2 = response.payload.accounts
                self.PaymentMethodsArr2 = response.payload.payment_methods
                for i in 0..<response.payload.accounts.count {
                    var accountstr = String()
                    accountstr += response.payload.accounts[i].account_number
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_group
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_type_title
                    accountstr += "-"
                    accountstr += response.payload.accounts[i].account_currency
                    self.AccountsArr.append(accountstr)
                }
                for i in 0..<response.payload.payment_methods.count {
                    self.PaymentMethodsArr.append(response.payload.payment_methods[i].method_name)
                    var imageurl = imageBaseurl + response.payload.payment_methods[i].icon
                    
                    
                    self.paymentIMG.append(imageurl)
                }
                //                    if self.PaymentMethodsArr.count % 2 == 0 {
                //                        self.CollectionViewheightref.constant = CGFloat(((self.PaymentMethodsArr.count + 2) / 3) * 130)
                //                    }else {
                //                        self.CollectionViewheightref.constant = CGFloat((self.PaymentMethodsArr.count / 3) * 130)
                //                    }
                self.tableviewref.reloadData()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    func ExistingaccountDeatails(_ completion: @escaping () -> Void){
        indicator.showActivityIndicator()
        let parameters = [
            "payment_method_id":"\(self.selectionpaymentID)",
        ]
        NetworkManager.Apicalling(url: ExistingAccountURL, paramaters: parameters, httpMethodType: .post, success: { (response:ExistingaccountModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                //  self.PaymentMethodsArr2 = response.payload.payment_methods
                for i in 0..<response.payload.count {
                    var accountstr = String()
                    accountstr += response.payload[i].detail
                    //                    accountstr += "-"
                    //                    accountstr += response.payload.accounts[i].account_group
                    //                    accountstr += "-"
                    //                    accountstr += response.payload.accounts[i].account_type_title
                    //                    accountstr += "-"
                    //                    accountstr += response.payload.accounts[i].account_currency
                    self.existingAccountArr.append(accountstr)
                    
                    self.ExistingaccountIdArr.append(response.payload[i].id)
                }
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
                
                
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    //MARK:- Withdrawl account
    func WithDrawlAccountMethod(){
        indicator.showActivityIndicator()
        var account_number = self.selectedAccountNumber
        var existingaccountId = "\(self.ExistingaccountId ?? 0)"
        guard let amount = Amounttfref.text else{
            indicator.hideActivityIndicator()
            return}
        var payment_method_id = "\(self.selectionpaymentID)"
        
        guard let existingaccountnumb = existingAccountNumberTFref.text else {
            indicator.hideActivityIndicator()
            return
        }
        guard let note = Commenttextviewref.text else{
            indicator.hideActivityIndicator()
            return}
        var parameters = [String:Any]()
        if self.ToAccountType == "new" {
            parameters = [
                "account_number":account_number,
                "amount":amount,
                "payment_method_id":payment_method_id,
                "new_old_option":"new",
                "my_account_details":existingaccountnumb,
                //"payment_method_detail_id":payment_method_id,
                "note":note
            ]
        }else {
            parameters = [
                "account_number":account_number,
                "amount":amount,
                "payment_method_id":payment_method_id,
                "new_old_option":"old",
                //my_account_details:
                "payment_method_detail_id":existingaccountId,
                "note":note
            ]
        }
        NetworkManager.Apicalling(url: WithdrawalAccountURL, paramaters: parameters, httpMethodType: .post, success: { (response:CreateLiveAccountModel) in
            //        NetworkManager.MultiformApicalling(url: DepositPrefetchURL, paramaters: parameters, Images: [self.UploadFilesImgref.image!], Imagesname: "receipt", httpMethodType: .post,  success: { (response:ProfilePicUpdateModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.ShowAlertWithPop(message: response.message)
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
}
//MARK: - Validation
extension WithdrawalVC {
    var validatepaymentmethodDetails:UserValidationState {
        if selectionpaymentID == 0 {
            return .invalid("Please choose payment Method")
            
        }
        
        return .valid
    }
    var validatewithdrawalDetails:UserValidationState {
        if Commenttextviewref.text ?? ""  == "" || Amounttfref.text ?? "" == "" || ChooseAccountTF.text ?? "" == ""{
            return .invalid(ATErrorMessage.login.required)
        }else if Commenttextviewref.text.count ?? 0 == 0 {
            return .invalid("Please add your commit")
        }else if Amounttfref.text?.count ?? 0 == 0 {
            return .invalid("Please enter amount")
        }else if ChooseAccountTF.text?.count ?? 0 == 0 {
            return .invalid("Please choose your account")
        }
        
        return .valid
    }
    
}
