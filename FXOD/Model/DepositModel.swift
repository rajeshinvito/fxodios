//
//  DepositModel.swift
//  FXOD
//
//  Created by rajesh gandru on 7/6/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class DepositModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = Depositdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Depositdata:Codable,Mappable {
    var accounts  = [accountsdata]()
    var payment_methods = [payment_methodsdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        accounts <- map ["accounts"]
        payment_methods <- map ["payment_methods"]
    }
}
class accountsdata:Codable,Mappable {
    var id = 0
    var user_id = 0
    var account_number = ""
    var account_type_id = ""
    var account_currency = ""
    var leverage = ""
    var master_password = ""
    var investor_password = ""
    var account_group = ""
    var account_type = ""
    var account_balance = 0
    var account_balance_usc = 0
    var account_id_manual_field = ""
    var status = ""
    var agent = ""
    var mam_manager_id = 0
    var slave_id = ""
    var mam_deposit = ""
    var mam_profit_eligible = ""
    var created_at = ""
    var updated_at = ""
    var account_type_title = ""
    var user_name = ""
    var user_email = ""
    var account_leverage = ""
    

    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        account_number  <- map["account_number"]
        account_type_id  <- map["account_type_id"]
        account_currency  <- map["account_currency"]
        leverage  <- map["leverage"]
        master_password  <- map["master_password"]
        investor_password  <- map["investor_password"]
        account_group  <- map["account_group"]
        account_type  <- map["account_type"]
        account_balance <- map["account_balance"]
        account_balance_usc <- map["account_balance_usc"]
        account_id_manual_field  <- map["account_id_manual_field"]
        status  <- map["status"]
        agent  <- map["agent"]
        mam_manager_id <- map["mam_manager_id"]
        slave_id  <- map["slave_id"]
        mam_deposit  <- map["mam_deposit"]
        mam_profit_eligible  <- map["mam_profit_eligible"]
        created_at  <- map["created_at"]
        updated_at  <- map["updated_at"]
        account_type_title  <- map["account_type_title"]
        user_name  <- map["user_name"]
        user_email  <- map["user_email"]
        account_leverage  <- map["account_leverage"]
        
        
        

    }
}
class payment_methodsdata:Codable,Mappable {
    var id = 0
    var method_name = ""
    var short_code = ""
    var icon = ""
    var deposit = 0
    var withdraw = 0
    var is_redirect = 0
    var created_at = ""
    var updated_at = ""
    var deleted_at = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        id <- map["id"]
        method_name  <- map["method_name"]
        short_code  <- map["short_code"]
        icon  <- map["icon"]
        deposit <- map["deposit"]
        withdraw <- map["withdraw"]
        is_redirect <- map["is_redirect"]
        created_at  <- map["created_at"]
        updated_at  <- map["updated_at"]
        deleted_at  <- map["deleted_at"]
    }
}
class TransferModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = Depositdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Transferdata:Codable,Mappable {
    var accounts  = [accountsdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        accounts <- map ["accounts"]
    }
}

class ExistingaccountModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = [Existingaccountdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Existingaccountdata:Codable,Mappable {
    var created_at = ""
    var deleted_at = ""
    var detail  = ""
    var id  = 0
    var payment_method_id = 0
    var updated_at = ""
    var user_id = 0
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
    created_at  <- map["created_at"]
    deleted_at  <- map["deleted_at"]
    detail   <- map["detail"]
    id  <- map["id"]
    payment_method_id <- map["payment_method_id"]
    updated_at  <- map["updated_at"]
    user_id <- map["user_id"]
        
    }
}
