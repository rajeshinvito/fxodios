
//
//  MyTransactionsModel.swift
//  FXOD
//
//  Created by rajesh gandru on 7/2/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class MytransactionsListModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = [MytransactionsListdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class MytransactionsListdata:Codable,Mappable {
    var id = 0
    var client = ""
    var account_number = 0
    var to_account = ""
    var payment_date = ""
    var amount = ""
    var payment_method = ""
    var status = ""
    var type = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        id <- map["id"]
        client <- map["client"]
        account_number <- map["account_number"]
        to_account <- map["to_account"]
        payment_date <- map["payment_date"]
        amount <- map["amount"]
        payment_method <- map["payment_method"]
        status <- map["status"]
        type <- map["type"]
    }
}
