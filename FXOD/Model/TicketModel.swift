//
//  TicketModel.swift
//  FXOD
//
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class TicketListModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = [Ticketdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Ticketdata:Codable,Mappable {
    var id = 0
    var code = ""
    var subject = ""
    var body = ""
    var status = ""
    var department = 0
    var user_id = 0
    var project_id = ""
    var priority = ""
    var due_date = ""
    var is_locked = 0
    var locked_by = ""
    var locked_time = ""
    var rated = 0
    var feedback_disabled = 0
    var closed_at = ""
    var closed_by = ""
    var token = ""
    var assignee = ""
    var resolution_time = 0
    var archived_at = ""
    var todo_percent = ""
    var deleted_at = ""
    var created_at = ""
    var updated_at = ""
    var response_status = ""
    var deptid = 0
    var deptname = ""
    var depthidden = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        id <- map["id"]
        code  <- map["code"]
        subject  <- map["subject"]
        body  <- map["body"]
        status  <- map["status"]
        department <- map["department"]
        user_id <- map["user_id"]
        project_id  <- map["project_id"]
        priority  <- map["priority"]
        due_date  <- map["due_date"]
        is_locked <- map["is_locked"]
        locked_by  <- map["locked_by"]
        locked_time  <- map["locked_time"]
        rated <- map["rated"]
        feedback_disabled <- map["feedback_disabled"]
        closed_at  <- map["closed_at"]
        closed_by  <- map["closed_by"]
        token  <- map["token"]
        assignee  <- map["assignee"]
        resolution_time <- map["resolution_time"]
        archived_at  <- map["archived_at"]
        todo_percent  <- map["todo_percent"]
        deleted_at  <- map["deleted_at"]
        created_at  <- map["created_at"]
        updated_at  <- map["updated_at"]
        response_status  <- map["response_status"]
        deptid <- map["deptid"]
        deptname  <- map["deptname"]
        depthidden  <- map["depthidden"]
    }
}


class ticketPrefetchModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = ticketPrefetchdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}

class ticketPrefetchdata : Codable,Mappable {
    var departments  = [ticketPrefetchDepartmentdata]()
       var priorities = [ticketPrefetchprioritiesdata]()
       required init?(map: Map) {}
       init() {}
       func mapping(map: Map) {
           departments <- map ["departments"]
           priorities <- map ["priorities"]
       }
}

class ticketPrefetchDepartmentdata:Codable,Mappable {
    var deptid = 0
    var deptname = ""
    var depthidden = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        deptid <- map ["deptid"]
        deptname <- map ["deptname"]
        depthidden <- map ["depthidden"]
        
    }
}
class ticketPrefetchprioritiesdata:Codable,Mappable {
    var id = 0
    var priority = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        id <- map ["id"]
        priority <- map ["priority"]
    }
}
