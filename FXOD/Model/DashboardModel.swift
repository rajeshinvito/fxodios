//
//  DashboardModel.swift
//  FXOD
//
//  Created by rajesh gandru on 6/17/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper


class DashboardModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = Dashboarddata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Dashboarddata:Codable,Mappable {
    var ib_menu  = 0
    var email_verify  = 0
    var document_verify  = 0
   
    var balance = Dashboarddata2()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        ib_menu <- map ["ib_menu"]
        email_verify <- map ["email_verify"]
        document_verify <- map ["document_verify"]
        document_verify <- map ["document_verify"]
        balance <- map ["balance"]


    }
}

class Dashboarddata2:Codable,Mappable {
    var total_amount  = 0
    var deposit  = 0
    var withdrawal  = 0
    var liveaccounts = 0
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        total_amount <- map ["total_amount"]
        deposit <- map ["deposit"]
        withdrawal <- map ["withdrawal"]
        liveaccounts <- map ["liveaccounts"]

    }
}



//{
//    "status": true,
//    "message": "Data fetched successfully!",
//    "payload": {
//        "balance": {
//            "total_amount": 1500,
//            "deposit": 1500,
//            "withdrawal": 0,
//            "liveaccounts": 12
//        },
//        "ib_menu": true,
//        "email_verify": true,
//        "document_verify": false
//    }
//}
