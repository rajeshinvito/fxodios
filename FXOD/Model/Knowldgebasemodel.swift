//
//  Knowldgebasemodel.swift
//  FXOD
//
//  Created by rajesh gandru on 7/12/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper

class KnowbasePrefetchModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = [KnowbasePrefetchdata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class KnowbasePrefetchdata:Codable,Mappable {
    var id = 0
    var name = ""
    var module = ""
    var color = ""
    var active = 0
    var order = 0
    var description = ""
    var pipeline = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
    
        id <- map["id"]
        name  <- map["name"]
        module  <- map["module"]
        color  <- map["color"]
        active <- map["active"]
        order <- map["order"]
        description  <- map["description"]
        pipeline  <- map["pipeline"]
        
    }
}
class KnowbasePrefetchsecondModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = [KnowbasePrefetchseconddata]()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class KnowbasePrefetchseconddata:Codable,Mappable {
    
    var id = 0
    var group = 0
    var subject = ""
    var slug = ""
    var description = ""
    var user_id = 0
    var order = 0
    var active = 0
    var views = 0
    var allow_comments = 0
    var created_at = ""
    var updated_at = ""
    var deleted_at = ""
    var comments_count = 0
    var rating = 0
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        
        id <- map["id"]
        group <- map["group"]
        subject  <- map["subject"]
        slug  <- map["slug"]
        description  <- map["description"]
        user_id <- map["user_id"]
        order <- map["order"]
        active <- map["active"]
        views <- map["views"]
        allow_comments <- map["allow_comments"]
        created_at  <- map["created_at"]
        updated_at  <- map["updated_at"]
        deleted_at  <- map["deleted_at"]
        comments_count <- map["comments_count"]
        rating <- map["rating"]
        
    }
}
