//
//  ProfileDetailsVCModel.swift
//  FXOD
//
//  Created by rajesh gandru on 6/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper
class ProfilePicUpdateModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = Profilepicdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}
class Profilepicdata:Codable,Mappable {
    var avatar = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        avatar <- map ["avatar"]
    }
}

class ProfileUpdateModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = ProfileDetailsdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}

class ProfileDetailsModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = ProfileDetailsdata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}


class ProfileDetailsdata:Codable,Mappable {
    var id = 0
    var username = ""
    var email  = ""
    var email_verified_at = ""
    var name = ""
    var phone = ""
    var banned = 0
    var ban_reason = ""
    var last_ip = ""
    var language = ""
    var last_login = ""
    var mobile_otp = ""
    var mobile_verify = 0
    var first_login = 0
    var slack_webhook_url = ""
    var device_id = ""
    var referrer_id = ""
    var ib_number = ""
    var partner_id = ""
    var relation_id = ""
    var type = ""
    var access_token = ""
    var email_preferences = ""
    var locale = ""
    var unsubscribed_at = ""
    var google2fa_enable = 0
    var google2fa_secret = ""
    var on_holiday = 0
    var created_at = ""
    var updated_at = ""
    var deleted_at = ""
    var has_chats = 0
    var whatsapp_optin = 0
    var token = ""
    var avatar = ""
    var document_verified = Bool()
    var id_proof_front = ""
    var id_proof_back = ""
    var address_proof = ""
    var address1 = ""
    var address2 = ""
    var country = ""
    var state = ""
    var city = ""
    var postcode = ""
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
         id <- map["id"]
         username  <- map["username"]
         email   <- map["email"]
         email_verified_at  <- map["email_verified_at"]
         name  <- map["name"]
         phone  <- map["phone"]
         banned <- map["banned"]
         ban_reason  <- map["ban_reason"]
         last_ip  <- map["last_ip"]
         language  <- map["language"]
         last_login  <- map["last_login"]
         mobile_otp  <- map["mobile_otp"]
         mobile_verify <- map["mobile_verify"]
         first_login <- map["first_login"]
         slack_webhook_url  <- map["slack_webhook_url"]
         device_id  <- map["device_id"]
         referrer_id  <- map["referrer_id"]
         ib_number  <- map["ib_number"]
         partner_id  <- map["partner_id"]
         relation_id  <- map["relation_id"]
         type  <- map["type"]
         access_token  <- map["access_token"]
         email_preferences  <- map["email_preferences"]
         locale  <- map["locale"]
         unsubscribed_at  <- map["unsubscribed_at"]
         google2fa_enable <- map["google2fa_enable"]
         google2fa_secret  <- map["google2fa_secret"]
         on_holiday <- map["on_holiday"]
         created_at  <- map["created_at"]
         updated_at  <- map["updated_at"]
         deleted_at  <- map["deleted_at"]
         has_chats <- map["has_chats"]
         whatsapp_optin <- map["whatsapp_optin"]
         token  <- map["token"]
         avatar  <- map["avatar"]
         document_verified  <- map["document_verified"]
         id_proof_front  <- map["id_proof_front"]
         id_proof_back  <- map["id_proof_back"]
         address_proof  <- map["address_proof"]
         address1  <- map["address1"]
         address2  <- map["address2"]
         country  <- map["country"]
         state  <- map["state"]
         city  <- map["city"]
         postcode  <- map["postcode"]
        
    }
}
/*
{
    "status": true,
    "message": "data fetched successfully.",
    "payload": {
        "id": 129,
        "username": "dakshsemwal35@gmail.com",
        "email": "dakshsemwal35@gmail.com",
        "email_verified_at": "2020-05-19 11:42:50",
        "name": "Daksh Semwal",
        "phone": "2124714307",
        "banned": 0,
        "ban_reason": null,
        "last_ip": "45.127.194.99",
        "language": "en",
        "last_login": "2020-06-24 08:28:14",
        "mobile_otp": "813961",
        "mobile_verify": 1,
        "first_login": 0,
        "slack_webhook_url": null,
        "device_id": null,
        "referrer_id": null,
        "ib_number": "2100191556",
        "partner_id": "6",
        "relation_id": null,
        "type": "client",
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiNjhiNjEyOGUyYjlmZDk0NTBhYzYzYWMxZjQxNDZiNDFhYTEyMTFhMzY1Y2UwNGFhOGFmY2JkZDM4ZDY2Njk5MTVmMDQzZWI5MjIzYzMxM2IiLCJpYXQiOjE1ODk4ODg1NzAsIm5iZiI6MTU4OTg4ODU3MCwiZXhwIjoxNjIxNDI0NTcwLCJzdWIiOiIxMjkiLCJzY29wZXMiOltdfQ.q8J8mNnpug5NDU_klnD7w1WurlAXGuV2Hkjja2vmiOE4dLAWWviXXWrQfa0_hzboC3CCo_dqNioieWtEhDdMvrz1s2AEV6sXWif-kaRs8GvGq3C08nft9Xb34m78HUIeIjQFIhrAQfzPCXeqlDIDo-cJFETE8Qma0iyyk5ztW5w8qHEBrRTlYuh9PVmxQCGicbdeDGkP5JEGa_cu26wLO2n0xRb1pqr0IManFWB3Oa8zhp6baVITwxaLBNPM3opnwZ5wD8nigEgc0OAfUpn2sAImMpnG2bGuqASKqOXE6gDtCiFuFtqiuUMpUMCn5VdVxxOX9gvMx0WZ55_QWMakt6T-GITNh1qUcBvghXIURbkQHhQZ9ylQEokrKFVRxYsEmvQ-jJsLWbJeNU0KOzyhaB5d0vAdMwPQpFdBUNmIGGgHiKKl4k-0lMV5zxZCAb5GrnASHKQF1K-q4iRYdnKpo0jQo2hW5TLS3R0uWyBWD2-i7SmVwLkS_D_cRbn1QJsaWP6aXTNdlRzvJRzVzjlHyTcvOyUQDO-Pg0RdmONR8kWuD4pJgJ1y8kdYv9wJev1hUH0sVrQ3saJYpmlb8VXWC2a2FZgVG9lBTUs2mLh7CB3lWYww3goebpODrx4aidZ4iPzjT1Eg7CoPhmouKiVhzt5DQhHF-kVYQ0C6phnAQp0",
        "email_preferences": null,
        "locale": "en",
        "unsubscribed_at": null,
        "google2fa_enable": 0,
        "google2fa_secret": null,
        "on_holiday": 0,
        "created_at": "2020-05-19 11:42:50",
        "updated_at": "2020-06-25 08:31:41",
        "deleted_at": null,
        "has_chats": 0,
        "whatsapp_optin": 0,
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiMjJhNGJhYTY0YWNiMzY4M2JjNTVmOGVlZDQzNDMxNjUxMWE0YTNjZTZhZTk1M2ZhYWUxOTMwZmU2NWMyMmZjZjBjMzYyYmZkMGY1MWJhNzEiLCJpYXQiOjE1OTMxNjI1NjIsIm5iZiI6MTU5MzE2MjU2MiwiZXhwIjoxNjI0Njk4NTYyLCJzdWIiOiIxMjkiLCJzY29wZXMiOltdfQ.QG_uux22B73lgYOjX2diCd0J78Z7lNOJ51_5sDdOTSxDqMtB37qMkbDjuMpIV5HV9PL36DdP3QK8hz45DtHq_y3huvOHDLTiDdFififJliJ0J3UKOz2wSRbqbl_wHDLwg8atFPE3wOaG8G8xGEmZ-oEY0dIEOPrFUfWKGHyXeS59akTkGM0GDJRRUxrVsK3H5iexjOeXYXli1uZrEnju9cukOZfAXhtVpzmJv535KsMWwJSJjrnJSyuuht59pogR_4niDjPeHUPSvuM6-qJ-vwAFO6195bS9XUeqwxPIbU4-18Dtzw6AYgAl-6i5KnmLdjahVaVc7934wgflqi7xWavrkZYVpqw-M3dCNUG-m4JNX-TrJL-Z6JJng4CM5hWBbssib7FOiXERyXCgtn2QepE7FAuDnJT0x0SB4VPu52-bOi0Gp-nlAq7Mm-JrG9wzJyUMA6prLCUqooxTpNCgAYTT3ogmPUWW8xlEidXkE-WsCLZfE2MHNjfECP7npOSqspUIzihsRdd9uxPFR6uVKOMi6A9ZTclovyyuYJc3o5byZ5fiBICG87X2l1ta1a0379aTguJa8jR_XYIlcYRIFl75zkg37QACZlKRi0cBB7X7UDl6-4jjCTt4ZxkGGHYmu-CQwzJGbDrNr0Rr_WuybtsL_NYgqFPXIr-G7-UoClI",
        "avatar": "https://clients.fxod.net/storage/uploads/profile/LrXporJrcSYQD9e0gNQZ9to0ypj58EUkDmD3QYz9.jpeg",
        "document_verified": false,
        "id_proof_front": "Pending",
        "id_proof_back": "",
        "address_proof": "",
        "address1": "799,Settlers Lane,New York",
        "address2": "799,Settlers Lane,New York",
        "country": "United States",
        "state": "New York",
        "city": "New York",
        "postcode": "10036"
    }
}
*/
