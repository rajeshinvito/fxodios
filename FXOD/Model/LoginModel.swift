
//
//  LoginModel.swift
//  FXOD
//
//  Created by rajesh gandru on 6/7/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import ObjectMapper


class userdataModel:Codable,Mappable {
    var status  = 0
    var message = ""
    var payload = userdatadata()
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {
        status <- map ["status"]
        message <- map ["message"]
        payload <- map ["payload"]
        
    }
}

class userdatadata : Codable,Mappable {

    var id = 0
    var username = ""
    var email = ""
    var email_verified_at = ""
    var name = ""
    var phone = ""
    var banned = 0
    var ban_reason = ""
    var last_ip = ""
    var language = ""
    var last_login = ""
    var mobile_otp = ""
    var mobile_verify = 0
    var first_login = 0
    var slack_webhook_url = ""
    var device_id = ""
    var referrer_id = ""
    var ib_number = ""
    var partner_id = ""
    var relation_id = ""
    var type = ""
    var access_token = ""
    var email_preferences = ""
    var locale = ""
    var unsubscribed_at = ""
    var google2fa_enable = 0
    var google2fa_secret = ""
    var on_holiday = 0
    var created_at = ""
    var updated_at = ""
    var deleted_at = ""
    var has_chats = 0
    var whatsapp_optin = 0
    var token = ""
    
    required init?(map: Map) {}
    init() {}
    func mapping(map: Map) {

 
        id  <- map ["id"]
        username <- map ["username"]
        email <- map ["email"]
        email_verified_at <- map ["email_verified_at"]
        name <- map ["name"]
        phone <- map ["phone"]
        banned  <- map ["banned"]
        ban_reason <- map ["ban_reason"]
        last_ip <- map ["last_ip"]
        language <- map ["language"]
        last_login <- map ["last_login"]
        mobile_otp <- map ["mobile_otp"]
        mobile_verify  <- map ["mobile_verify"]
        first_login  <- map ["first_login"]
        slack_webhook_url <- map ["slack_webhook_url"]
        device_id <- map ["device_id"]
        referrer_id <- map ["referrer_id"]
        ib_number <- map ["ib_number"]
        partner_id <- map ["partner_id"]
        relation_id <- map ["relation_id"]
        type <- map ["type"]
        access_token <- map ["access_token"]
        email_preferences <- map ["email_preferences"]
        locale <- map ["locale"]
        unsubscribed_at <- map ["unsubscribed_at"]
        google2fa_enable  <- map ["google2fa_enable"]
        google2fa_secret <- map ["google2fa_secret"]
        on_holiday  <- map ["on_holiday"]
        created_at <- map ["created_at"]
        updated_at <- map ["updated_at"]
        deleted_at <- map ["deleted_at"]
        has_chats  <- map ["has_chats"]
        whatsapp_optin  <- map ["whatsapp_optin"]
        token <- map ["token"]
    }
}



class SocialModel: NSObject {

    static let shared = SocialModel()
    
    var fillCount = 0
    var email: String?
    var id: String?
    var firstName: String?
    var lastName: String?
    var name: String?
    var profileImg:String?
    var gender:String?
    var dob:String?
}
