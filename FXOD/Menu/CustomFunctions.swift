//
//  CustomFunctions.swift
//  PTFinder
//
//  Created by Invito Software Solutions on 24/10/19.
//  Copyright © 2019 Invito Software Solutions. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    
    func Alert(title:String, message:String){
        
        let alert = UIAlertController(title: NSLocalizedString(title, comment: ""), message: NSLocalizedString(message, comment: "") , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

    
    func convertToLocalTime(_ str:String) ->String{
        if str != ""{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let dt = dateFormatter.date(from: str)
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "hh:mm a"
            
            return dateFormatter.string(from: dt!)

        }
        return ""
    }
    
    func calculateDateOfMessage(_ lastDateTime:String)->String{
        
        if lastDateTime != ""{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let date: Date? = dateFormatterGet.date(from: lastDateTime)
            return (dateFormatter.string(from: date!))

        }
        return ""
    }
    
    func gettingtimeFromShort(_ date:String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeStyle = .short
        let dateFormatterAPI = DateFormatter()
        dateFormatterAPI.dateFormat = "hh:mm a"
        let date_Date = dateFormatter.date(from: date)
        return date_Date!
    }

    func convertToTweleveHour(str:String) ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = dateFormatter.date(from: str)
        dateFormatter.dateFormat = "hh:mm a"
        let date24 = dateFormatter.string(from: date!)
        
        return date24
        
    }
    func convertDateFormate(str:String) ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: str)
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date24 = dateFormatter.string(from: date!)
        return date24
        
    }
    
    var topViewController: UIViewController? {
        return self.topViewController(currentViewController: self)
    }
    
    private func topViewController(currentViewController: UIViewController) -> UIViewController {
        if let tabBarController = currentViewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
            return self.topViewController(currentViewController: selectedViewController)
        } else if let navigationController = currentViewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return self.topViewController(currentViewController: visibleViewController)
        } else if let presentedViewController = currentViewController.presentedViewController {
            return self.topViewController(currentViewController: presentedViewController)
        } else {
            return currentViewController
        }
    }

    
}



