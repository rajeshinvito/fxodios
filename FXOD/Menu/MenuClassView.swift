//
//  MenuClassView.swift
//  JalfApp
//
//  Created by osx on 29/12/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit
//import RappleProgressHUD

protocol MenuDelegate {
    func callMethod(vcIndex:Int)
    func  touchEvents(_ direction:String)
}

let kNotificationMenuChanged = NSNotification.Name(rawValue:"kNotificationMenuChanged")
class MenuClassView: UIView,UITableViewDelegate,UITableViewDataSource{
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    let kHeaderSectionTag: Int = 6900;
    var collpseSection :Int = -1
    var delegate : MenuDelegate? = nil

    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var rightButtonOut: UIButton!
    @IBOutlet var customeView :UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImage: UIImageView!

    @IBOutlet weak var usernamelblref: UILabel!
    @IBOutlet weak var Documentlblref: UILabel!
    @IBOutlet weak var Documentimgref: UIImageView!
    
    @IBOutlet weak var Emailimgref: UIImageView!
    @IBOutlet weak var Emaillblref: UILabel!
    
    var subList = ["Block List","My Profile","Change Settings"]
    var menuClass = [MenuClass]()
    var VcInfo:[String] = []
    var VcIcons:[String] = []
    func initializingForMenu() {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(receiveMenuChangedNotification), name: kNotificationMenuChanged, object: nil)
        
        //            VcIcons = ["calendar","clock","Packages","mail-1","credit-card (1)","payment","logout"]
        //            VcInfo = ["Dashboard","Sessions","Manage Package","Message","Saved Cards","Subscription","Logout"]
        
        VcIcons = ["Home","MyAccount","Deposit","Withdrawal","Transfer","Investment","IBAccount","Quastion","Ticket","DataAnalysis","LogOut-blue"]
        VcInfo = ["Dashboard","My Accounts","Deposit","Withdrawal","Transfer","Investment","IB Account","Knowledgebase","Tickets","Daily Analysis","Logout"]
        menuClass.removeAll()
        menuClass.append(MenuClass(sectionName: VcInfo[0], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[1], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[2], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[3], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[4], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[5], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[6], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[7], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[8], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[9], rowsInSections: [],selected:false))
        menuClass.append(MenuClass(sectionName: VcInfo[10], rowsInSections: [],selected:false))
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        initializingForMenu()
        
        Bundle.main.loadNibNamed("menu", owner: self, options: nil)
        self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width*0.8, height: UIScreen.main.bounds.height)
        customeView.frame = self.frame
        addSubview(customeView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        
        //        profileImage.image = UIImage(named:"nobody")
        self.customeView.addGestureRecognizer(swipeLeft)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "MenuCellX", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        rightButtonOut.addTarget(self, action: #selector(buttonTouched), for: .touchUpInside)
        
        //username
        if let userName = UserDefaults.standard.string(forKey: "name") as? String {
            self.usernamelblref.text = userName
        }
        //Email
        if let IsEmailVerify = UserDefaults.standard.bool(forKey: "IsEmailVerify") as? Bool {
            if IsEmailVerify {
                self.Emailimgref.image = #imageLiteral(resourceName: "Emailverifyd")
                self.Emaillblref.text = "Email verified"
            }else {
                self.Emailimgref.image = #imageLiteral(resourceName: "DocumentNOTverified")
                self.Emaillblref.text = "Email not verified"
            }
        }
        //Document
        if let IsDocumentVerify = UserDefaults.standard.bool(forKey: "IsDocumentVerify") as? Bool {
            if IsDocumentVerify {
                self.Documentimgref.image = #imageLiteral(resourceName: "Emailverifyd")
                self.Documentlblref.text = "Document verified"
            }else {
                self.Documentimgref.image = #imageLiteral(resourceName: "DocumentNOTverified")
                self.Documentlblref.text = "Document not verified"
            }
        }
        
    }
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        
        switch gesture.direction {
        case UISwipeGestureRecognizer.Direction.right:
            delegate?.touchEvents("right")
        case UISwipeGestureRecognizer.Direction.down:
            delegate?.touchEvents("down")
        case UISwipeGestureRecognizer.Direction.left:
            delegate?.touchEvents("left")
        case UISwipeGestureRecognizer.Direction.up:
            delegate?.touchEvents("up")
        default:
            break
        }
    }
    
    @objc func buttonTouched(sender:UIButton) {
        delegate?.touchEvents("right")
        
    }
    @objc func receiveMenuChangedNotification(notification:NSNotification) {
        
        if notification.name == kNotificationMenuChanged {
            initializingForMenu()
            tableView.reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if collpseSection == section && menuClass[section].selected{
            return menuClass[section].rowsInSections.count
            
        }
        else {
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuClass.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            
            return tableView.frame.height/9
        }
        return tableView.frame.height / 6
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 60
        
    }
    @objc func sectionHeaderWasTouched(_ sender: UIButton) {
        
        guard let tag = sender.tag as? Int  else {
            return
        }
        print(tag)
        //        if tag != 5 {
        // navigate to the screen
        delegate?.callMethod(vcIndex:tag)
        //        }
        menuClass[tag].selected = !menuClass[tag].selected
        collpseSection = tag
        self.tableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuCustCell
        cell.label.text = menuClass[section].sectionName
        
        cell.backgroundColor = UIColor.clear
        cell.imageLeading.constant = 15
        cell.tag = section
        cell.buttonTapOut.tag = section
        if section == 0{
            
        }
        
        if section == 1{
            
        }
        cell.buttonTapOut.addTarget(self, action: #selector(sectionHeaderWasTouched), for: .touchUpInside)
        
        if  menuClass[section].rowsInSections.count == 0 {
            cell.downArrowImage.isHidden = true
            
        }
        else {
            cell.downArrowImage.isHidden = false
        }
        cell.icon.image = UIImage(named: VcIcons[section])
        return cell.contentView
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuCustCell
        cell.selectionStyle = .none
        cell.imageLeading.constant = 60
        cell.downArrowImage.isHidden = true
        cell.label.text = menuClass[indexPath.section].rowsInSections[indexPath.row]
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 ||  indexPath.row == 1 ||  indexPath.row == 2 || indexPath.row ==  3 || indexPath.row == 5
        {
            delegate?.callMethod(vcIndex:indexPath.row)
            print("Nothing is Selected")
            
        }
        else {
            
        }
    }
}

extension UIViewController {
    func navigationClass(index:Int) {
        sharedMenuClass.menuObj.touchEventsOnMainClass(view: self.view)
        
        var Vc = UIViewController()
        
        if index == 10{
            let alert = UIAlertController(title: "Logout?", message: "Are your Sure want to logout?",  preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertAction.Style.destructive, handler: { _ in
                //Cancel Action
            }))
            alert.addAction(UIAlertAction(title: "YES",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            self.logout()
                                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                            
                                            var navigation = UINavigationController()
                                            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                            let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                                            navigation = UINavigationController(rootViewController: initialViewControlleripad)
                                            appDelegate.window?.rootViewController = navigation
                                            appDelegate.window?.makeKeyAndVisible()
                                            
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        else{
            
            switch index {
            case 0:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            case 1:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
            case 2:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "DepositVC") as! DepositVC
            case 3:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "WithdrawalVC") as! WithdrawalVC
            case 4:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "TransferVC") as! TransferVC
            case 5:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "InvestmentVC") as! InvestmentVC
            case 6:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "IBAccountVC") as! IBAccountVC
            case 7:
                Vc = self.storyboard?.instantiateViewController(withIdentifier: "KnowledgebaseVC") as! KnowledgebaseVC
                //  case 9:
                //  Vc = self.storyboard?.instantiateViewController(withIdentifier: "TransferVC") as! TransferVC
                
                
            default:
                print("case 0")
            }
            
            DispatchQueue.main.async {
                var vcArray = self.navigationController?.viewControllers
                vcArray?.removeLast()
                vcArray?.append(Vc)
                if index == 0 || index == 1 || index == 2 {
                    self.tabclass(selectedIndex : index)
                }else {
                self.navigationController?.setViewControllers(vcArray!, animated: true)
                }
            }
            
        }
        
    }
    
    
    func logout(){
        
        indicator.showActivityIndicator()
        let parameters = [
            "":"",
            
        ]
        NetworkManager.Apicalling(url: logOUTUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(false, forKey: "LoggedIn")
                UserDefaults.standard.removeObject(forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "user_id")
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set("", forKey: "Accesstoken")
                UserDefaults.standard.set("", forKey: "name")
                
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    
    
    func tabclass(selectedIndex : Int){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        var nav1 = UINavigationController()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        /*
        //First VC
        let first: DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
        nav1.viewControllers = [first]
        nav1.setNavigationBarHidden(true, animated: false)
        nav1.title = "first"
        let item = UITabBarItem()
       // item.title = "Home"
        item.image = UIImage(named: "Home")
        nav1.tabBarItem = item

        //Second VC
        let nav2 = UINavigationController()
        let second: MyAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
        nav2.viewControllers = [second]
        nav2.setNavigationBarHidden(true, animated: false)
        let item2 = UITabBarItem()
       // item2.title = "Home"
        item2.image = UIImage(named: "MyAccount")
        nav2.tabBarItem = item2
        
        //third VC
        let nav3 = UINavigationController()
        let third: DepositVC = mainStoryboard.instantiateViewController(withIdentifier: "DepositVC") as! DepositVC
        nav3.viewControllers = [third]
        nav3.setNavigationBarHidden(true, animated: false)
        let item3 = UITabBarItem()
       // item3.title = "Home"
        item3.image = UIImage(named: "Deposit")
        nav3.tabBarItem = item3
        
        //third VC
        let nav4 = UINavigationController()
        let fourth: CommonChatVC = mainStoryboard.instantiateViewController(withIdentifier: "CommonChatVC") as! CommonChatVC
        nav4.viewControllers = [fourth]
        nav4.setNavigationBarHidden(true, animated: false)
        let item4 = UITabBarItem()
       // item4.title = "Home"
        item4.image = UIImage(named: "MyTransactions")
        nav4.tabBarItem = item4
        
        let tabController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController//tabbarVCViewController()
        tabController.viewControllers = [nav1,nav2,nav3,nav4]
        tabController.selectedIndex = selectedIndex
        
        
       */
        
        let initialViewControlleripad : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
        initialViewControlleripad.selectedIndex = selectedIndex
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
       // nav1.isNavigationBarHidden = true
        nav1 = UINavigationController(rootViewController: initialViewControlleripad)
        //self.navigationController?.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = nav1
        appDelegate.window?.makeKeyAndVisible()
//        appDelegate.window!.rootViewController = tabController
//        appDelegate.window?.makeKeyAndVisible()
    }
    
}

class MenuClass {
    
    var sectionName = String()
    var rowsInSections = [String]()
    var selected = Bool()
    
    init(sectionName:String,rowsInSections:[String],selected:Bool) {
        self.sectionName = sectionName
        self.rowsInSections = rowsInSections
        self.selected = selected
    }
}
