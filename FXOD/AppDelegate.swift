//
//  AppDelegate.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import CoreData
import FBSDKCoreKit
import GoogleSignIn
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate {
    
    var window: UIWindow?
    var tabBarController: UITabBarController?

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //MARK:-
        //Integrate FBSDK
        //ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = "576848796975-3qujk19r8d4jsm7s3b7kiqgr283iocjs.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        self.navigateToPage()
        return true
    }
    
    //MARK:- Google sign in Delegates
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print(user)
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    
    func navigateToPage(){
        
        let login = UserDefaults.standard.bool(forKey: "LoggedIn")
        let isguideopen = UserDefaults.standard.bool(forKey: "isguideopen")
        var navigation = UINavigationController()
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let DashBoardStoryboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        
        if login{
            let initialViewControllertabbar : tabbarVCViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
             let initialViewController : DashBoardVC = DashBoardStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            let SliderMenuVCController : SliderMenuVC = DashBoardStoryboard.instantiateViewController(withIdentifier: "SliderMenuVC") as! SliderMenuVC

//            self.window = UIWindow(frame: UIScreen.main.bounds)//
//            navigation.isNavigationBarHidden = true
//            navigation = UINavigationController(rootViewController: initialViewControllertabbar)
//            self.tabclass(selectedIndex : 0)
            
//            var nextViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
//            nextViewController = SWRevealViewController.init(rearViewController: SliderMenuVCController,frontViewController: initialViewControllertabbar)
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            navigation.isNavigationBarHidden = true
//            navigation = UINavigationController(rootViewController: nextViewController)
            
            
           // self.window?.rootViewController = nextViewController
          //  self.window?.makeKeyAndVisible()
            
//            let navSidebar = UINavigationController(rootViewController: SliderMenuVCController)
//             navSidebar.navigationBar.isHidden = true
//
//            let navDashboard = UINavigationController(rootViewController: initialViewControllertabbar)
//            navDashboard.navigationBar.isHidden = true
//
//            let mainRevealController = SWRevealViewController.init(rearViewController: navSidebar,frontViewController: navDashboard)
//
//               navigation.isNavigationBarHidden = true
//            navigation = UINavigationController(rootViewController: mainRevealController!)
            
            
          //  AppDelegate().window?.rootViewController = mainRevealController
         //   mainRevealController?.pushFrontViewController(navigation, animated: true)
            
            
            
          //  let initialViewController : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
//            initialViewControllertabbar.selectedIndex = 0
//        let navController = UINavigationController(rootViewController: initialViewControllertabbar)
//        navController.setViewControllers([initialViewControllertabbar], animated:true)
//        navController.isNavigationBarHidden = true
       // self.revealViewController().setFront(navController, animated: true)
       // self.revealViewController().setFrontViewPosition(FrontViewPosition.left, animated: true)
//       let mainRevealController = SWRevealViewController.init(rearViewController: SliderMenuVCController,frontViewController: initialViewControllertabbar)
//            navigation = UINavigationController(rootViewController: mainRevealController!)
            //AppDelegate().window?.rootViewController = mainRevealController
           // mainRevealController?.pushFrontViewController(initialViewControllertabbar, animated: true)

            var destinationController = DashBoardStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as? tabbarVCViewController

          var frontNavigationController = UINavigationController(rootViewController: destinationController!)

            var rearViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "SliderMenuVC") as? SliderMenuVC

          var mainRevealController = SWRevealViewController()

          mainRevealController.rearViewController = rearViewController
          mainRevealController.frontViewController = frontNavigationController
          self.window!.rootViewController = mainRevealController
          self.window?.makeKeyAndVisible()
        }else{
            if isguideopen == true {
                let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.window = UIWindow(frame: UIScreen.main.bounds)
                navigation.isNavigationBarHidden = true
                navigation = UINavigationController(rootViewController: initialViewControlleripad)
            }else {
                let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "UserguideVCViewController") as! UserguideVCViewController
                self.window = UIWindow(frame: UIScreen.main.bounds)
                navigation.isNavigationBarHidden = true
                navigation = UINavigationController(rootViewController: initialViewControlleripad)
            }
        
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
        }
        
    }
    
    func tabclass(selectedIndex : Int){
          //  let appDelegate = UIApplication.shared.delegate as! AppDelegate
          //  appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            var nav1 = UINavigationController()
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
            
            //First VC
            let first: DashBoardVC = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as! DashBoardVC
            nav1.viewControllers = [first]
            nav1.setNavigationBarHidden(true, animated: false)
            nav1.title = "first"
            let item = UITabBarItem()
           // item.title = "Home"
            item.image = UIImage(named: "Home")
            nav1.tabBarItem = item

            //Second VC
            let nav2 = UINavigationController()
            let second: MyAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
            nav2.viewControllers = [second]
            nav2.setNavigationBarHidden(true, animated: false)
            let item2 = UITabBarItem()
           // item2.title = "Home"
            item2.image = UIImage(named: "MyAccount")
            nav2.tabBarItem = item2
            
            //third VC
            let nav3 = UINavigationController()
            let third: DepositVC = mainStoryboard.instantiateViewController(withIdentifier: "DepositVC") as! DepositVC
            nav3.viewControllers = [third]
            nav3.setNavigationBarHidden(true, animated: false)
            let item3 = UITabBarItem()
           // item3.title = "Home"
            item3.image = UIImage(named: "Deposit")
            nav3.tabBarItem = item3
            
            //third VC
            let nav4 = UINavigationController()
            let fourth: CommonChatVC = mainStoryboard.instantiateViewController(withIdentifier: "CommonChatVC") as! CommonChatVC
            nav4.viewControllers = [fourth]
            nav4.setNavigationBarHidden(true, animated: false)
            let item4 = UITabBarItem()
           // item4.title = "Home"
            item4.image = UIImage(named: "MyTransactions")
            nav4.tabBarItem = item4
            
            let tabController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController//tabbarVCViewController()
            tabController.viewControllers = [nav1,nav2,nav3,nav4]
            tabController.selectedIndex = selectedIndex
            var navigation = UINavigationController()

             navigation.isNavigationBarHidden = true
            navigation = UINavigationController(rootViewController: tabController)
           
            
    //        let initialViewControlleripad : UITabBarController = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as! tabbarVCViewController
    //        initialViewControlleripad.selectedIndex = selectedIndex
    //        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
    //       // nav1.isNavigationBarHidden = true
    //        nav1 = UINavigationController(rootViewController: initialViewControlleripad)
    //        //self.navigationController?.navigationBar.isHidden = true
    //        appDelegate.window?.rootViewController = nav1
    //        appDelegate.window?.makeKeyAndVisible()
             self.window?.rootViewController = navigation
                 self.window?.makeKeyAndVisible()
        }
}

