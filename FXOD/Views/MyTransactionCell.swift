

//
//  MyTransactionCell.swift
//  FXOD
//
//  Created by rajesh gandru on 6/25/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class MyTransactionCell: UITableViewCell {

    @IBOutlet weak var typeTransactionIMGref: UIImageView!
    @IBOutlet weak var TransactionTitlelblref: UILabel!
    @IBOutlet weak var TransactionDatelblref: UILabel!
    @IBOutlet weak var TransactionIDlblref: UILabel!
    @IBOutlet weak var TransactionStatuslblref: UILabel!
    @IBOutlet weak var TransactionAmountlblref: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
