//
//  SlideMenuTableViewCell.swift
//  DBHUser
//
//  Created by Anaad on 1/8/19.
//  Copyright © 2019 DriverByHour. All rights reserved.


import UIKit

class SlideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_TitleRef: UILabel!
    @IBOutlet weak var imageview_Ref: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
