//
//  Alert.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.


import Foundation
import UIKit

extension UIViewController {
   
     func ShowAlert(message : String){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
     }
    
    func ShowAlertWithPop(message : String){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.popToBackVC()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func ShowAlertWithDismiss(message : String){
           let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
           let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
               self.dismiss()
           }
           alertController.addAction(OKAction)
           self.present(alertController, animated: true, completion: nil)
       }
    
    func ShowAlertWithpush(message : String,id: String,StoryID: String){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.movetonextvc(id: id, storyBordid: StoryID)
        }

        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    

   func showAlertAppDelegate(title : String,message : String,buttonTitle : String,window: UIWindow){
       let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            
        }
        alertController.addAction(OKAction)
    window.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertLogOut(){
       let alertController = UIAlertController(title: kApptitle, message: "Are you sure you want logout?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            UserDefaults.standard.set(false, forKey: "LoggedIn")
            self.movetonextvc(id: "LoginVC", storyBordid: "Main")
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .default) { (UIAlertAction) in
            
        }
        alertController.addAction(CancelAction)
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion: nil)

    }

    func showAlertAppDelegateDoublelogincheck(title : String,message : String,buttonTitle : String,window: UIWindow){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
              let OKAction = UIAlertAction(title: buttonTitle, style: .default) { (UIAlertAction) in
                UserDefaults.standard.set(false, forKey: "LoggedIn")
                UserDefaults.standard.set("", forKey: "user_id")
                UserDefaults.standard.set(0, forKey: "purchase")
                UserDefaults.standard.set("", forKey: "email")
                UserDefaults.standard.set(0, forKey: "login_status")
              }
              alertController.addAction(OKAction)
          window.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
   
 
    
    func ShowAlertWithAllvcDismiss(message : String){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)              }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertwithparticularpopvc(vc : UIViewController,message : String){
        let alertController = UIAlertController(title: kApptitle, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            self.navigationController?.backToViewController(vc: vc.self)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    func alertWithTF() {
        //Step : 1
        let alert = UIAlertController(title: "TRIAGE911", message: "Please enter pin for access documents", preferredStyle: UIAlertController.Style.alert )
        //Step : 2
        let save = UIAlertAction(title: "Ok", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            let textField2 = alert.textFields![1] as UITextField
            if textField.text != "" {
                //Read TextFields text data
                print(textField.text!)
                print("TF 1 : \(textField.text!)")
            } else {
                print("TF 1 is Empty...")
            }

           
        }

        //Step : 3
        //For first TF
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your first name"
            textField.textColor = .red
        }
       

        //Step : 4
        alert.addAction(save)
        //Cancel action
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (alertAction) in
            
        }
        alert.addAction(cancel)
        //OR single line action
        //alert.addAction(UIAlertAction(title: "Cancel", style: .default) { (alertAction) in })

        self.present(alert, animated:true, completion: nil)

    }
    
}

extension UINavigationController {

   func backToViewController(vc: Any) {
      // iterate to find the type of vc
      for element in viewControllers as Array {
        if "\(type(of: element)).Type" == "\(type(of: vc))" {
            self.popToViewController(element, animated: true)
            break
         }
      }
   }

}
extension UITextView {

    func addDoneButton(title: String, target: Any, selector: Selector) {

        let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                              y: 0.0,
                                              width: UIScreen.main.bounds.size.width,
                                              height: 44.0))//1
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
        let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
        toolBar.setItems([flexible, barButton], animated: false)//4
        self.inputAccessoryView = toolBar//5
    }
}
