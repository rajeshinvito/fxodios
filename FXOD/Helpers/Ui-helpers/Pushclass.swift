//
//  Pushclass.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/24/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func movetonextvc(id:String,storyBordid : String){
        let Storyboard : UIStoryboard = UIStoryboard(name: storyBordid, bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(nxtVC, animated: true)
    }
    
   func movetonextvcwithAnimationfalse(id:String,storyBordid : String){
         let Storyboard : UIStoryboard = UIStoryboard(name: storyBordid, bundle: nil)
         let nxtVC = Storyboard.instantiateViewController(withIdentifier: id)
         self.navigationController?.pushViewController(nxtVC, animated: false)
     }
    func popToBackVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismiss(){
        self.dismiss(animated: true, completion: nil)
    }
    func uiAnimate(view : NSLayoutConstraint, Constratint : Float){
        UIView.animate(withDuration:0.2, delay: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: {
            view.constant = CGFloat(Constratint)
              self.view.layoutIfNeeded()
          }, completion: nil)
    }
    
    func show(_ view : UIViewController) {
           let win = UIWindow(frame: UIScreen.main.bounds)
           let vc = view
           vc.view.backgroundColor = .clear
           win.rootViewController = vc
           win.windowLevel = UIWindow.Level.alert + 1  // Swift 3-4: UIWindowLevelAlert + 1
           win.makeKeyAndVisible()
           vc.present(self, animated: true, completion: nil)
       }
    
    func movetofromTicketlist(Ticket_data : Ticketdata){
        let Storyboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let nxtVC = Storyboard.instantiateViewController(withIdentifier: "TicketDetailsVC") as! TicketDetailsVC
        nxtVC.Ticketlidtdata = Ticket_data
        self.navigationController?.pushViewController(nxtVC, animated: true)
    }
    
   
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .none
    }
}

