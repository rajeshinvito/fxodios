//
//  ApiUrls.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation

let url = "https://clients.fxod.net/api/v1/user/"
let imageBaseurl = "https://clients.fxod.net/"

//------------
let loginUrl = url + "login"
let forgotUrl = url + "forgot/password"
let SignUpUrl = url + "register"
let OtpVerify = url + "verify-otp"

//-------------------------
let logOUTUrl = url + "logout"

//--------------- Dash board urls -------------

let DashBoardUrl = url + "dashboard"



//------------------------- Profile Details ----------
let ProfileDetailsUrl = url + "profile"
let ProfileUpdateUrl = url + "profile/update"
let ProfilepicUpdateUrl = url + "profile/image/upload"
let ChangePasswordUrl = url + "password/update"
let ProfileDocumentuploadUrl = url + "profile/document/upload"


//--------------------------get My Account Details -------------
let LiveAccountListURL = url + "liveaccounts"
let DemoAccountListURL = url + "demoaccounts"


//-----------------------------Live Account Creation --------------

let LivePrefetchDataURl = url + "lookup/accounttypes/live"
let CreateLiveAccountURl = url + "liveaccounts/add"

let DemoPrefetchDataURl = url + "lookup/accounttypes/demo"
let CreateDemoAccountURl = url + "demoaccounts/add"


//---------------------------- All My Transactions ------------------

let AllTransactionURL = url + "transaction/all"

//----------------------------  Deposit ------------------
let DepositPrefetchURL = url + "deposit/accounts"
let DepositAddURL = url + "deposit/add"


//----------------------------  Transfer ------------------
let TransferPrefetchURL = url + "transfer/accounts"
let TransferAddURL = url + "transfer/add"


//----------------------------  Invest ------------------
let myinvestURl = url + "mam/my_investment_accounts"
let FoundManagersURL = url + "mam/mam_manager"

let myinvestAccountprefetchURl = url + "lookup/accounttypes/investment"
let myinvestAccountURl = url + "liveaccounts/mam/add"


//----------------------------  Withdrawal ------------------
let WithdrawalPrefetchURL = url + "withdrawal/accounts"
let ExistingAccountURL = url + "withdrawal/info"
let WithdrawalAccountURL = url + "withdrawal/add"

//let DepositAddURL = url + "deposit/add"


//-------------------------- Ticket list --------------
let ticketListURL = url + "tickets"
let ticketPrefetchURL = url + "ticket/create"
let ticketStoreURL = url + "ticket/store"
let KnowldgecategoriesbaseURL = url + "categories/knowledgebase"
let KnowldgebaseURL = url + "knowledgebase"
