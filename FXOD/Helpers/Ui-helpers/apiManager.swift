//
//  apiManager.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.

import Foundation
import Alamofire
import ObjectMapper

enum DataNotFound:Error {
    case dataNotFound
}

typealias successs = (Data)-> Void
typealias failure = (Any)-> Void
typealias Response = [String:Any]


func header()->HTTPHeaders{
    let bearerToken = UserDefaults.standard.string(forKey:"Accesstoken") ?? ""
    var headerMessage :HTTPHeaders!
    if bearerToken != ""{
        headerMessage = [
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization":"Bearer \(bearerToken)"]
        
    }else{
        headerMessage = [
            "Content-Type":"application/x-www-form-urlencoded","Accept":"application/json" ]
    }
    return headerMessage
}

class NetworkManager{
    var alamoFireManager : SessionManager?
    private init(){}
    
    static  var alamoFireManager : SessionManager?
    
    
    
    static func Apicalling<T:Mappable>(
        url:String,
        paramaters:Response = [:],
        httpMethodType:HTTPMethod = .post ,
        success:@escaping(T)-> () ,
        failure: @escaping failure)
    {
        if Connectivity.isNotConnectedToInternet{
            failure(ATErrorMessage.Server.checkConnection)
        }
        guard let httpMethod = HTTPMethod(rawValue: httpMethodType.rawValue) else{
            assertionFailure("OOPS !!! HTTP Method Not Found \(httpMethodType)")
            return
        }
        print(" ---------- T ** Model  ---------- \n ","\(T.self)")
        print(" ---------- HTTP Method  ---------- \n",httpMethod)
        print(" ----------  URL  ---------- \n",url)
        print(" ---------- parameters  ---------- \n",paramaters)
        print(" ---------- Header  ---------- \n",header())
        
        let configuration = URLSessionConfiguration.default
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        alamoFireManager?.request(url, method: httpMethod, parameters: paramaters, encoding:URLEncoding.default, headers: header()).responseJSON { (response) in
            print(response.response?.statusCode as Any)
            switch response.result{
            case .success(let value):
                guard let responseDict = value as? Response else{return}
                print(" ---------- responseDict  ---------- \n",responseDict)
                guard let item = Mapper<T>.init().map(JSON: responseDict) else{
                    if let error = response.result.error{
                        failure(error)
                    }
                    return
                }
                print(" ---------- MODEL  ---------- \n",item)
                guard let statusCode = response.response?.statusCode,statusCode == 200 else{
                    if let errorMessage = responseDict["message"] as? String{
                        failure(errorMessage)
                    }
                    return
                }
                success(item)
            case .failure(let error):
                failure(error)
            }
        }.session.finishTasksAndInvalidate()
    }
    
    
    
    static  func MultiformApicalling<T:Mappable>(
        url:String,
        paramaters:Response = [:],
        Images:[UIImage] = [],
        Imagesname : String,
        httpMethodType:HTTPMethod = .post ,
        success:@escaping(T)-> () ,
        failure: @escaping failure)
    {
        if Connectivity.isNotConnectedToInternet{
            failure(ATErrorMessage.Server.checkConnection)
        }
        guard let httpMethod = HTTPMethod(rawValue: httpMethodType.rawValue) else{
            assertionFailure("OOPS !!! HTTP Method Not Found \(httpMethodType)")
            return
        }
        print(" ---------- T ** Model  ---------- \n ","\(T.self)")
        print(" ---------- HTTP Method  ---------- \n",httpMethod)
        print(" ----------  URL  ---------- \n",url)
        print(" ---------- parameters  ---------- \n",paramaters)
        print(" ---------- Header  ---------- \n",header())
        
        
        
        //  let imgData = UIImageJPEGRepresentation(UIImage(named: "1.png")!,1)
        
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                for (key, value) in paramaters {
                    // MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8), withName: key)
                    if value is String {
                        if let temp = value as? String {
                            MultipartFormData.append(temp.description.data(using: .utf8)!, withName: key)
                        }
                    }
                }
                for i in 0..<Images.count {
                    MultipartFormData.append(Images[i].jpegData(compressionQuality: 1)!, withName: Imagesname, fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                }
        }, to: url) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    print(response.result.value)
                    guard let responseDict = response.result.value as? Response else{return}
                    print(" ---------- responseDict  ---------- \n",responseDict)
                    guard let item = Mapper<T>.init().map(JSON: responseDict) else{
                        if let error = response.result.error{
                            failure(error)
                        }
                        return
                    }
                    print(" ---------- MODEL  ---------- \n",item)
                    guard let statusCode = response.response?.statusCode,statusCode == 200 else{
                        if let errorMessage = responseDict["message"] as? String{
                            failure(errorMessage)
                        }
                        return
                    }
                    success(item)
                }
                
            case .failure(let encodingError): break
            print(encodingError)
            failure(encodingError)
                
            }

        }
    }
}





//    
//    
//    
//    
//    static func GETApicalling<T:Mappable>(
//        url:String,
//        paramaters:Response = [:],
//        httpMethodType:HTTPMethod = .get ,
//        success:@escaping(T)-> () ,
//        failure: @escaping failure)
//    {
//        if Connectivity.isNotConnectedToInternet{
//            failure(ATErrorMessage.Server.checkConnection)
//        }
//        guard let httpMethod = HTTPMethod(rawValue: httpMethodType.rawValue) else{
//            assertionFailure("OOPS !!! HTTP Method Not Found \(httpMethodType)")
//            return
//        }
//        print(" ---------- T ** Model  ---------- \n ","\(T.self)")
//        print(" ---------- HTTP Method  ---------- \n",httpMethod)
//        print(" ----------  URL  ---------- \n",url)
//        print(" ---------- parameters  ---------- \n",paramaters)
//        print(" ---------- Header  ---------- \n",header())
//        
//        Alamofire.request(url, method: .get, encoding: JSONEncoding.default)
//            .responseJSON { (response) in
//                switch response.result{
//                case .success(let value):
//                    guard let responseDict = value as? Response else{return}
//                    print(" ---------- responseDict  ---------- \n",responseDict)
//                    guard let item = Mapper<T>.init().map(JSON: responseDict) else{
//                        if let error = response.result.error{
//                            failure(error)
//                        }
//                        return
//                    }
//                    print(" ---------- MODEL  ---------- \n",item)
//                    guard let statusCode = response.response?.statusCode,statusCode == 200 else{
//                        if let errorMessage = responseDict["message"] as? String{
//                            failure(errorMessage)
//                        }
//                        return
//                    }
//                    success(item)
//                case .failure(let error):
//                    failure(error)
//                }
//        }
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        
//        //           let configuration = URLSessionConfiguration.default
//        //           alamoFireManager = Alamofire.SessionManager(configuration: configuration)
//        //           alamoFireManager?.request(url, method: httpMethod, parameters: paramaters, encoding:  URLEncoding.default, headers: header()).responseJSON { (response) in
//        //               print(response.response?.statusCode as Any)
//        //               switch response.result{
//        //               case .success(let value):
//        //                   guard let responseDict = value as? Response else{return}
//        //                   print(" ---------- responseDict  ---------- \n",responseDict)
//        //                   guard let item = Mapper<T>.init().map(JSON: responseDict) else{
//        //                       if let error = response.result.error{
//        //                           failure(error)
//        //                       }
//        //                       return
//        //                   }
//        //                   print(" ---------- MODEL  ---------- \n",item)
//        //                   guard let statusCode = response.response?.statusCode,statusCode == 200 else{
//        //                       if let errorMessage = responseDict["message"] as? String{
//        //                           failure(errorMessage)
//        //                       }
//        //                       return
//        //                   }
//        //                   success(item)
//        //               case .failure(let error):
//        //                   failure(error)
//        //               }
//        //           }.session.finishTasksAndInvalidate()
//    }
//    
//}
///*
// 
// import Alamofire // pod 'Alamofire'
// import SDWebImage // pod 'SDWebImage', '~> 4.0'
//
// //remoteLogin JSON Parsing
// func remoteLogin()
// {
//     let params : Parameters = [
//         "user": "username",
//         "pass": "password"
//     ]
//
//     print("param is \(params)")
//
//     let url = URL(string: "\(Constant.API)login")!
//     print("Url printed \(url)")
//
//     let manager = Alamofire.SessionManager.default
//     manager.session.configuration.timeoutIntervalForRequest = 120
//
//     manager.request(url, method: .post, parameters: params)
//         .responseJSON
//         {
//             response in
//
//             switch (response.result)
//             {
//             case .success:
//
//                 //do json stuff
//                 let jsonResponse = response.result.value as! NSDictionary
//                 print("JsonResponse printed \(jsonResponse)")
//
//                 //check authenticate is yes or no..
//                 let myInt: Bool = jsonResponse["authenticate"]! as! Bool
//
//                 if (myInt == true)
//                 {
//                     //do Success stuff
//
//                     //If you want to use Array then
//                     //var data = NSArray()
//                     //data = dicsResponse.value(forKey: "data") as! NSArray
//
//                     //If you are using table then get the data array value like this
//                     //let name = (data[indexPath.row] as! NSDictionary).value(forKey: "name") as? String
//                     //print("GetNameData \(name)")
//
//                     //If you want to use Image
//                     //let url = URL.init(string:"\(companyLogo!)")
//                     //Img_Logo!.sd_setImage(with: url, placeholderImage: UIImage(named: ""), completed: nil)
//
//
//                     //alertview
//                     let alert = UIAlertController(title: Constants.APP_ALERT_TITLE, message: Constants.SUCCESS_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
//
//                     DispatchQueue.main.async {
//                         self.present(alert, animated: true, completion: nil)
//                     }
//
//                     alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
//                 }
//                 else
//                 {
//                     //do Error stuff
//
//                     //alertview
//                     let alert = UIAlertController(title: Constants.APP_ALERT_TITLE, message: Constants.ERROR_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
//
//                     DispatchQueue.main.async {
//                         self.present(alert, animated: true, completion: nil)
//                     }
//
//                     alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
//                 }
//                 break
//
//             case .failure(let error):
//
//                 if error._code == NSURLErrorTimedOut {
//                     //HANDLE TIMEOUT HERE
//                 }
//
//                 let alert = UIAlertController(title: Constants.APP_ALERT_TITLE, message: Constants.ERROR_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
//
//                 DispatchQueue.main.async {
//                     self.present(alert, animated: true, completion: nil)
//                 }
//
//                 alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
//
//                 print("\n\nAuth request failed with error:\n \(error)")
//                 break
//             }
//     }
// }
// */
