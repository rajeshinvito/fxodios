//
//  navigator.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD
import UIKit

class Connectivity {
    static var isNotConnectedToInternet:Bool{
        return !NetworkReachabilityManager()!.isReachable
    }
}



class indicator {
    class  func showActivityIndicator(){
         DispatchQueue.main.async{
        let KAppDelegate = UIApplication.shared.delegate as! AppDelegate
        var downloading = MBProgressHUD()
        downloading = MBProgressHUD(view: (KAppDelegate.window?.rootViewController?.view.window!)!)
        KAppDelegate.window!.addSubview(downloading)
        downloading.label.text = "Loading"
        downloading.show(animated: true)
        }
    }
    class func hideActivityIndicator(){
         DispatchQueue.main.async {
         let KAppDelegate = UIApplication.shared.delegate as! AppDelegate
         MBProgressHUD.hide(for: (KAppDelegate.window?.rootViewController?.view.window!)!, animated: true)
        }
    }
    
}


extension UIViewController {

    func showHUD(progressLabel:String){
        DispatchQueue.main.async{
            let progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
            progressHUD.label.text = progressLabel
        }
    }

    func dismissHUD(isAnimated:Bool) {
       
        DispatchQueue.main.async{
            MBProgressHUD.hide(for: self.view, animated: isAnimated)
        }
    }
}

extension UIViewController {
func json(from object:Any) -> String? {
       guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
           return nil
       }
       return String(data: data, encoding: String.Encoding.utf8)
   }
}
