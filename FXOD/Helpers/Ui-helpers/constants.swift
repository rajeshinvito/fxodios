//
//  constants.swift
//  TherapyClient
//
//  Created by rajesh gandru on 3/26/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation

let kApptitle = "Credo"

struct ATErrorMessage {
    struct Server {
        static let checkConnection = "Please check your Internet connection"
        static let down = "Could not connect to server, Please check your internet connection and try again later."
        static let notFound = "The page you are looking for cannot be found. Drop us an email and let us know how you got here! You can also email us at: connect@liscio.me"
        static let corruptedResponse = "Unable to read data coming form the server."
        static let invalidKey = "Data received form server is not coming on a valid key."
    }
    
    struct login {
        static let email = "Please enter valid email."
        static let password = "Please enter valid password."
        static let Maxpassword = "Please enter 8 digit password."
        static let required = "All feilds required."
        static let LastName = "Please enter your last name."
        static let FirstName = "Please enter your first name."
        static let Phone = "Please enter your phone number."


    }
    
    struct update {
        static let email = "please enter valid email."
        static let valid_Phone = "please enter valid Phonenumber."
         static let Phone = "please enter  Phonenumber."
        static let Address = "please enter Address"
        static let required = "All feilds required."
    }
    
    struct changePass {
        static let Oldpass = "please enter valid old passward."
        static let newpass = "please enter  new password."
        static let conform = "please enter  conform password."
        static let checking = "New password doesn't match with old password"
        static let required = "All feilds required."
    }
}
enum UserValidationState {
    case valid
    case invalid(String)
}
