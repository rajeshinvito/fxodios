//
//  Tabbar.swift
//  FXOD
//
//  Created by rajesh gandru on 6/16/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import Foundation
import UIKit
extension UITabBarController {
    func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
        if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -(height + 30)  : height)
      //  if tabBarIsVisible() {
        // animation
        UIViewPropertyAnimator(duration: duration, curve: .linear) {
            self.tabBar.frame.offsetBy(dx:0, dy:offsetY)
            self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }.startAnimation()
//        }
//        else {
//            UIViewPropertyAnimator(duration: duration, curve: .linear) {
             //   let offset: CGFloat? = tabBarIsVisible() ? self.tabBar.frame.size.height : - self.tabBar.frame.size.height
//                   UIView.animate(withDuration: animated ? 0.250 : 0.0, delay: 0.1, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [.curveEaseIn, .layoutSubviews], animations: {() -> Void in
//                       self.tabBar.center = CGPoint(x: CGFloat(self.tabBar.center.x), y: CGFloat(self.tabBar.center.y + offset!))
//
//                       //Check if View is already at bottom so we don't want to move view more up (it will show black screen on bottom ) Scnario : When  present mail app
//                       if (Int(offset!) <= 0 && UIScreen.main.bounds.height ==   self.view.frame.height) == false {
//                           self.view.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height + offset!)
//                       }
//
                
                //self.tabBar.frame.offsetBy(dx:self.tabBar.frame.size.height, dy:offsetY)
//                      self.view.frame = CGRect(x:0,y:0,width: self.view.frame.width, height: self.view.frame.height - height)
//                      self.view.setNeedsDisplay()
//                      self.view.layoutIfNeeded()
//                  }.startAnimation()
       // }
    }

    func tabBarIsVisible() ->Bool {
        return self.tabBar.frame.origin.y < UIScreen.main.bounds.height
    }
}
