

//
//  LoginVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
class LoginVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var EmailTF : UITextField!
    @IBOutlet weak var PasswordTF : UITextField!
    @IBOutlet weak var SigninBtn : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true

        // Do any additional setup after loading the view.
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
     
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        if textField == EmailTF || textField == PasswordTF {
                 if EmailTF.text?.count ?? 0 > 0 && PasswordTF.text?.count ?? 0 > 0 {
                     self.SigninBtn.setImage(#imageLiteral(resourceName: "BlueDonebtn"), for: .normal)
                 }else if EmailTF.text ?? "" == "" ||  PasswordTF.text ?? "" == "" {
                     self.SigninBtn.setImage(#imageLiteral(resourceName: "lightDonebtn"), for: .normal)
                 }
             }
        return true
    }
  
    
    @IBAction func forgotpasswordBtn(_ sender :UIButton) {
        self.movetonextvc(id: "forgotPasswordVC", storyBordid: "Main")
    }
    @IBAction func RegisterBtn(_ sender :UIButton) {
           self.movetonextvc(id: "RegisterVC", storyBordid: "Main")
       }
    @IBAction func DashBoardBtn(_ sender :UIButton) {
        // TODO: - 👊 Validations -
        switch validateSignupCredentials {
        case .valid:
            // self.moveToNext()
            self.login()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    @IBAction func cmdFacebook(_ sender: UIButton) {
      
        let fbLoginManager:LoginManager = LoginManager()
              fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
                  if error != nil{
                      print(error?.localizedDescription as Any)
                      return
                  }
                  let fbLoginResult:LoginManagerLoginResult = result!
                  if fbLoginResult.grantedPermissions != nil{
                      self.getFBUserData()
                  }
              }

    }
    
    @IBAction func cmdGoogle(_ sender: UIButton) {
           GIDSignIn.sharedInstance().delegate = self
           GIDSignIn.sharedInstance()?.presentingViewController = self
           GIDSignIn.sharedInstance()?.signIn() // ******* this must be called after setting delegates
       }
    
    //MARK:- Implementing Custom Method
       
    func getFBUserData(){
        if let token = AccessToken.current,
               !token.isExpired {
       // if((AccessToken.current) != nil){
            indicator.showActivityIndicator()
            //,albums
            GraphRequest(graphPath: "me", parameters: ["fields": "id,name,first_name,last_name,picture.type(large),email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    indicator.hideActivityIndicator()
                    //everything works print the user data
                    print(AccessToken.current!)
                    // print(result)
                    let dic = result as? Dictionary<String, Any>
                    let fbModel  = SocialModel.shared
                    fbModel.id = dic?["id"] as? String
                    fbModel.firstName = dic?["first_name"] as? String
                    fbModel.lastName = dic?["last_name"] as? String
                    fbModel.name = dic?["name"] as? String
                    fbModel.email = dic?["email"] as? String
                    // fbModel.gender = (dic?["gender"] as? String)?.capitalizingFirstLetter()
                    // fbModel.dob = dic?["birthday"] as? String
                    
                    guard let userInfo = result as? [String: Any] else { return }
                    
                    if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                        fbModel.profileImg = imageURL
                    }
                    
                   // self.callSocialRegisterLogin(logintype: Constant().LoginTypeConst.FACEBOOK, token: fbModel.id!, socialUser: fbModel, Email: fbModel.email ?? "")
                }
                else{
                    print("Error     :   \(String(describing: error?.localizedDescription))")
                    indicator.hideActivityIndicator()
                }
                
            })
        
        }
    }

}
extension LoginVC {
    //MARK:- login func
    func login(){
        UserDefaults.standard.removeObject(forKey: "Accesstoken")
        
        indicator.showActivityIndicator()
        guard let email = EmailTF.text else{
            indicator.hideActivityIndicator()
            return}
        guard let password = PasswordTF.text else{
            indicator.hideActivityIndicator()
            return}
        
        let parameters = [
            "email":email,
            "password":password,
            "device_type":"IOS",
        ]
        NetworkManager.Apicalling(url: loginUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(true, forKey: "LoggedIn")
                UserDefaults.standard.set(response.payload.id, forKey: "user_id")
                UserDefaults.standard.set(response.payload.email, forKey: "email")
                UserDefaults.standard.set(response.payload.token, forKey: "Accesstoken")
                UserDefaults.standard.set(response.payload.name, forKey: "name")

                self.moveToNext()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    //MARK:- move to next
    func moveToNext(){
       // self.movetonextvc(id: "tabbarVCViewController", storyBordid: "DashBoard")
        let DashBoardStoryboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

          var destinationController = DashBoardStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as? tabbarVCViewController

        var frontNavigationController = UINavigationController(rootViewController: destinationController!)

          var rearViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "SliderMenuVC") as? SliderMenuVC

        var mainRevealController = SWRevealViewController()

        mainRevealController.rearViewController = rearViewController
        mainRevealController.frontViewController = frontNavigationController
        appDelegate.window?.rootViewController = mainRevealController
        appDelegate.window?.makeKeyAndVisible()


    }
}


//MARK: - Validation
extension LoginVC {
    var validateSignupCredentials:UserValidationState {
        if EmailTF.text == "" || PasswordTF.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if EmailTF.text?.isEmpty == true  {
            return .invalid(ATErrorMessage.login.email)
        }else if PasswordTF.text?.isEmpty == true {
            return .invalid(ATErrorMessage.login.password)
        }
//        else if Passwordtfref.text?.count ?? 0 < 8 {
//            return .invalid(ATErrorMessage.login.Maxpassword)
//        }
        return .valid
    }
    
    
}
extension LoginVC:GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error.localizedDescription)
            return
        }
        guard let  profile = user.profile else{return}
        let GoglModel  = SocialModel.shared
        GoglModel.id = user.userID as String
        GoglModel.name = profile.name as String
        GoglModel.firstName = profile.givenName as String
        GoglModel.lastName = profile.familyName as String
        GoglModel.email = profile.email as String
        GoglModel.profileImg = "\(profile.imageURL(withDimension:200) as URL)"
       
       // self.callSocialRegisterLogin(logintype: Constant().LoginTypeConst.GMAIL, token: GoglModel.id!, socialUser: GoglModel, Email: GoglModel.email ?? "")
       
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }

}
