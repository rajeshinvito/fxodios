//
//  RegisterVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import CountryPickerView
class RegisterVC: UIViewController,UITextFieldDelegate, CountryPickerViewDelegate, CountryPickerViewDataSource  {
   

    //MARK:-  Class IBoutlets 
    @IBOutlet weak var Emailtfref: UITextField!
    @IBOutlet weak var Passwordtfref: UITextField!
    @IBOutlet weak var Lastnametfref: UITextField!
    @IBOutlet weak var Firstnametfref: UITextField!
    @IBOutlet weak var Phonetfref: UITextField!
    @IBOutlet weak var Checkmarkbtnref: UIButton!
    @IBOutlet weak var SignUPBtn : UIButton!
    @IBOutlet weak var UiAlertPopView: UIView!
    @IBOutlet weak var CountryImageref: UIImageView!
    @IBOutlet weak var PopUptitleLblref: UILabel!
    
    
    //MARK:-  Class Properties 
    let countryPickerView = CountryPickerView()
    var ischeckmark = true
    
    
    //MARK:-  View Lyfe cycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.UiAlertPopView.isHidden = true
        
        //MARK:- Country picker
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        let country = countryPickerView.selectedCountry
        self.CountryImageref.image = country.flag
    }
    
    
    
    //MARK:-  Class Button actions 
    
    //country list showing
    @IBAction func buttonPressed(_ sender: Any) {
      countryPickerView.showCountriesList(from: self)
    }
    // agree check mark
    @IBAction func Checkmarkbtnref(_ sender: Any) {
        
        if ischeckmark {
            ischeckmark = false
            self.Checkmarkbtnref.setImage(#imageLiteral(resourceName: "checkmarks"), for: .normal)
        }else {
             ischeckmark = true
            self.Checkmarkbtnref.setImage(#imageLiteral(resourceName: "Uncheckmark"), for: .normal)

        }
    }
    //Terms and condition
    @IBAction func Termsconditionbtnref(_ sender: Any) {
        
    }
    //SignUP button
    @IBAction func SignUpbntref(_ sender: Any) {
       // self.popToBackVC()
        self.movetonextvc(id: "LoginVC", storyBordid: "Main")

    }
    //Sign Up btn
    @IBAction func Submitbtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateSignupCredentials {
        case .valid:
            RegistrationMethod()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    @IBAction func RemoveAlertbtnref(_ sender: Any) {
        self.UiAlertPopView.isHidden = true

    }
    
    @IBAction func Backbtnref(_ sender: Any) {
        self.UiAlertPopView.isHidden = true
    }
}

//MARK:-  text field actions
extension RegisterVC {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
       
        
        if textField == Emailtfref || textField == Passwordtfref || textField == Lastnametfref || textField == Firstnametfref || textField == Phonetfref{
            if Emailtfref.text?.count ?? 0 > 0 && Passwordtfref.text?.count ?? 0 > 0 && Lastnametfref.text?.count ?? 0 > 0 && Firstnametfref.text?.count ?? 0 > 0 && Phonetfref.text?.count ?? 0 > 0 {
                self.SignUPBtn.setImage(#imageLiteral(resourceName: "BlueDonebtn"), for: .normal)
            }else if Emailtfref.text ?? "" == "" ||  Passwordtfref.text ?? "" == "" ||  Lastnametfref.text ?? "" == "" ||  Firstnametfref.text ?? "" == "" ||  Phonetfref.text ?? "" == "" {
                self.SignUPBtn.setImage(#imageLiteral(resourceName: "lightDonebtn"), for: .normal)
            }
        }
        return true
    }
    //MARK:- picker delegates
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        self.CountryImageref.image = country.flag
    }
}
extension RegisterVC {
    //MARK:- login func
    func RegistrationMethod(){
        UserDefaults.standard.removeObject(forKey: "Accesstoken")
        
        indicator.showActivityIndicator()
        guard let email = Emailtfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let password = Passwordtfref.text else{
            indicator.hideActivityIndicator()
            return}
        guard let Lastname = Lastnametfref.text else{
                   indicator.hideActivityIndicator()
                   return}
        guard let Firstname = Firstnametfref.text else{
        indicator.hideActivityIndicator()
        return}
        guard let Phone = Phonetfref.text else{
        indicator.hideActivityIndicator()
        return}
        let parameters = [
            "first_name":Firstname,
            "last_name":Lastname,
            "email":email,
            "password":password,
            "phone":Phone,
            "deviceid":UIDevice.current.identifierForVendor!.uuidString
        ]
        NetworkManager.Apicalling(url: SignUpUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                UserDefaults.standard.set(true, forKey: "LoggedIn")
                UserDefaults.standard.set(response.payload.id, forKey: "user_id")
                UserDefaults.standard.set(response.payload.email, forKey: "email")
                UserDefaults.standard.set(response.payload.access_token, forKey: "Accesstoken")
                UserDefaults.standard.set(response.payload.name, forKey: "name")

                self.movetonextvc(id: "OTPVc", storyBordid: "Main")
            }else {
                indicator.hideActivityIndicator()
                if response.message == "The email has already been taken." {
                    self.PopUptitleLblref.text = "Your email id \(self.Emailtfref.text ?? "") already has an account on this app."
                    self.UiAlertPopView.isHidden = false
                }else {
                self.ShowAlert(message: response.message)
                }
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                if err == "The email has already been taken." {
                    self.PopUptitleLblref.text = "Your email id \(self.Emailtfref.text ?? "") already has an account on this app."
                    self.UiAlertPopView.isHidden = false
                }else {
                    self.ShowAlert(message: err)
                }
            }
        }
    }
    
    //MARK:- move to next
    func moveToNext(){
        self.movetonextvc(id: "DashBoardVC", storyBordid: "DashBoard")

    }
}


//MARK: - Validation
extension RegisterVC {
    var validateSignupCredentials:UserValidationState {
        if Emailtfref.text == "" || Passwordtfref.text ?? "" == "" || Lastnametfref.text ?? "" == "" || Firstnametfref.text ?? "" == "" || Phonetfref.text ?? "" == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if Emailtfref.text?.isEmpty == true  {
            return .invalid(ATErrorMessage.login.email)
        }else if Passwordtfref.text?.isEmpty == true {
            return .invalid(ATErrorMessage.login.password)
        }else if Passwordtfref.text?.count ?? 0 < 8 {
            return .invalid(ATErrorMessage.login.Maxpassword)
        }else if Lastnametfref.text?.isEmpty == true {
            return .invalid(ATErrorMessage.login.LastName)
        }else if Firstnametfref.text?.isEmpty == true {
            return .invalid(ATErrorMessage.login.FirstName)
        }else if Phonetfref.text?.isEmpty == true{
            return .invalid(ATErrorMessage.login.Phone)
        }
        return .valid
    }
    
    
}
