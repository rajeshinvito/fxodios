//
//  UserguideVCViewController.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class UserguideVCViewController: UIViewController,UIScrollViewDelegate {
    
    
    
    //MARK:- OUTLETS
    @IBOutlet weak var Backpagenationref: UIView!
    
    //MARK:- VARIABLES
    let imagelist = ["guaidImages1","guaidimages2","guaidimages3","guaidimages4","guaidimages5"]
    //let imagelist = [#imageLiteral(resourceName: "guaidimages5"),#imageLiteral(resourceName: <#T##String#>),#imageLiteral(resourceName: <#T##String#>),#imageLiteral(resourceName: <#T##String#>),#imageLiteral(resourceName: <#T##String#>)]
    var scrollView = UIScrollView()
    let screenSize: CGRect = UIScreen.main.bounds
    var pageControl = UIPageControl()
    var yPosition:CGFloat = 0
    var scrollViewContentSize:CGFloat=0;
    
    //MARK:- VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
      //  pageControl = UIPageControl(frame:CGRect(x: self.screenSize.width/2 - pageControl.frame.size.width/2, y: self.screenSize.height - 200, width: 200, height: 0))
       // pageControl.center.x = self.view.center.x
        configurePageControl()
        
        scrollView.delegate = self
        self.Backpagenationref.addSubview(scrollView)
        for  i in stride(from: 0, to: imagelist.count, by: 1) {
            var frame = CGRect.zero
            let myImage:UIImage = UIImage(named: imagelist[i])!
            let myImageView:UIImageView = UIImageView()
            myImageView.image = myImage
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(i)
            myImageView.contentMode = UIView.ContentMode.scaleAspectFill
            //frame.origin.x + 140
         //   myImageView.frame = CGRect(x: frame.origin.x + self.scrollView.frame.size.width/2 - 100, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height)
            myImageView.frame = CGRect(x: frame.origin.x , y: 10, width: self.scrollView.frame.size.width, height: self.view.frame.height)
            
            // frame.origin.y = 0
            // frame.size = CGRect(x: <#T##Int#>, y: <#T##Int#>, width: <#T##Int#>, height: <#T##Int#>)
            self.scrollView.isPagingEnabled = true
            scrollView.addSubview(myImageView)
            
        }
      
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(imagelist.count), height: self.scrollView.frame.size.height + 50)
              self.scrollView.clipsToBounds = true
            pageControl.addTarget(self, action: Selector(("changePage:")), for: UIControl.Event.valueChanged)

    }
    
    
    @IBAction func signinbtnref(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//               self.navigationController?.pushViewController(vc, animated: true)
        UserDefaults.standard.set(true, forKey: "isguideopen")
        self.movetonextvc(id: "LoginVC", storyBordid: "Main")
    }
    
    @IBAction func signupbtnref(_ sender: Any) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
//                   self.navigationController?.pushViewController(vc, animated: true)
        self.movetonextvc(id: "RegisterVC", storyBordid: "Main")
        UserDefaults.standard.set(true, forKey: "isguideopen")


    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        self.pageControl.numberOfPages = imagelist.count
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.red
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor(named: "Green")
        self.view.addSubview(pageControl)
    }
    
    // MARK : TO CHANGE WHILE CLICKING ON PAGE CONTROL
    func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x: x,y :0), animated: true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        
        pageControl.currentPage = Int(pageNumber)
    }
    
}


