//
//  DevicePasswordVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/7/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class DevicePasswordVC: UIViewController {

    
    @IBOutlet weak var EnterPasswordtfref: UITextField!
    @IBOutlet weak var AlertBackViewref: UIView!
    @IBOutlet weak var ConformPasstfref: UITextField!
    @IBOutlet weak var Passworddosnotmathheightref: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Passworddosnotmathheightref.constant = 0
    }
    

    @IBAction func Donebtnref(_ sender: Any) {
        self.AlertBackViewref.isHidden = true
    // self.movetonextvc(id: "tabbarVCViewController", storyBordid: "DashBoard")
        let DashBoardStoryboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

              var destinationController = DashBoardStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as? tabbarVCViewController

            var frontNavigationController = UINavigationController(rootViewController: destinationController!)

              var rearViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "SliderMenuVC") as? SliderMenuVC

            var mainRevealController = SWRevealViewController()

            mainRevealController.rearViewController = rearViewController
            mainRevealController.frontViewController = frontNavigationController
            appDelegate.window?.rootViewController = mainRevealController
            appDelegate.window?.makeKeyAndVisible()
    }
    
    @IBAction func Submitbtnref(_ sender: Any) {
        // TODO: - 👊 Validations -
        switch validateSignupCredentials {
        case .valid:
            UserDefaults.standard.set(EnterPasswordtfref, forKey: "AppPassword")
            self.AlertBackViewref.isHidden = false
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
        
    }
}
extension DevicePasswordVC {
    var validateSignupCredentials:UserValidationState {
        if EnterPasswordtfref.text ?? "" == "" || ConformPasstfref.text  ?? "" == ""{
            return .invalid(ATErrorMessage.login.required)
        }else if EnterPasswordtfref.text ?? "" == ConformPasstfref.text ?? ""{
            Passworddosnotmathheightref.constant = 32
            return .invalid("Your conform password didn't match with password")
        }
        return .valid
    }
    
    
}
