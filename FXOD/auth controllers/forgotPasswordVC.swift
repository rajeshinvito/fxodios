
//
//  forgotPasswordVC.swift
//  FXOD
//
//  Created by rajesh gandru on 6/4/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit

class forgotPasswordVC: UIViewController {
    @IBOutlet weak var EmailTF : UITextField!

    @IBOutlet weak var AlertBackviewref: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.AlertBackviewref.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Backbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    @IBAction func SubmitBtn(_ sender :UIButton) {
          // TODO: - 👊 Validations -
          switch validateSignupCredentials {
          case .valid:
              // self.moveToNext()
              self.ForgotApiMethod()
          case .invalid(let message):
              self.ShowAlert(message: message)
          }
      }
   
    @IBAction func Okbtnref(_ sender: Any) {
        self.popToBackVC()
    }
    @IBAction func Alerthidebtnref(_ sender: Any) {
        self.AlertBackviewref.isHidden = false

    }
    
}
extension forgotPasswordVC {
    //MARK:- login func
    func ForgotApiMethod(){
        UserDefaults.standard.removeObject(forKey: "Accesstoken")
        
        indicator.showActivityIndicator()
        guard let email = EmailTF.text else{
            indicator.hideActivityIndicator()
            return}
     
        
        let parameters = [
            "email":email
           
        ]
        NetworkManager.Apicalling(url: forgotUrl, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.AlertBackviewref.isHidden = false
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    //MARK:- move to next
    func moveToNext(){
        self.movetonextvc(id: "LoginVC", storyBordid: "Main")

    }
}


//MARK: - Validation
extension forgotPasswordVC {
    var validateSignupCredentials:UserValidationState {
        if EmailTF.text == "" {
            return .invalid(ATErrorMessage.login.required)
        }else if EmailTF.text?.isEmpty == true  {
            return .invalid(ATErrorMessage.login.email)
        }

        return .valid
    }
    
    
}
