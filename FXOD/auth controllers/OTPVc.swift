
//
//  OTPVc.swift
//  FXOD
//
//  Created by rajesh gandru on 6/7/20.
//  Copyright © 2020 rajesh gandru. All rights reserved.
//

import UIKit
import DPOTPView
class OTPVc: UIViewController {

    
    @IBOutlet weak var titlelblref: UILabel!
    @IBOutlet weak var OtpViewref: UIView!
    @IBOutlet weak var SendAgainOtplblref: UILabel!
    @IBOutlet weak var Secondslblref: UILabel!
    @IBOutlet weak var Sentbtnref: UIButton!
    
    @IBOutlet weak var sendagainbtnref: UIButton!
    let Email = UserDefaults.standard.string(forKey: "email") ?? ""
    var txtOTPView: DPOTPView!
    var seconds = 60
    var timer = Timer()
    var FINALOTP = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titlelblref.text = "A verification codes has been sent\nto \(Email)"
        self.Secondslblref.text = "0s"
        self.SendAgainOtplblref.textColor = UIColor(named: "#CADBFB")
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
//        txtDPOTPView.dpOTPViewDelegate = self
//        txtDPOTPView.fontTextField = UIFont.systemFont(ofSize: 40)
//        txtDPOTPView.textEdgeInsets = UIEdgeInsets(top: 0, left: -1, bottom: 0, right: 0)
//        txtDPOTPView.editingTextEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        

    }
    @objc func update() {
        
        if(seconds > 0) {
            seconds -= 1
            self.Secondslblref.text = String(seconds) + "s"
            self.SendAgainOtplblref.textColor = UIColor(named: "#CADBFB")
        }else {
            self.SendAgainOtplblref.textColor = UIColor(named: "Bluecolor")
            self.timer.invalidate()
             
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if txtOTPView == nil {
            txtOTPView = DPOTPView(frame: CGRect(x: (self.OtpViewref.frame.width - 150 ) / 2 , y: 0, width: 150, height: 50))
                
               // DPOTPView(frame: 5, y: 5, width: 150, height: 60)
            txtOTPView.count = 6
            txtOTPView.spacing = 10
            txtOTPView.fontTextField = UIFont(name: "Montserrat-Medium", size: CGFloat(15.0))!
            txtOTPView.dismissOnLastEntry = true
            txtOTPView.borderColorTextField = .black
            txtOTPView.selectedBorderColorTextField = .black
            txtOTPView.borderWidthTextField = 2
            txtOTPView.keyboardType = .numberPad
            txtOTPView.backgroundColor = .white
            txtOTPView.dpOTPViewDelegate = self
           // txtOTPView.backGroundColorTextField = .lightGray
            txtOTPView.cornerRadiusTextField = 8
        //    txtOTPView.isCursorHidden = true
            //        txtOTPView.isSecureTextEntry = true
                    txtOTPView.isBottomLineTextField = true
            //        txtOTPView.isCircleTextField = true
            self.OtpViewref.addSubview(txtOTPView)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        txtDPOTPView.text = "1234"
        //        print(txtDPOTPView.text ?? "")
        //        print(txtDPOTPView.validate())
        //        _ = txtDPOTPView.becomeFirstResponder()
    }

    @IBAction func SkipNowbtnref(_ sender: Any) {
        self.Secondslblref.text =  "0s"
        self.SendAgainOtplblref.textColor = UIColor(named: "#CADBFB")
       // self.movetonextvc(id: "DashBoardVC", storyBordid: "DashBoard")
        
        let DashBoardStoryboard : UIStoryboard = UIStoryboard(name: "DashBoard", bundle: nil)
           let appDelegate = UIApplication.shared.delegate as! AppDelegate

                 var destinationController = DashBoardStoryboard.instantiateViewController(withIdentifier: "tabbarVCViewController") as? tabbarVCViewController

               var frontNavigationController = UINavigationController(rootViewController: destinationController!)

                 var rearViewController = DashBoardStoryboard.instantiateViewController(withIdentifier: "SliderMenuVC") as? SliderMenuVC

               var mainRevealController = SWRevealViewController()

               mainRevealController.rearViewController = rearViewController
               mainRevealController.frontViewController = frontNavigationController
               appDelegate.window?.rootViewController = mainRevealController
               appDelegate.window?.makeKeyAndVisible()

    }
    
    @IBAction func Sentbtnref(_ sender: Any) {
        
        // TODO: - 👊 Validations -
        switch validateSignupCredentials {
        case .valid:
            // self.moveToNext()
            self.OtpVerification()
        case .invalid(let message):
            self.ShowAlert(message: message)
        }
    }
    
    
    @IBAction func ChangeEmailbtnref(_ sender: Any) {
        self.movetonextvc(id: "LoginVC", storyBordid: "Main")
    }
    
    @IBAction func Sendagainbtnref(_ sender: Any) {
        self.seconds = 60
        self.SendAgainOtplblref.textColor = UIColor(named: "#CADBFB")

    }
}
extension OTPVc : DPOTPViewDelegate {
    func dpOTPViewAddText(_ text: String, at position: Int) {
        print("addText:- " + text + " at:- \(position)" )
        if position == 5 {
            self.FINALOTP = text
        }
    }
    
    func dpOTPViewRemoveText(_ text: String, at position: Int) {
        print("removeText:- " + text + " at:- \(position)" )
    }
    
    func dpOTPViewChangePositionAt(_ position: Int) {
        print("at:-\(position)")
        if position == 5 {
            self.Sentbtnref.backgroundColor = UIColor(named: "Bluecolor")
            self.timer.invalidate()
            
        }else {
            //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
            self.Sentbtnref.backgroundColor = UIColor(named: "#CADBFB")

        }
        
    }
    func dpOTPViewBecomeFirstResponder() {
        
    }
    func dpOTPViewResignFirstResponder() {
        
    }
}
extension OTPVc {
    //MARK:- login func
    func OtpVerification(){
        indicator.showActivityIndicator()
        let parameters = [
            "otp":FINALOTP
        ]
        NetworkManager.Apicalling(url: OtpVerify, paramaters: parameters, httpMethodType: .post, success: { (response:userdataModel) in
            print(response.payload)
            if response.status == 1 {
                indicator.hideActivityIndicator()
                self.moveToNext()
            }else {
                indicator.hideActivityIndicator()
                self.ShowAlert(message: response.message)
            }
        }) { (errorMsg) in
            indicator.hideActivityIndicator()
            if let err = errorMsg as? String{
                self.ShowAlert(message: err)
            }
        }
    }
    
    //MARK:- move to next
    func moveToNext(){
        self.movetonextvc(id: "DevicePasswordVC", storyBordid: "Main")

    }
}
//MARK: - Validation
extension OTPVc {
    var validateSignupCredentials:UserValidationState {
        
        if FINALOTP.count  < 6 {
            return .invalid("Please enter pin")
        }
        return .valid
    }
    
    
}
